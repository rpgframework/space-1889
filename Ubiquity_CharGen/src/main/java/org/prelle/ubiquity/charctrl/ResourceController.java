/**
 * 
 */
package org.prelle.ubiquity.charctrl;

import java.util.List;

import org.prelle.ubiquity.Resource;
import org.prelle.ubiquity.ResourceValue;

/**
 * @author Stefan
 *
 */
public interface ResourceController {

	//-------------------------------------------------------------------
	public double getPointsLeft();
	
	//-------------------------------------------------------------------
	public List<Resource> getAvailableResources();

	//-------------------------------------------------------------------
	public boolean canBeSelected(Resource talent);

	//-------------------------------------------------------------------
	public boolean canBeDeSelected(ResourceValue key);

	//-------------------------------------------------------------------
	public ResourceValue select(Resource talent);

	//-------------------------------------------------------------------
	public boolean deselect(ResourceValue key);

	//-------------------------------------------------------------------
	public boolean canBeDecreased(ResourceValue key);

	//-------------------------------------------------------------------
	public boolean canBeIncreased(ResourceValue key);

	//-------------------------------------------------------------------
	public boolean increase(ResourceValue key);

	//-------------------------------------------------------------------
	public boolean decrease(ResourceValue key);

	//-------------------------------------------------------------------
	public ResourceValue findResourceReference(Resource res, String descr,
			String idref);

	//-------------------------------------------------------------------
	public void trash(ResourceValue ref);

}
