/**
 * 
 */
package org.prelle.ubiquity.charctrl;

import org.prelle.ubiquity.Talent;
import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.UbiquityRuleset;

import de.rpgframework.genericrpg.modification.Modification;


/**
 * Common interface for creating and leveling a character
 * 
 * @author prelle
 *
 */
public interface CharacterController {
	
	//-------------------------------------------------------------------
	public UbiquityRuleset getRuleset();
	
	//-------------------------------------------------------------------
	public UbiquityCharacter getModel();
	
	//-------------------------------------------------------------------
	public CharGenMode getMode();
	
	//-------------------------------------------------------------------
	public AttributeController getAttributeController();
	
	//-------------------------------------------------------------------
	public SkillController getSkillController();
	
	//-------------------------------------------------------------------
	public TalentController getTalentController();
	
	//-------------------------------------------------------------------
	public ResourceController getResourceController();
	
	//-------------------------------------------------------------------
	public FlawController getFlawController();
	
	//-------------------------------------------------------------------
	public ItemController getItemController();
	
	//-------------------------------------------------------------------
	public void apply(Modification mod);
	
	//-------------------------------------------------------------------
	public void undo(Modification mod);

	//-------------------------------------------------------------------
	public boolean isRequirementMet(Talent item);
	
}
