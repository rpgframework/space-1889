/**
 * 
 */
package org.prelle.ubiquity.levelling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.ubiquity.Skill;
import org.prelle.ubiquity.SkillSpecialization;
import org.prelle.ubiquity.SkillSpecializationValue;
import org.prelle.ubiquity.SkillValue;
import org.prelle.ubiquity.Talent;
import org.prelle.ubiquity.TalentValue;
import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.UbiquityRuleset;
import org.prelle.ubiquity.charctrl.SkillController;
import org.prelle.ubiquity.chargen.event.GenerationEvent;
import org.prelle.ubiquity.chargen.event.GenerationEventDispatcher;
import org.prelle.ubiquity.chargen.event.GenerationEventListener;
import org.prelle.ubiquity.chargen.event.GenerationEventType;
import org.prelle.ubiquity.modifications.ModificationImpl;
import org.prelle.ubiquity.modifications.SkillModification;
import org.prelle.ubiquity.modifications.SpecializationModification;

/**
 * @author Stefan
 *
 */
public class SkillLeveller implements SkillController, GenerationEventListener {

	private static Logger logger = LogManager.getLogger("ubiquity.chargen");

	private UbiquityRuleset rules;
	private UbiquityCharacter model;

	private List<Skill> skills;
	private List<ModificationImpl> undoList;
	private Map<Skill, Stack<SkillModification>> skillUndoStack;

	//--------------------------------------------------------------------
	/**
	 */
	public SkillLeveller(UbiquityRuleset rules, UbiquityCharacter model, List<ModificationImpl> undoList) {
		this.rules = rules;
		this.model = model;
		this.undoList = undoList;
		skills = new ArrayList<Skill>();

		skillUndoStack = new HashMap<Skill, Stack<SkillModification>>();
		for (Skill key : rules.getSkills()) {
			skillUndoStack.put(key, new Stack<SkillModification>());
		}
		
		updateAvailable();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#getRuleset()
	 */
	@Override
	public UbiquityRuleset getRuleset() {
		return rules;
	}

	//-------------------------------------------------------------------
	private boolean hasSkillMaster(Skill skillGroup) {
		for (TalentValue tVal : model.getTalents()) {
			Talent talent = tVal.getTalent();
			if (talent.getId().equals("skillmaster")) {
				if (tVal.getSkillOption()==skillGroup)
					return true;
			}
		}
		return false;
	}

	//-------------------------------------------------------------------
	private void updateAvailable() {
		logger.debug("updateAvailable");
		
		List<Skill> added = new ArrayList<>();
		List<Skill> removed = new ArrayList<>();
		for (Skill skill : rules.getSkills()) {
			// Non group skills are simply added
			if (!skill.isGroup() && !skill.isPartOfGroup()) {
				if (!skills.contains(skill)) {
					skills.add(skill);
					added.add(skill);
				}					
			} else if (skill.isPartOfGroup()) {
				/* 
				 * This is part of a skill group. It can be
				 * added, unless the skillmaster talent is 
				 * present for this group
				 */
				Skill skillGroup = skill.getParentSkill();
				if (hasSkillMaster(skillGroup)) {
					// Skillmaster present - sub-skills may not be added
					// or must be removed
					if (skills.contains(skill)) {
						skills.remove(skill);
						removed.add(skill);
					} 
					/*
					 * Only remove those points invested in this
					 * skill that can be undone.
					 */
					while (canBeDecreased(skill)) {
						logger.warn("free EXP than can be undone when disabling "+skill);
						decrease(skill);
					}
					
//					if (model.getSkill(skill)!=null) {
//						pointsLeft += model.getSkill(skill).getPoints();
//						model.removeSkill(model.getSkill(skill));
//						GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILL_REMOVED, skill));
//						GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SKILLS, null, pointsLeft));
//					}
				} else {
					// Skillmaster not present. May be added
					if (!skills.contains(skill)) {
						skills.add(skill);
						added.add(skill);
					}					
				}
			} else if (skill.isGroup()) {
				/*
				 * This is a group. It may only be added if
				 * the corresponding skillmaster talent is
				 * present
				 */
				if (hasSkillMaster(skill)) {
					// Skillmaster present. May be added
					if (!skills.contains(skill)) {
						skills.add(skill);
						added.add(skill);
					}										
				} else {
					// Skillmaster not present. Must not be
					// added and must be removed if present
					if (skills.contains(skill)) {
						skills.remove(skill);
						removed.add(skill);
					} 
					/*
					 * Only remove those points invested in this
					 * skill that can be undone.
					 */
					while (canBeDecreased(skill)) {
						logger.warn("free EXP than can be undone when disabling "+skill);
						decrease(skill);
					}

					//					if (model.getSkill(skill)!=null) {
//						pointsLeft += model.getSkill(skill).getPoints();
//						model.removeSkill(model.getSkill(skill));
//						GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILL_REMOVED, skill));
//						GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SKILLS, null, pointsLeft));
//					}
				}
			}
		}
		logger.debug("Available skills added: "+added);
		logger.debug("Available skills removed: "+removed);
		
		if (added.isEmpty() && removed.isEmpty()) {
			// Nothing changed
			logger.debug("Nothing changed");
			return;
		}
		logger.debug("Some changes");
		List<Skill>[] ret = new List[]{skills,added,removed};
		GenerationEventDispatcher.fireEvent(
				new GenerationEvent(
						GenerationEventType.SKILL_AVAILABLE_CHANGED, 
						ret));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#getAvailableSkills()
	 */
	@Override
	public List<Skill> getAvailableSkills() {
		return skills;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#getPointsLeft()
	 */
	@Override
	public double getPointsLeft() {
		return 0;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#canBeDecreased(org.prelle.ubiquity.Skill)
	 */
	@Override
	public boolean canBeDecreased(Skill skill) {
		SkillValue set = model.getSkill(skill);
		if (set==null) return false;
		return (set.getPoints()>0) && !skillUndoStack.get(skill).isEmpty();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#canBeIncreased(org.prelle.ubiquity.Skill)
	 */
	@Override
	public boolean canBeIncreased(Skill skill) {
		SkillValue set = model.getSkill(skill);
		if (set==null) return false;
		
		// Determine new target value
		int newVal = set.getPoints()+1;
		
		// Determine how much exp is needed
		int expNeeded = newVal*2;
		return expNeeded<=model.getExperienceFree();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#getIncreaseCost(org.prelle.ubiquity.SkillValue)
	 */
	@Override
	public int getIncreaseCost(SkillValue skill) {
		return (skill.getPoints()+1)*2;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#increase(org.prelle.ubiquity.Skill)
	 */
	@Override
	public boolean increase(Skill skill) {
		if (!canBeIncreased(skill))
			return false;
		
		SkillValue set = model.getSkill(skill);
		
		int oldVal = set.getPoints();
		int newVal = set.getPoints()+1;
		// Determine how much exp is needed
		int expNeeded = getIncreaseCost(set);

		// Update model
		logger.info("Increase "+skill+" from "+set.getPoints()+" to "+newVal);
		set.setPoints(newVal);
		if (oldVal==0)
			model.addSkill(set);
		model.setExperienceFree(model.getExperienceFree()-expNeeded);
		model.setExperienceInvested(model.getExperienceInvested()+expNeeded);
		// Add to undo list
		SkillModification mod = new SkillModification(skill, newVal);
		mod.setExpCost(expNeeded);
		undoList.add(mod);
		skillUndoStack.get(skill).push(mod);
		// Inform listener
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILL_CHANGED, skill, new int[]{oldVal,newVal}));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new int[]{
				model.getExperienceFree(),
				model.getExperienceInvested()
		}));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.UNDO_LIST_CHANGED, undoList));

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#decrease(org.prelle.ubiquity.Skill)
	 */
	@Override
	public boolean decrease(Skill skill) {
		if (!canBeDecreased(skill))
			return false;
		
		SkillValue set = model.getSkill(skill);
		
		// Find matching modification to undo
		SkillModification mod = skillUndoStack.get(skill).pop();
		
		int oldVal = set.getPoints();
		int newVal = set.getPoints()-1;
		// Determine how much exp is needed
		int expNeeded = mod.getExpCost();

		// Update model
		logger.info("Decrease "+skill+" from "+set.getPoints()+" to "+newVal);
		set.setPoints(newVal);
		if (newVal==0)
			model.removeSkill(set);
		model.setExperienceFree(model.getExperienceFree()+expNeeded);
		model.setExperienceInvested(model.getExperienceInvested()-expNeeded);
		// Remove to undo list
		undoList.remove(mod);
		// Inform listener
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILL_CHANGED, skill, new int[]{oldVal,newVal}));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new int[]{
				model.getExperienceFree(),
				model.getExperienceInvested()
		}));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.UNDO_LIST_CHANGED, undoList));

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#getAvailableSpecializations(org.prelle.ubiquity.Skill)
	 */
	@Override
	public List<SkillSpecialization> getAvailableSpecializations(Skill key) {
		return key.getSpecializations();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#canSelectSpecialization(org.prelle.ubiquity.SkillSpecialization)
	 */
	@Override
	public boolean canSelectSpecialization(SkillSpecialization key) {
		// Need 3 exp
		if (model.getExperienceFree()<3)
			return false;
		// Need at least 1 point in skill
		if (model.getSkill(key.getSkill()).getPoints()<=0)
			return false;
		// Must not have specialization yet
		if (model.getSkill(key.getSkill()).getSpecializations().contains(key))
			return false;
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#canDeselectSpecialization(org.prelle.ubiquity.SkillSpecialization)
	 */
	@Override
	public boolean canDeselectSpecialization(SkillSpecializationValue key) {
		if (key==null)
			return false;
		if (!model.getSkill(key.getSpecialization().getSkill()).getSpecializations().contains(key))
			return false;

		if (key.getValue()>1)
			return false;
		
		/* Find SpecializationModification */
		SpecializationModification mod = null;
		for (ModificationImpl tmp : undoList) {
			if ((tmp instanceof SpecializationModification) && ((SpecializationModification)tmp).getSpecialization()==key.getSpecialization()) {
				mod = (SpecializationModification) tmp;
				break;
			}
		}
		return mod!=null;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#selectSpecialization(org.prelle.ubiquity.SkillSpecialization)
	 */
	@Override
	public SkillSpecializationValue selectSpecialization(SkillSpecialization special) {
		if (!canSelectSpecialization(special)) 
			return null;

		logger.info("select specialization "+special);
		Skill skill = special.getSkill();

		// Buy specialization
		int expNeeded = 3;
		SpecializationModification mod = new SpecializationModification(special);
		mod.setExpCost(expNeeded);
//		masteryUndoStack.get(skill).add(mod);
		undoList.add(mod);
		// Add to model
		SkillSpecializationValue specVal = new SkillSpecializationValue(special, 1);
		model.getSkill(skill).addSpecialization(specVal);
		logger.info("Buy specialization '"+special+"' for skill "+skill);
		model.setExperienceInvested(model.getExperienceInvested()+expNeeded);
		model.setExperienceFree(model.getExperienceFree()-expNeeded);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILL_CHANGED, skill));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.UNDO_LIST_CHANGED, undoList));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new int[]{model.getExperienceFree(), model.getExperienceInvested()}));
		return specVal;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#deselectSpecialization(org.prelle.ubiquity.SkillSpecialization)
	 */
	@Override
	public boolean deselectSpecialization(SkillSpecializationValue specVal) {
		if (!canDeselectSpecialization(specVal))
			return false;

		SkillSpecialization special = specVal.getSpecialization();
		logger.info("deselect specialization "+special);
		Skill skill = special.getSkill();

		/* Find SpecializationModification */
		SpecializationModification mod = null;
		for (ModificationImpl tmp : undoList) {
			if ((tmp instanceof SpecializationModification) && ((SpecializationModification)tmp).getSpecialization()==special) {
				mod = (SpecializationModification) tmp;
				break;
			}
		}
		if (mod==null) {
			logger.warn("Trying to deselect an unknown specialization");
			return false;
		}
		
		undoList.remove(mod);
		// Add to model
		int expGranted = 3;
		model.getSkill(skill).removeSpecialization(specVal);
		logger.info("Reimburse specialization '"+special+"' for skill "+skill);
		model.setExperienceInvested(model.getExperienceInvested()-expGranted);
		model.setExperienceFree(model.getExperienceFree()+expGranted);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILL_CHANGED, skill));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.UNDO_LIST_CHANGED, undoList));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new int[]{model.getExperienceFree(), model.getExperienceInvested()}));
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.ubiquity.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		TalentValue tVal = null;
		Talent talent = null;
		switch (event.getType()) {
		case TALENT_ADDED:
			logger.debug("RCV "+event);
			// In case of a skillmaster talent, memorize
			tVal = (TalentValue)event.getKey();
			talent = tVal.getTalent();
			int highest = 0;
			if (talent.getId().equals("skillmaster")) {
				// Search the highest value in subskills to remove
				for (Skill child : tVal.getSkillOption().getChildSkills()) {
					SkillValue sVal = model.getSkill(child);
					logger.debug(" "+child+" = "+sVal);
					if (sVal!=null) {
						highest = Math.max(highest, sVal.getPoints());
					}
				}
				updateAvailable();
				logger.info("Auto-select "+tVal.getSkillOption()+" on "+highest);
				for (int i=0; i<highest; i++)
					increase(tVal.getSkillOption());
			} else
				updateAvailable();
			break;
		case TALENT_REMOVED:
			updateAvailable();
			break;
		case SKILL_AVAILABLE_CHANGED:
			// 
		}
		
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		ArrayList<String> ret = new ArrayList<>();
//		if (getPointsLeft()>0)
//			ret.add(String.format(RES.getString("skillgen.todo.points"), getPointsLeft()));
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#canIncreaseSpecialization(org.prelle.ubiquity.SkillSpecialization)
	 */
	@Override
	public boolean canIncreaseSpecialization(SkillSpecializationValue key) {
		if (key==null) 
			return false;
		// Need 3 exp
		if (model.getExperienceFree()<3)
			return false;
		// Need at least 1 point in skill
		if (model.getSkill(key.getSpecialization().getSkill()).getPoints()<=0)
			return false;
		// Must have specialization yet
		if (!model.getSkill(key.getSpecialization().getSkill()).getSpecializations().contains(key)) 
			return false;
		// Specialization must be below 5
		if (key.getValue()>=5)
			return false;
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#canDecreaseSpecialization(org.prelle.ubiquity.SkillSpecialization)
	 */
	@Override
	public boolean canDecreaseSpecialization(SkillSpecializationValue key) {
		if (key==null) 
			return false;
		if (!model.getSkill(key.getSpecialization().getSkill()).getSpecializations().contains(key))
			return false;

		if (key.getValue()<2)
			return false;
		
		/* Find SpecializationModification */
		SpecializationModification mod = null;
		for (ModificationImpl tmp : undoList) {
			if ((tmp instanceof SpecializationModification) && ((SpecializationModification)tmp).getSpecialization()==key.getSpecialization() && ((SpecializationModification)tmp).getValue()==key.getValue()) {
				mod = (SpecializationModification) tmp;
				break;
			}
		}
		return mod!=null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#increaseSpecialization(org.prelle.ubiquity.SkillSpecialization)
	 */
	@Override
	public boolean increaseSpecialization(SkillSpecializationValue key) {
		if (key==null) 
			return false;
		if (!canIncreaseSpecialization(key)) {
			logger.warn("Trying to increase specialization, which cannot be increased: "+key);
			return false;
		}
		
		// Increase
		key.setValue(key.getValue()+1);
		// Pay EP
		logger.info("Increase specialization '"+key+"' for skill "+key.getSpecialization().getSkill()+" to "+key.getValue());
		int expNeeded = 3;
		model.setExperienceFree(model.getExperienceFree()-expNeeded);
		model.setExperienceInvested(model.getExperienceInvested()+expNeeded);
		
		SpecializationModification mod = new SpecializationModification(key.getSpecialization(), key.getValue());
		mod.setExpCost(expNeeded);
//		masteryUndoStack.get(skill).add(mod);
		undoList.add(mod);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILL_CHANGED, key.getSpecialization().getSkill()));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.UNDO_LIST_CHANGED, undoList));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new int[]{model.getExperienceFree(), model.getExperienceInvested()}));
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#decreaseSpecialization(org.prelle.ubiquity.SkillSpecialization)
	 */
	@Override
	public boolean decreaseSpecialization(SkillSpecializationValue specVal) {
		if (specVal==null) 
			return false;
		if (!canDecreaseSpecialization(specVal))
			return false;

		SkillSpecialization special = specVal.getSpecialization();
		logger.info("decrease specialization "+special);
		Skill skill = special.getSkill();

		/* Find SpecializationModification */
		SpecializationModification mod = null;
		for (ModificationImpl tmp : undoList) {
			if ((tmp instanceof SpecializationModification) && ((SpecializationModification)tmp).getSpecialization()==special  && ((SpecializationModification)tmp).getValue()==specVal.getValue()) {
				mod = (SpecializationModification) tmp;
				break;
			}
		}
		if (mod==null) {
			logger.warn("Trying to deselect an unknown specialization");
			return false;
		}
		
		undoList.remove(mod);
		// Decrease
		specVal.setValue(specVal.getValue()-1);
		// Grant exp
		int expGranted = 3;
		logger.info("Reimburse specialization '"+special+"' for skill "+skill);
		model.setExperienceInvested(model.getExperienceInvested()-expGranted);
		model.setExperienceFree(model.getExperienceFree()+expGranted);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILL_CHANGED, skill));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.UNDO_LIST_CHANGED, undoList));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new int[]{model.getExperienceFree(), model.getExperienceInvested()}));
		return true;
	}

}
