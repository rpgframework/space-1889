/**
 *
 */
package org.prelle.ubiquity.levelling;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.ubiquity.Attribute;
import org.prelle.ubiquity.AttributeValue;
import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.UbiquityRuleset;
import org.prelle.ubiquity.charctrl.AttributeController;
import org.prelle.ubiquity.chargen.event.GenerationEvent;
import org.prelle.ubiquity.chargen.event.GenerationEventDispatcher;
import org.prelle.ubiquity.chargen.event.GenerationEventType;
import org.prelle.ubiquity.modifications.AttributeModification;
import org.prelle.ubiquity.modifications.ModificationImpl;
import org.prelle.ubiquity.modifications.ModificationValueType;

/**
 * @author Stefan
 *
 */
public class AttributeLeveller implements AttributeController {

	private static Logger logger = LogManager.getLogger("ubiquity.chargen");

	private UbiquityRuleset rules;
	private UbiquityCharacter model;

	private List<ModificationImpl> undoList;
	private Map<Attribute, Stack<AttributeModification>> attributeUndoStack;

	//--------------------------------------------------------------------
	/**
	 */
	public AttributeLeveller(UbiquityRuleset rules, UbiquityCharacter model, List<ModificationImpl> undoList) {
		this.rules = rules;
		this.model = model;
		this.undoList = undoList;

		attributeUndoStack = new HashMap<Attribute, Stack<AttributeModification>>();
		for (Attribute key : Attribute.values())
			attributeUndoStack.put(key, new Stack<AttributeModification>());

//		calculateDerived();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.AttributeController#getPointsLeft()
	 */
	@Override
	public int getPointsLeft() {
		return 0;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.AttributeController#canBeDecreased(org.prelle.ubiquity.Attribute)
	 */
	@Override
	public boolean canBeDecreased(Attribute attrib) {
		return !attributeUndoStack.get(attrib).isEmpty();
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeDecreased(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public boolean canBeDecreased(AttributeValue val) {
		if (model.getAttribute(val.getAttribute())!=val)
			return false;
		return !attributeUndoStack.get(val.getAttribute()).isEmpty();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.AttributeController#canBeIncreased(org.prelle.ubiquity.Attribute)
	 */
	@Override
	public boolean canBeIncreased(Attribute key) {
		int current = model.getAttribute(key).getPoints();
		int required= (current+1)*5;

		// TODO Upper limits? Do they even exist?

		return model.getExperienceFree()>=required ;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeIncreased(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public boolean canBeIncreased(AttributeValue val) {
		if (model.getAttribute(val.getAttribute())!=val)
			return false;
		int current = val.getPoints();
		int required= (current+1)*5;

		// TODO Upper limits? Do they even exist?

		return model.getExperienceFree()>=required ;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.AttributeController#getIncreaseCost(org.prelle.ubiquity.Attribute)
	 */
	@Override
	public int getIncreaseCost(Attribute key) {
		AttributeValue set = model.getAttribute(key);

		// Determine new target value
		int newVal = set.getValue()+1;

		// Determine how much exp is needed
		return newVal*5;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.AttributeController#increase(org.prelle.ubiquity.Attribute)
	 */
	@Override
	public boolean increase(Attribute attrib) {
		AttributeValue set = model.getAttribute(attrib);
		return increase(set);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.AttributeController#increase(org.prelle.ubiquity.Attribute)
	 */
	@Override
	public boolean increase(AttributeValue set) {
		if (!canBeIncreased(set)) {
			return false;
		}
		int newVal = set.getValue()+1;
		int expNeeded = getIncreaseCost(set.getAttribute());

			// Update model
			logger.info("Increase "+set.getModifyable()+" from "+set.getValue()+" to "+newVal);
			set.setPoints(newVal);
			model.setExperienceFree(model.getExperienceFree()-expNeeded);
			model.setExperienceInvested(model.getExperienceInvested()+expNeeded);
			// Add to undo list
			AttributeModification mod = new AttributeModification(ModificationValueType.ABSOLUT, set.getAttribute(), newVal);
			mod.setExpCost(expNeeded);
			undoList.add(mod);
			attributeUndoStack.get(set.getAttribute()).push(mod);
			// Inform listener
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, set.getAttribute(), set));
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new int[]{
					model.getExperienceFree(),
					model.getExperienceInvested()
			}));
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.UNDO_LIST_CHANGED, undoList));

			// Update derived attributes
			calculateDerived();
			return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.AttributeController#decrease(org.prelle.ubiquity.Attribute)
	 */
	@Override
	public boolean decrease(Attribute attrib) {
		AttributeValue set = model.getAttribute(attrib);
		return decrease(set);
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#decrease(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public boolean decrease(AttributeValue set) {
		int newVal = set.getPoints()-1;

		if (canBeDecreased(set)) {
			// Update model
			AttributeModification mod = attributeUndoStack.get(set.getAttribute()).pop();
			logger.info("Decrease "+set.getModifyable()+" from "+set.getValue()+" to "+newVal);
			set.setPoints(newVal);
			model.setExperienceFree(model.getExperienceFree()+mod.getExpCost());
			model.setExperienceInvested(model.getExperienceInvested()-mod.getExpCost());
			logger.info("Undo = "+undoList.remove(mod));

			// Inform listener
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, set.getAttribute(), set));
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new int[]{
					model.getExperienceFree(),
					model.getExperienceInvested()
			}));
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.UNDO_LIST_CHANGED, undoList));

			// Update derived attributes
			calculateDerived();
			return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	int get(Attribute attr) {
		return model.getAttribute(attr).getValue();
	}

	//-------------------------------------------------------------------
	void set(Attribute attr, int derivedValue) {
		AttributeValue val = model.getAttribute(attr);
		int oldVal = val.getValue();

		val.setPoints(derivedValue);
//		val.setValue(derivedValue + val.getModifier());

		if (oldVal!=val.getValue())
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, attr, val));
	}

	//-------------------------------------------------------------------
	private void calculateDerived() {
		// If Athletics skill is higher, use it instead of dexterity
			int toAdd = Math.max(
				get(Attribute.DEXTERITY),
				model.getSkill("athletics").getPoints()
				);
		set(Attribute.MOVE      , get(Attribute.STRENGTH) + toAdd);
		set(Attribute.PERCEPTION, get(Attribute.INTELLIGENCE) + get(Attribute.WILLPOWER));

		set(Attribute.INITIATIVE, get(Attribute.DEXTERITY) - get(Attribute.INTELLIGENCE));
		set(Attribute.DEFENSE   , get(Attribute.DEXTERITY) + get(Attribute.BODY) - get(Attribute.SIZE));
		set(Attribute.STUN      , get(Attribute.BODY));
		set(Attribute.HEALTH    , get(Attribute.BODY) + get(Attribute.WILLPOWER) + get(Attribute.SIZE));
	}

	//-------------------------------------------------------------------
	void addModification(AttributeModification mod) {
		logger.debug("addModification "+mod);
		if (mod.getType()==ModificationValueType.RELATIVE) {
			AttributeValue value = model.getAttribute(mod.getAttribute());
			value.addModification(mod);
			calculateDerived();
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, mod.getAttribute(), value));
		} else
			logger.warn("Don't know how to deal with non-relative attribute mods");
	}

	//-------------------------------------------------------------------
	void removeModification(AttributeModification mod) {
		logger.debug("remove Modification");
		if (mod.getType()==ModificationValueType.RELATIVE) {
			AttributeValue value = model.getAttribute(mod.getAttribute());
			value.removeModification(mod);
			calculateDerived();
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, mod.getAttribute(), value));
		}
	}

}
