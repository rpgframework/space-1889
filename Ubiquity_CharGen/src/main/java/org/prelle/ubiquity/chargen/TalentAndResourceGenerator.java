/**
 * 
 */
package org.prelle.ubiquity.chargen;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.ubiquity.GenerationTemplate;
import org.prelle.ubiquity.Resource;
import org.prelle.ubiquity.Resource.Type;
import org.prelle.ubiquity.ResourceValue;
import org.prelle.ubiquity.Skill;
import org.prelle.ubiquity.Talent;
import org.prelle.ubiquity.TalentValue;
import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.UbiquityRuleset;
import org.prelle.ubiquity.charctrl.ResourceController;
import org.prelle.ubiquity.charctrl.TalentController;
import org.prelle.ubiquity.chargen.event.GenerationEvent;
import org.prelle.ubiquity.chargen.event.GenerationEventDispatcher;
import org.prelle.ubiquity.chargen.event.GenerationEventListener;
import org.prelle.ubiquity.chargen.event.GenerationEventType;
import org.prelle.ubiquity.modifications.AttributeModification;
import org.prelle.ubiquity.modifications.SkillModification;
import org.prelle.ubiquity.requirements.Requirement;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author Stefan
 *
 */
public class TalentAndResourceGenerator implements TalentController, ResourceController, GenerationEventListener {

	private static Logger logger = LogManager.getLogger("ubiquity.chargen");

	private UbiquityRuleset rules;
	private GenerationTemplate template;
	private UbiquityCharacter model;
	private CharacterGenerator charGen;
	private double pointsLeft;

	private boolean showUnavailableTalents;
	private boolean showUnavailableResources;

	private List<Talent  > availableTal;
	private List<Resource> availableRes;

	//--------------------------------------------------------------------
	/**
	 * @param template 
	 */
	public TalentAndResourceGenerator(GenerationTemplate template, UbiquityRuleset rules, UbiquityCharacter model, CharacterGenerator charGen) {
		this.model = model;
		this.template = template;
		this.rules = rules;
		this.charGen = charGen;
		pointsLeft = template.getTalentAndResourcePoints();
		availableTal = new ArrayList<>();
		availableRes = new ArrayList<>();

		// Assign base resources
		for (Resource res : rules.getResources()) {
			if (res.getType()==Type.BASE)
				model.addResource(new ResourceValue(res,0));
		}

		updateAvailable();

		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	public void changeTemplate(GenerationTemplate template) {
		if (this.template==template)
			return;

		logger.debug("Before: "+pointsLeft+"/"+this.template.getTalentAndResourcePoints());
		pointsLeft += (template.getTalentAndResourcePoints() - this.template.getTalentAndResourcePoints());
		this.template = template;
		logger.debug("After: "+pointsLeft+"/"+this.template.getTalentAndResourcePoints());
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_TALENTS_RESOURCES, null, pointsLeft));

		updateAvailable();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentAndResourceController#getPointsLeft()
	 */
	@Override
	public double getPointsLeft() {
		return pointsLeft;
	}

	//--------------------------------------------------------------------
	private void updateAvailable() {
		availableTal.clear();
		availableTal.addAll(rules.getTalents());
		// Remove those already selected
		for (TalentValue tmp : model.getTalents()) {
			availableTal.remove(tmp.getTalent());
		}
		// Remove those that cannot be selected
		if (!showUnavailableTalents) {
			for (Talent tmp : new ArrayList<Talent>(availableTal)) {
				if (!canBeSelected(tmp))
					availableTal.remove(tmp);
			}
		}

		// Inform GUI
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.TALENT_AVAILABLE_CHANGED, 
				availableTal));


		availableRes.clear();
		availableRes.addAll(rules.getResources());
		// Remove those already selected
		for (ResourceValue tmp : model.getResources()) {
			availableRes.remove(tmp.getModifyable());
		}
		// Remove those that cannot be selected
		if (!showUnavailableResources) {
			for (Resource tmp : new ArrayList<Resource>(availableRes)) {
				if (!canBeSelected(tmp))
					availableRes.remove(tmp);
			}
		}

		// Inform GUI
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.RESOURCE_AVAILABLE_CHANGED, 
				availableRes));
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentAndResourceController#getAvailableTalents()
	 */
	@Override
	public List<Talent> getAvailable() {
		return availableTal;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentAndResourceController#canBeSelected(org.prelle.ubiquity.Talent)
	 */
	@Override
	public boolean canBeSelected(Talent talent) {
		// Check for enough GP
		if (pointsLeft<1)
			return false;
		// Check if already selected
		for (TalentValue val : model.getTalents()) {
			if (val.getTalent()==talent) {
				if (getOptions(talent).isEmpty())
					return false;
				else
					// More options possible
					break;
			}
		}
		// Are requirements met
		for (Requirement req : talent.getRequirements()) {
			if (!model.meetsRequirement(req))
				return false;
		}

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentAndResourceController#canBeDeSelected(org.prelle.ubiquity.TalentValue)
	 */
	@Override
	public boolean canBeDeselected(TalentValue key) {
		return model.getTalents().contains(key);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentAndResourceController#getOptions(org.prelle.ubiquity.Talent)
	 */
	@Override
	public List<Skill> getOptions(Talent talent) {
		ArrayList<Skill> options = new ArrayList<>(talent.getSkillOptions());

		for (TalentValue check : model.getTalents()) {
			if (check.getTalent()!=talent)
				continue;
			if (check.getSkillOption()!=null) {
				// Talent with this option already chosen
				logger.debug("Talent "+talent.getId()+", option "+check.getSkillOption()+" already chosen");
				options.remove(check.getSkillOption());
			}
		}
		logger.debug("Remaining skill options for "+talent+": "+options);
		return options;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentAndResourceController#select(org.prelle.ubiquity.Talent)
	 */
	@Override
	public TalentValue select(Talent talent) {
		logger.info("Select "+talent);

		if (!canBeSelected(talent))
			return null;

		if (!talent.getSkillOptions().isEmpty()) {
			logger.error("Cannot select "+talent+" without an skill option");
			return null;
		}

		TalentValue val = new TalentValue(talent, 1);
		model.addTalent(val);

		pointsLeft-=1.0;

		// Apply and memorize the talents modifications
		applyTalentModifications(val);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.TALENT_ADDED, 
				val));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.POINTS_LEFT_TALENTS_RESOURCES,
				pointsLeft));
		updateAvailable();
		return val;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentAndResourceController#select(org.prelle.ubiquity.Talent, org.prelle.ubiquity.Skill)
	 */
	@Override
	public TalentValue select(Talent talent, Skill skill) {
		logger.info("Select "+talent);

		if (!canBeSelected(talent))
			return null;

		if (talent.getSkillOptions().isEmpty()) {
			logger.warn("Redirecting to correct select method for "+talent);
			return select(talent);
		}

		TalentValue val = new TalentValue(talent, skill, 1);
		// Apply modifications
		// Specialize modifications, if present
		for (Modification mod : talent.getModifications()) {
			if (mod instanceof SkillModification) {
				SkillModification sMod = (SkillModification)mod;
				if (sMod.getSkill()==Skill.CHOSEN) {
					SkillModification realSMod = new SkillModification(skill, sMod.getValue());
					val.addModification(realSMod);
					logger.debug("Add as "+realSMod);
					charGen.apply(realSMod);
					continue;
				}
			}
			charGen.apply(mod);
		}

		model.addTalent(val);
		logger.info("Selected "+val);

		pointsLeft-=1.0;

		// Apply and memorize the talents modifications
		applyTalentModifications(val);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.TALENT_ADDED, 
				val));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.POINTS_LEFT_TALENTS_RESOURCES,
				pointsLeft));
		updateAvailable();
		return val;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentAndResourceController#deselect(org.prelle.ubiquity.TalentValue)
	 */
	@Override
	public boolean deselect(TalentValue val) {
		logger.info("Deselect "+val);

		if (!canBeDeselected(val))
			return false;

		model.removeTalent(val);

		pointsLeft+=1.0;

		// Undo and forget the talents modifications
		undoTalentModifications(val);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.TALENT_REMOVED, 
				val));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.POINTS_LEFT_TALENTS_RESOURCES, 
				pointsLeft));

		updateAvailable();
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentAndResourceController#canBeDecreased(org.prelle.ubiquity.TalentValue)
	 */
	@Override
	public boolean canBeDecreased(TalentValue key) {
		Talent talent = key.getTalent();
		// Has talent multiple levels?
		if (talent.getLevel()<=1)
			return false;

		return key.getLevel()>=0;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentAndResourceController#canBeIncreased(org.prelle.ubiquity.TalentValue)
	 */
	@Override
	public boolean canBeIncreased(TalentValue key) {
		Talent talent = key.getTalent();
		// Has talent multiple levels?
		if (talent.getLevel()<=1)
			return false;

		if (key.getLevel()>=talent.getLevel())
			return false;

		return pointsLeft>=1.0;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentAndResourceController#increase(org.prelle.ubiquity.TalentValue)
	 */
	@Override
	public boolean increase(TalentValue key) {
		logger.debug("increase "+key);
		if (!canBeIncreased(key))
			return false;

		key.setLevel(key.getLevel()+1);
		pointsLeft-=1.0;

		// Apply and memorize the talents modifications
		undoTalentModifications(key);
		applyTalentModifications(key);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.TALENT_CHANGED, 
				key));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.POINTS_LEFT_TALENTS_RESOURCES, 
				pointsLeft));
		updateAvailable();
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentAndResourceController#decrease(org.prelle.ubiquity.TalentValue)
	 */
	@Override
	public boolean decrease(TalentValue key) {
		logger.debug("idecrease "+key);
		if (!canBeDecreased(key))
			return false;

		if (key.getLevel()==1) {
			return deselect(key);
		}

		key.setLevel(key.getLevel()-1);
		pointsLeft+=1.0;

		undoTalentModifications(key);
		applyTalentModifications(key);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.TALENT_CHANGED, 
				key));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.POINTS_LEFT_TALENTS_RESOURCES, 
				pointsLeft));
		updateAvailable();
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentAndResourceController#getAvailableTalents()
	 */
	@Override
	public List<Resource> getAvailableResources() {
		return availableRes;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentAndResourceController#canBeSelected(org.prelle.ubiquity.Resource)
	 */
	@Override
	public boolean canBeSelected(Resource talent) {
		// Check for enough GP
		if (pointsLeft<0.5)
			return false;
		// Check if already selected
		for (ResourceValue val : model.getResources()) {
			if (val.getModifyable()==talent)
				return false;
		}

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentAndResourceController#canBeDeSelected(org.prelle.ubiquity.ResourceValue)
	 */
	@Override
	public boolean canBeDeSelected(ResourceValue key) {
		if (key.getModifyable().getType()==Type.BASE)
			return false;
		return model.getResources().contains(key);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentAndResourceController#select(org.prelle.ubiquity.Resource)
	 */
	@Override
	public ResourceValue select(Resource talent) {
		logger.info("Select "+talent);

		if (!canBeSelected(talent))
			return null;

		ResourceValue val = new ResourceValue(talent, 0);
		model.addResource(val);

		pointsLeft-=0.5;

		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.RESOURCE_ADDED, 
				val));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.POINTS_LEFT_TALENTS_RESOURCES, 
				pointsLeft));

		updateAvailable();
		return val;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentAndResourceController#deselect(org.prelle.ubiquity.ResourceValue)
	 */
	@Override
	public boolean deselect(ResourceValue val) {
		logger.info("Deselect "+val);

		if (!canBeDeSelected(val))
			return false;

		model.removeResource(val);

		logger.debug("Deselecting "+val+" frees "+getDeselectCost(val));
		pointsLeft += getDeselectCost(val);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.RESOURCE_REMOVED, 
				val));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.POINTS_LEFT_TALENTS_RESOURCES, 
				pointsLeft));

		updateAvailable();
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentAndResourceController#canBeDecreased(org.prelle.ubiquity.ResourceValue)
	 */
	@Override
	public boolean canBeDecreased(ResourceValue key) {
		Resource data = key.getModifyable();
		// Has resource multiple levels?
		int min = (data.getType()==Resource.Type.BASE)?-2:0;

		return key.getPoints()>min;
	}

	//--------------------------------------------------------------------
	private double getIncreaseCost(ResourceValue key) {
		if (key.getPoints()==0 && key.getModifyable().getType()==Type.NORMAL) {
			return 0.5;
		} else
			return 1.0;
	}

	//--------------------------------------------------------------------
	private double getDecreaseCost(ResourceValue key) {
		switch (key.getModifyable().getType()) {
		case BASE:
			return 1.0;
		case NORMAL:
			if (key.getPoints()==1)
				return 0.5;
			return 1.0;		
		}
		return 1.0;		
	}

	//--------------------------------------------------------------------
	private double getDeselectCost(ResourceValue key) {
		if (key.getPoints()==0)
			return 0.5;
		else
			return key.getPoints();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentAndResourceController#canBeIncreased(org.prelle.ubiquity.ResourceValue)
	 */
	@Override
	public boolean canBeIncreased(ResourceValue key) {
		if (key.getPoints()>=5)
			return false;

		// Raising from 0 to 1 costs 0.5, everything else 1
		return pointsLeft >= getIncreaseCost(key);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentAndResourceController#increase(org.prelle.ubiquity.ResourceValue)
	 */
	@Override
	public boolean increase(ResourceValue key) {
		logger.debug("increase "+key);
		if (!canBeIncreased(key))
			return false;

		/*
		 * Decreasing a normal resource from 1 to 0
		 * frees half a point
		 */
		logger.debug("Increasing "+key+" costs "+getIncreaseCost(key));
		pointsLeft -= getIncreaseCost(key);

		key.setPoints(key.getPoints()+1);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.RESOURCE_CHANGED, 
				key));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.POINTS_LEFT_TALENTS_RESOURCES, 
				pointsLeft));
		updateAvailable();
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentAndResourceController#decrease(org.prelle.ubiquity.ResourceValue)
	 */
	@Override
	public boolean decrease(ResourceValue key) {
		logger.debug("try decrease "+key);
		if (!canBeDecreased(key))
			return false;

		/*
		 * Decreasing a normal attribute from 1 to 0
		 * frees half a point
		 */
		logger.debug("Decreasing "+key+" frees "+getDecreaseCost(key));
		pointsLeft += getDecreaseCost(key);
		key.setPoints(key.getPoints()-1);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.RESOURCE_CHANGED, 
				key));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.POINTS_LEFT_TALENTS_RESOURCES, 
				pointsLeft));
		updateAvailable();
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.ubiquity.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case SKILL_CHANGED:
		case SKILL_ADDED:
		case SKILL_REMOVED:
		case ATTRIBUTE_CHANGED:
			logger.debug("RCV "+event.getType()+" - update available");
			updateAvailable();
			break;
		default:
		}

	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentController#hasSkillMaster(org.prelle.ubiquity.Skill)
	 */
	@Override
	public boolean hasSkillMaster(Skill skill) {
		for (TalentValue val : model.getTalents()) {
			if (val.getTalent().getId().equals("skillmaster") && val.getSkillOption()==skill)
				return true;
		}
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.ResourceController#findResourceReference(org.prelle.ubiquity.Resource, java.lang.String, java.lang.String)
	 */
	@Override
	public ResourceValue findResourceReference(Resource res, String descr,
			String idref) {
		for (ResourceValue tmp : model.getResources()) {
			if (tmp.getModifyable()!=res)
				continue;
			if (tmp.getClass()!=null && idref!=null && tmp.getIdReference().equals(idref))
				return tmp;
			if (tmp.getComment()!=null && descr!=null && tmp.getComment().equals(descr))
				return tmp;
		}
		return null;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.ResourceController#trash(org.prelle.ubiquity.ResourceValue)
	 */
	@Override
	public void trash(ResourceValue ref) {
		model.removeResource(ref);
	}

	//--------------------------------------------------------------------
	private void applyTalentModifications(TalentValue val) {
		logger.info("applyTalentModifications "+val);
		List<Modification> toApply = new ArrayList<>();

		for (Modification mod : val.getTalent().getModifications()) {
			if (mod instanceof AttributeModification) {
				AttributeModification origMod = (AttributeModification)mod;
				AttributeModification newMod  = new AttributeModification(
						origMod.getAttribute(),
						origMod.getValue()*val.getLevel()
						);
				newMod.setSource(val);
				newMod.setConditional(origMod.isConditional());
				toApply.add(newMod);
			} else if (mod instanceof SkillModification) {
				SkillModification origMod = (SkillModification)mod;
				SkillModification newMod  = new SkillModification(
						origMod.getSkill(),
						origMod.getValue()*val.getLevel()
						);
				newMod.setSource(val);
				toApply.add(newMod);
			} else {
				if (mod instanceof ValueModification)
					logger.error("Need to do special handling for "+mod.getClass());
				toApply.add(mod);
			}
		}

		rules.getUtilities().applyToCharacter(model, toApply);
		for (Modification mod : toApply)
			 val.addModification(mod);
	}

	//--------------------------------------------------------------------
	private void undoTalentModifications(TalentValue val) {
		logger.debug("undoTalentModifications "+val);
		rules.getUtilities().unapplyToCharacter(model, val.getModifications());
		val.clearModifications();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectionController#getSelected()
	 */
	@Override
	public List<TalentValue> getSelected() {
		return new ArrayList<>(model.getTalents());
	}

	@Override
	public double getSelectionCost(Talent data) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getDeselectionCost(TalentValue value) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ValueModification<Talent> createModification(TalentValue value) {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectionController#needsOptionSelection(de.rpgframework.genericrpg.SelectableItem)
	 */
	@Override
	public boolean needsOptionSelection(Talent toSelect) {
		return toSelect.getSkillOptions()!=null && toSelect.getSkillOptions().size()>0;
	}

	@Override
	public TalentValue select(Talent data, Object option) {
		return select(data, (Skill)option);
	}

}
