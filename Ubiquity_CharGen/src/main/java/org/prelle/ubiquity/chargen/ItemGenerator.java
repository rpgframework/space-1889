/**
 * 
 */
package org.prelle.ubiquity.chargen;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.ubiquity.GenerationTemplate;
import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.UbiquityRuleset;
import org.prelle.ubiquity.charctrl.ItemController;
import org.prelle.ubiquity.chargen.event.GenerationEvent;
import org.prelle.ubiquity.chargen.event.GenerationEventDispatcher;
import org.prelle.ubiquity.chargen.event.GenerationEventType;
import org.prelle.ubiquity.items.CarriedItem;
import org.prelle.ubiquity.items.ItemTemplate;

import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author prelle
 *
 */
public class ItemGenerator implements ItemController {

	private static Logger logger = LogManager.getLogger("ubiquity.chargen");
	
	private UbiquityRuleset rules;
	private UbiquityCharacter model;

	//-------------------------------------------------------------------
	public ItemGenerator(GenerationTemplate template, UbiquityRuleset rules, UbiquityCharacter model) {
		this.rules = rules;
		this.model = model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectionController#getAvailable()
	 */
	@Override
	public List<ItemTemplate> getAvailable() {
		return rules.getItemTemplates();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectionController#getSelected()
	 */
	@Override
	public List<CarriedItem> getSelected() {
		return model.getItems();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectionController#getSelectionCost(de.rpgframework.genericrpg.SelectableItem)
	 */
	@Override
	public double getSelectionCost(ItemTemplate data) {
		return data.getPrice();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectionController#getDeselectionCost(de.rpgframework.genericrpg.SelectedValue)
	 */
	@Override
	public double getDeselectionCost(CarriedItem value) {
		return value.getCount()*value.getItem().getPrice();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectionController#canBeSelected(de.rpgframework.genericrpg.SelectableItem)
	 */
	@Override
	public boolean canBeSelected(ItemTemplate data) {
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectionController#canBeDeselected(de.rpgframework.genericrpg.SelectedValue)
	 */
	@Override
	public boolean canBeDeselected(CarriedItem value) {
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectionController#createModification(de.rpgframework.genericrpg.SelectedValue)
	 */
	@Override
	public ValueModification<ItemTemplate> createModification(CarriedItem value) {
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectionController#select(de.rpgframework.genericrpg.SelectableItem)
	 */
	@Override
	public CarriedItem select(ItemTemplate data) {
		CarriedItem item = new CarriedItem(data);
		logger.info("Remove "+item);
		model.addItem(item);
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EQUIPMENT_ADDED, item));
		return item;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectionController#deselect(de.rpgframework.genericrpg.SelectedValue)
	 */
	@Override
	public boolean deselect(CarriedItem item) {
		logger.info("Remove "+item);
		model.removeItem(item);
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EQUIPMENT_REMOVED, item));
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectionController#needsOptionSelection(de.rpgframework.genericrpg.SelectableItem)
	 */
	@Override
	public boolean needsOptionSelection(ItemTemplate toSelect) {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectionController#getOptions(de.rpgframework.genericrpg.SelectableItem)
	 */
	@Override
	public List<?> getOptions(ItemTemplate toSelect) {
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectionController#select(de.rpgframework.genericrpg.SelectableItem, java.lang.Object)
	 */
	@Override
	public CarriedItem select(ItemTemplate data, Object option) {
		return null;
	}

}
