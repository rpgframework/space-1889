/**
 * 
 */
package org.prelle.ubiquity.chargen.event;

/**
 * @author prelle
 *
 */
public enum GenerationEventType {
	
	/** Key is null, Value is Array [ExpFree, ExpInvested] */
	EXPERIENCE_CHANGED,
	/** Key is null, value is an Integer */
	POINTS_LEFT_ATTRIBUTES,
	/** Key is null, value is an Integer */
	POINTS_LEFT_SKILLS,
	/** Key is null, value is an Integer */
	POINTS_LEFT_TALENTS_RESOURCES,

	/** Key is the archetype */
	ARCHETYPE_CHANGED,
	
	/** Key is the motivation */
	MOTIVATION_CHANGED,
	
	/** Key is Attribute, Value is AttributeValue */
	ATTRIBUTE_CHANGED,
	
	/** List of available skills changed. Key = Array
	 * Index 0 = List<Skill> Current available
	 * Index 1 = List<Skill> Added
	 * Index 2 = List<Skill> Removed
	 * */
	SKILL_AVAILABLE_CHANGED, 	
	/** Key is Skill */
	SKILL_ADDED,
	/** Key is Skill, value is SkillValue */
	SKILL_REMOVED,
	/** Key is Skill, Value is distributed points */
	SKILL_CHANGED, 
	
	/** List of available talents changed. Key = Collection<TalentValue> */
	TALENT_AVAILABLE_CHANGED, 
	/** Key is TalentValue*/
	TALENT_ADDED,
	/** Key is TalentValue*/
	TALENT_REMOVED,
	/** Key is TalentValue*/
	TALENT_CHANGED,
	
	/** List of available resources changed. Key = Collection<ResourceValue> */
	RESOURCE_AVAILABLE_CHANGED, 
	/** Key is ResourceValue*/
	RESOURCE_ADDED,
	/** Key is ResourceValue*/
	RESOURCE_REMOVED,
	/** Key is ResourceValue*/
	RESOURCE_CHANGED,
	
	/** List of available flaws changed. Key = Collection<Flaw> */
	FLAW_AVAILABLE_CHANGED, 
	/** Key is FlawValue*/
	FLAW_ADDED,
	/** Key is FlawValue*/
	FLAW_REMOVED,

	/** Name, Gender, Weight, Size or colors changed. Value is NULL*/
	BASE_DATA_CHANGED,
	UNDO_LIST_CHANGED,
	/** Generation template changed. Key = GenerationTemplate */
	TEMPLATE_CHANGED,
	
	/** Key is CarriedItem */
	EQUIPMENT_ADDED,
	/** Key is CarriedItem */
	EQUIPMENT_REMOVED,
	/** Key is CarriedItem */
	EQUIPMENT_MODIFIED,
	
	/** Character notes changed */
	NOTES_CHANGED
}
