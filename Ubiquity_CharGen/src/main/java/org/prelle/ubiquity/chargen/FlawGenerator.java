/**
 * 
 */
package org.prelle.ubiquity.chargen;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.ubiquity.Flaw;
import org.prelle.ubiquity.FlawValue;
import org.prelle.ubiquity.GenerationTemplate;
import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.UbiquityRuleset;
import org.prelle.ubiquity.charctrl.FlawController;
import org.prelle.ubiquity.chargen.event.GenerationEvent;
import org.prelle.ubiquity.chargen.event.GenerationEventDispatcher;
import org.prelle.ubiquity.chargen.event.GenerationEventType;

import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author Stefan
 *
 */
public class FlawGenerator implements FlawController {
	
	private static Logger logger = LogManager.getLogger("ubiquity.chargen");

	private UbiquityRuleset rules;
	private GenerationTemplate template;
	private UbiquityCharacter model;

	private boolean gmAdmission;
	private List<Flaw> available;

	//--------------------------------------------------------------------
	public FlawGenerator(GenerationTemplate template, UbiquityRuleset rules, UbiquityCharacter model) {
		this.model = model;
		this.template = template;
		this.rules = rules;
		available = new ArrayList<>();
		
		updateAvailable();
	}

	//--------------------------------------------------------------------
	private void updateAvailable() {
		available.clear();
		if (model.getFlaws().isEmpty() || gmAdmission) {
			outer:
			for (Flaw flaw : rules.getFlaws()) {
				// Ignore those flaws already selected
				for (FlawValue selected : model.getFlaws()) {
					if (selected.getFlaw()==flaw)
						continue outer;
				}
				available.add(flaw);
			}
		}
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.FLAW_AVAILABLE_CHANGED, 
				available));		
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.FlawController#getAvailableFlaws()
	 */
	@Override
	public List<Flaw> getAvailable() {
		return available;
	}

	//--------------------------------------------------------------------
	private boolean isAlreadySelected(Flaw flaw) {
		for (FlawValue check : model.getFlaws()) {
			if (check.getFlaw()==flaw)
				return true;
		}
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.FlawController#canBeSelected(org.prelle.ubiquity.Flaw)
	 */
	@Override
	public boolean canBeSelected(Flaw flaw) {
		// Compare with already selected flaws
		if (isAlreadySelected(flaw))
			return false;
		
		// Selecting more than one flaw at beginning requires
		// gamemaster admission
		if (!model.getFlaws().isEmpty())
			return gmAdmission;
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.FlawController#canBeDeSelected(org.prelle.ubiquity.FlawValue)
	 */
	@Override
	public boolean canBeDeselected(FlawValue key) {
		return isAlreadySelected(key.getFlaw());
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.FlawController#select(org.prelle.ubiquity.Flaw)
	 */
	@Override
	public FlawValue select(Flaw data) {
		logger.debug("Select "+data);
		if (!canBeSelected(data))
			return null;

		FlawValue val = new FlawValue(data);
		model.addFlaw(val);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.FLAW_ADDED, 
				val));

		updateAvailable();
		return val;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.FlawController#deselect(org.prelle.ubiquity.FlawValue)
	 */
	@Override
	public boolean deselect(FlawValue data) {
		logger.debug("Deselect "+data);
		if (!canBeDeselected(data))
			return false;
		
		model.removeFlaw(data);
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.FLAW_REMOVED, 
				data));
		
		updateAvailable();
		return true;
	}

	//--------------------------------------------------------------------
	public void setAdmission(boolean admission) {
		gmAdmission = admission;
		updateAvailable();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectionController#getSelected()
	 */
	@Override
	public List<FlawValue> getSelected() {
		return model.getFlaws();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectionController#getSelectionCost(de.rpgframework.genericrpg.SelectableItem)
	 */
	@Override
	public double getSelectionCost(Flaw data) {
		// TODO Auto-generated method stub
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectionController#getDeselectionCost(de.rpgframework.genericrpg.SelectedValue)
	 */
	@Override
	public double getDeselectionCost(FlawValue value) {
		// TODO Auto-generated method stub
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectionController#createModification(de.rpgframework.genericrpg.SelectedValue)
	 */
	@Override
	public ValueModification<Flaw> createModification(FlawValue value) {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectionController#needsOptionSelection(de.rpgframework.genericrpg.SelectableItem)
	 */
	@Override
	public boolean needsOptionSelection(Flaw toSelect) {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectionController#getOptions(de.rpgframework.genericrpg.SelectableItem)
	 */
	@Override
	public List<?> getOptions(Flaw toSelect) {
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectionController#select(de.rpgframework.genericrpg.SelectableItem, java.lang.Object)
	 */
	@Override
	public FlawValue select(Flaw data, Object option) {
		return null;
	}

}
