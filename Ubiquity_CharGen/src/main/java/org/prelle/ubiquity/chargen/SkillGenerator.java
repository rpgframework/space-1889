/**
 * 
 */
package org.prelle.ubiquity.chargen;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.ubiquity.GenerationTemplate;
import org.prelle.ubiquity.Skill;
import org.prelle.ubiquity.SkillSpecialization;
import org.prelle.ubiquity.SkillSpecializationValue;
import org.prelle.ubiquity.SkillValue;
import org.prelle.ubiquity.Talent;
import org.prelle.ubiquity.TalentValue;
import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.UbiquityRuleset;
import org.prelle.ubiquity.charctrl.SkillController;
import org.prelle.ubiquity.chargen.event.GenerationEvent;
import org.prelle.ubiquity.chargen.event.GenerationEventDispatcher;
import org.prelle.ubiquity.chargen.event.GenerationEventListener;
import org.prelle.ubiquity.chargen.event.GenerationEventType;
import org.prelle.ubiquity.modifications.SkillModification;

/**
 * @author prelle
 *
 */
public class SkillGenerator implements SkillController, GenerationEventListener {
	
	private static Logger logger = LogManager.getLogger("ubiquity.chargen");

	private UbiquityRuleset   rules;
	private GenerationTemplate template;
	private UbiquityCharacter model;
	private double pointsLeft;
	
	private List<Skill> unselected;
	private List<Skill> skills;
	private Map<Skill, Collection<SkillModification>> modifications;

	//-------------------------------------------------------------------
	/**
	 * @param template 
	 */
	public SkillGenerator(GenerationTemplate template, UbiquityRuleset rules, UbiquityCharacter model) {
		this.model    = model;
		this.template = template;
		this.rules    = rules;
		pointsLeft    = template.getSkillPoints();
		
		skills = new ArrayList<Skill>();
		unselected = new ArrayList<Skill>();
		modifications = new HashMap<Skill, Collection<SkillModification>>();
		try {
			for (Skill skill : rules.getSkills()) {
				unselected.add(skill);
				modifications.put(skill, new ArrayList<>());
			}
		} catch (Throwable e) {
			logger.error("Failed loading skills",e);
		}
		updateAvailable();
		
		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	public UbiquityRuleset getRuleset() {
		return rules;
	}

	//-------------------------------------------------------------------
	public void changeTemplate(GenerationTemplate template) {
		if (this.template==template)
			return;
		
		logger.debug("Before: "+pointsLeft+"/"+this.template.getSkillPoints());
		pointsLeft += (template.getSkillPoints() - this.template.getSkillPoints());
		this.template = template;
		logger.debug("After: "+pointsLeft+"/"+this.template.getSkillPoints());
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SKILLS, null, pointsLeft));
	}

	//-------------------------------------------------------------------
	private boolean hasSkillMaster(Skill skillGroup) {
		for (TalentValue tVal : model.getTalents()) {
			Talent talent = tVal.getTalent();
			if (talent.getId().equals("skillmaster")) {
				if (tVal.getSkillOption()==skillGroup)
					return true;
			}
		}
		return false;
	}

	//-------------------------------------------------------------------
	private void updateAvailable() {
		logger.debug("updateAvailable");
		
		List<Skill> added = new ArrayList<>();
		List<Skill> removed = new ArrayList<>();
		for (Skill skill : rules.getSkills()) {
			// Non group skills are simply added
			if (!skill.isGroup() && !skill.isPartOfGroup()) {
				if (!skills.contains(skill)) {
					skills.add(skill);
					added.add(skill);
				}					
			} else if (skill.isPartOfGroup()) {
				/* 
				 * This is part of a skill group. It can be
				 * added, unless the skillmaster talent is 
				 * present for this group
				 */
				Skill skillGroup = skill.getParentSkill();
				if (hasSkillMaster(skillGroup)) {
					// Skillmaster present - sub-skills may not be added
					// or must be removed
					if (skills.contains(skill)) {
						skills.remove(skill);
						removed.add(skill);
					} 
					if (model.getSkill(skill)!=null) {
						pointsLeft += model.getSkill(skill).getPoints();
						model.removeSkill(model.getSkill(skill));
						GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILL_REMOVED, skill));
						GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SKILLS, null, pointsLeft));
					}
				} else {
					// Skillmaster not present. May be added
					if (!skills.contains(skill)) {
						skills.add(skill);
						added.add(skill);
					}					
				}
			} else if (skill.isGroup()) {
				/*
				 * This is a group. It may only be added if
				 * the corresponding skillmaster talent is
				 * present
				 */
				if (hasSkillMaster(skill)) {
					// Skillmaster present. May be added
					if (!skills.contains(skill)) {
						skills.add(skill);
						added.add(skill);
					}										
				} else {
					// Skillmaster not present. Must not be
					// added and must be removed if present
					if (skills.contains(skill)) {
						skills.remove(skill);
						removed.add(skill);
					} 
					if (model.getSkill(skill)!=null) {
						pointsLeft += model.getSkill(skill).getPoints();
						model.removeSkill(model.getSkill(skill));
						GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILL_REMOVED, skill));
						GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SKILLS, null, pointsLeft));
					}
				}
			}
		}
		logger.debug("Available skills added: "+added);
		logger.debug("Available skills removed: "+removed);
		
		if (added.isEmpty() && removed.isEmpty()) {
			// Nothing changed
			logger.debug("Nothing changed");
			return;
		}
		logger.debug("Some changes");
		List<Skill>[] ret = new List[]{skills,added,removed};
		GenerationEventDispatcher.fireEvent(
				new GenerationEvent(
						GenerationEventType.SKILL_AVAILABLE_CHANGED, 
						ret));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#getAvailableSkills()
	 */
	@Override
	public List<Skill> getAvailableSkills() {
		return skills;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#getPointsLeft()
	 */
	@Override
	public double getPointsLeft() {
		return pointsLeft;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#canBeDecreased(org.prelle.ubiquity.SkillValue)
	 */
	@Override
	public boolean canBeDecreased(Skill key) {
		if (pointsLeft>=15)
			return false;
		if (model.getSkill(key)==null)
			return false;
		return model.getSkill(key).getPoints()>0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#canBeIncreased(org.prelle.ubiquity.SkillValue)
	 */
	@Override
	public boolean canBeIncreased(Skill key) {
		// No more points for increasing
		if (pointsLeft==0)
			return false;
		// Is not a valid skill
		if (!skills.contains(key))
			return false;
		// Not present yet
		if (model.getSkill(key)==null)
			return true;
		// Invested points below maximum
		return model.getSkill(key).getPoints()<template.getSkillPointMax();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#getIncreaseCost(org.prelle.ubiquity.SkillValue)
	 */
	@Override
	public int getIncreaseCost(SkillValue key) {
		return 1;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#increase(org.prelle.ubiquity.SkillValue)
	 */
	@Override
	public boolean increase(Skill skill) {
		logger.debug("increase "+skill);
		if (!canBeIncreased(skill))
			return false;
		SkillValue key = model.getSkill(skill);
		if (key==null) {
			key = new SkillValue(skill, 0);
			model.addSkill(key);
		}
		
		pointsLeft--;
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SKILLS, null, pointsLeft));
		logger.debug("Increase from "+key.getPoints());
		key.setPoints(key.getPoints()+1);
		logger.info("increase "+key.getModifyable()+" to "+key.getPoints());
		if (key.getPoints()==1) {
			model.addSkill(key);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILL_ADDED, skill, key));
		} else {
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILL_CHANGED, skill, key.getPoints()));
		}

		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#decrease(org.prelle.ubiquity.SkillValue)
	 */
	@Override
	public boolean decrease(Skill skill) {
		if (!canBeDecreased(skill))
			return false;
		SkillValue key = model.getSkill(skill);
		
		pointsLeft++;
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SKILLS, null, pointsLeft));
		key.setPoints(key.getPoints()-1);
		logger.info("decrease "+key.getModifyable()+" to "+key.getPoints());

		if (key.getPoints()==0) {			
			model.removeSkill(key);
			// Free points for specializations
			if (!key.getSpecializations().isEmpty()) {
				pointsLeft += key.getSpecializations().size() * 0.5;
				GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SKILLS, null, pointsLeft));
			}
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILL_REMOVED, skill));
		} else
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILL_CHANGED, skill));

		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#getAvailableSpecializations(org.prelle.ubiquity.Skill)
	 */
	@Override
	public List<SkillSpecialization> getAvailableSpecializations(Skill key) {
		List<SkillSpecialization> ret = new ArrayList<>(key.getSpecializations());
		ret.removeAll(model.getSkill(key).getSpecializations());
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#canSelectSpecialization(org.prelle.ubiquity.SkillSpecialization)
	 */
	@Override
	public boolean canSelectSpecialization(SkillSpecialization spec) {
		logger.debug("canSelectSpec("+spec+")");
		if (pointsLeft==0)
			return false;
		
		// Need at least one point in the skill
		SkillValue sVal = model.getSkill(spec.getSkill());
		if (sVal==null) {
			// Skill not present
			return false;
		}
		// Should not happen, but present skills with 0 points
		// don't work either
		if (sVal.getPoints()==0)
			return false;
		
		// During generation only one specialization is allowed
		// Also return false, if specialization already exists
		if (!sVal.getSpecializations().isEmpty())
			return false;
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#canSelectSpecialization(org.prelle.ubiquity.SkillSpecialization)
	 */
	@Override
	public boolean canDeselectSpecialization(SkillSpecializationValue key) {
		return model.getSkill(key.getSpecialization().getSkill()).getSpecializations().contains(key);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#selectSpecialization(org.prelle.ubiquity.SkillSpecialization)
	 */
	@Override
	public SkillSpecializationValue selectSpecialization(SkillSpecialization spec) {
		logger.debug("selectSpecialization("+spec+")");
		if (!canSelectSpecialization(spec))
			return null;

		SkillValue value = model.getSkill(spec.getSkill());
		SkillSpecializationValue specVal = new SkillSpecializationValue(spec, 1);
		value.addSpecialization(specVal);
		logger.info("Added specialization "+spec+" in "+value);

		pointsLeft -= 0.5;
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SKILLS, null, pointsLeft));
		GenerationEvent event = new GenerationEvent(GenerationEventType.SKILL_CHANGED, spec.getSkill());
		GenerationEventDispatcher.fireEvent(event);
		return specVal;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#deselectSpecialization(org.prelle.ubiquity.SkillSpecialization)
	 */
	@Override
	public boolean deselectSpecialization(SkillSpecializationValue key) {
		logger.debug("deselectSpecialization "+key);
		if (!canDeselectSpecialization(key))
			return false;

		model.getSkill(key.getSpecialization().getSkill()).removeSpecialization(key);
		logger.info("Removed specialization "+key);

		pointsLeft += 0.5;

		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SKILLS, null, pointsLeft));
		GenerationEvent event = new GenerationEvent(GenerationEventType.SKILL_CHANGED, key.getSpecialization().getSkill());
		GenerationEventDispatcher.fireEvent(event);
		return true;
	}

	//-------------------------------------------------------------------
	public void addModification(SkillModification mod) {
		logger.debug("addModification");
		Collection<SkillModification> modList = modifications.get(mod.getSkill());		
		modList.add(mod);

		SkillValue sVal = model.getSkill(mod.getSkill());
		sVal.addModification(mod);
		GenerationEvent event = new GenerationEvent(GenerationEventType.SKILL_CHANGED, sVal.getModifyable(), sVal.getPoints());
		GenerationEventDispatcher.fireEvent(event);
	}

	//-------------------------------------------------------------------
	public void removeModification(SkillModification mod) {
		logger.debug("remove Modification");
		Collection<SkillModification> modList = modifications.get(mod.getSkill());		
		modList.remove(mod);
		
		SkillValue sVal = model.getSkill(mod.getSkill());
		sVal.removeModification(mod);
		GenerationEvent event = new GenerationEvent(GenerationEventType.SKILL_CHANGED, sVal.getModifyable(), sVal.getPoints());
		GenerationEventDispatcher.fireEvent(event);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.ubiquity.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		TalentValue tVal = null;
		Talent talent = null;
		switch (event.getType()) {
		case TALENT_ADDED:
			logger.debug("RCV "+event);
			// In case of a skillmaster talent, memorize
			tVal = (TalentValue)event.getKey();
			talent = tVal.getTalent();
			int highest = 0;
			if (talent.getId().equals("skillmaster")) {
				// Search the highest value in subskills to remove
				for (Skill child : tVal.getSkillOption().getChildSkills()) {
					SkillValue sVal = model.getSkill(child);
					logger.debug(" "+child+" = "+sVal);
					if (sVal!=null) {
						highest = Math.max(highest, sVal.getPoints());
					}
				}
				updateAvailable();
				logger.info("Auto-select "+tVal.getSkillOption()+" on "+highest);
				for (int i=0; i<highest; i++)
					increase(tVal.getSkillOption());
			} else
				updateAvailable();
			break;
		case TALENT_REMOVED:
			updateAvailable();
			break;
		case SKILL_AVAILABLE_CHANGED:
			// 
		}
		
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		// TODO Auto-generated method stub
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#canIncreaseSpecialization(org.prelle.ubiquity.SkillSpecialization)
	 */
	@Override
	public boolean canIncreaseSpecialization(SkillSpecializationValue key) {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#canDecreaseSpecialization(org.prelle.ubiquity.SkillSpecialization)
	 */
	@Override
	public boolean canDecreaseSpecialization(SkillSpecializationValue key) {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#increaseSpecialization(org.prelle.ubiquity.SkillSpecialization)
	 */
	@Override
	public boolean increaseSpecialization(SkillSpecializationValue key) {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.SkillController#decreaseSpecialization(org.prelle.ubiquity.SkillSpecialization)
	 */
	@Override
	public boolean decreaseSpecialization(SkillSpecializationValue key) {
		return false;
	}

}
