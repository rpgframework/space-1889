/**
 * 
 */
package org.prelle.ubiquity.chargen;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.ubiquity.Archetype;
import org.prelle.ubiquity.GenerationTemplate;
import org.prelle.ubiquity.Motivation;
import org.prelle.ubiquity.RewardImpl;
import org.prelle.ubiquity.Talent;
import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.UbiquityCore;
import org.prelle.ubiquity.UbiquityRuleset;
import org.prelle.ubiquity.charctrl.AttributeController;
import org.prelle.ubiquity.charctrl.CharGenMode;
import org.prelle.ubiquity.charctrl.CharacterController;
import org.prelle.ubiquity.charctrl.FlawController;
import org.prelle.ubiquity.charctrl.ItemController;
import org.prelle.ubiquity.charctrl.ResourceController;
import org.prelle.ubiquity.charctrl.SkillController;
import org.prelle.ubiquity.charctrl.TalentController;
import org.prelle.ubiquity.chargen.event.GenerationEvent;
import org.prelle.ubiquity.chargen.event.GenerationEventDispatcher;
import org.prelle.ubiquity.chargen.event.GenerationEventType;
import org.prelle.ubiquity.modifications.AttributeModification;
import org.prelle.ubiquity.modifications.ModificationChoice;
import org.prelle.ubiquity.modifications.ModificationImpl;
import org.prelle.ubiquity.modifications.SkillModification;
import org.prelle.ubiquity.requirements.Requirement;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class CharacterGenerator implements CharacterController {
	
	private static Logger logger = LogManager.getLogger("ubiquity.chargen");
	
	private UbiquityRuleset rules;
	
	private AttributeGenerator attributes;
	private SkillGenerator skills;
	private TalentAndResourceGenerator talentsAndResources;
	private FlawController flaws;
	private ItemController items;
	
	/**
	 * Memorizes all choices made by the user. 
	 */
	private Map<ModificationChoice, ModificationImpl[]> choices;
	private UbiquityCharacter model;

	private GenerationTemplate template;
	
	//-------------------------------------------------------------------
	/**
	 */
	public CharacterGenerator(UbiquityRuleset rules, UbiquityCharacter model) {
		this.model = model;
		this.rules = rules;
		if (rules==null)
			throw new NullPointerException("No ruleset given");
		
		choices    = new HashMap<ModificationChoice, ModificationImpl[]>();

		template = rules.getDefaultTemplates();
		attributes = new AttributeGenerator(template, rules, model);
		skills     = new SkillGenerator(template, rules, model);
		talentsAndResources = new TalentAndResourceGenerator(template, rules, model, this);
		flaws      = new FlawGenerator(template, rules, model);
		items      = new ItemGenerator(template, rules, model);
		
		rules.getUtilities().calculateSecondaryAttributes(model);
	}
	
	//-------------------------------------------------------------------
	public GenerationTemplate getTemplate() {
		return template;
	}
	
	//-------------------------------------------------------------------
	public void setTemplate(GenerationTemplate template) {
		logger.info("setTemplate("+template+")");
		this.template = template;
		((AttributeGenerator)attributes).changeTemplate(template);
		((SkillGenerator)skills).changeTemplate(template);
		((TalentAndResourceGenerator)talentsAndResources).changeTemplate(template);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.TEMPLATE_CHANGED, template, null));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.CharacterController#getModel()
	 */
	@Override
	public UbiquityCharacter getModel() {
		return model;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.CharacterController#getMode()
	 */
	@Override
	public CharGenMode getMode() {
		return CharGenMode.CREATING;
	}
	
	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.CharacterController#getRuleset()
	 */
	public UbiquityRuleset getRuleset() {
		return rules;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.CharacterController#apply(org.prelle.ubiquity.modifications.ModificationImpl)
	 */
	@Override
	public void apply(Modification mod) {
		logger.debug("apply "+mod);
		if (mod instanceof AttributeModification) {
			attributes.addModification((AttributeModification) mod);
		} else if (mod instanceof SkillModification) {
				skills.addModification((SkillModification) mod);
		} else {
			logger.warn("Don't know how to apply modification of type "+mod.getClass());
			System.exit(0);
		}
	}

	//-------------------------------------------------------------------
	public void apply(String origin, Collection<ModificationImpl> modsToApply) {//, LetUserChooseListener callback) {
		for (ModificationImpl mod : modsToApply) {
			try {
//				// Check if modification is a choice - if so, ask user
//				if (mod instanceof ModificationChoice) {
//					logger.debug("Let user choose: "+mod);
//					Modification[] madeChoices = callback.letUserChoose(origin, (ModificationChoice) mod);
//					if (madeChoices==null) 
//						throw new IllegalArgumentException("No choice has been made on "+mod);
//					
//					/* 
//					 * Eventually a MastershipModification with a skill type
//					 * is selected. In this case build another choice
//					 */
//					for (Modification mod2 : madeChoices) {
//						if (mod2 instanceof MastershipModification) { 
//							MastershipModification mMod = (MastershipModification)mod2;
//							if (mMod.getSkillType()==null) continue;
//							// Build a ModificationChoice for the skill type
//							List<Modification> mods = new ArrayList<Modification>();
//							for (Skill skill : SplitterMondCore.getSkills(mMod.getSkillType())) {
//								if (model.getSkillValue(skill).getValue()>0)
//									mods.add(new MastershipModification(skill, mMod.getLevel()));
//							}
//							ModificationChoice modC = new ModificationChoice(mods, 1);
//							madeChoices = callback.letUserChoose(origin, modC);
//							if (madeChoices==null) 
//								throw new IllegalArgumentException("No choice has been made on "+mod);
//							logger.debug("made choices : "+Arrays.toString(madeChoices));
//						}
//					}
//					
//					choices.put((ModificationChoice) mod, madeChoices);
//					for (Modification choice : madeChoices) {
//						apply(choice);
//					}
//				} else if (mod instanceof MastershipModification && ((MastershipModification)mod).getSkillType()!=null) {
//					MastershipModification mMod = (MastershipModification)mod;
//					logger.debug("Select mastership of type "+mMod.getSkillType()+" for a selected skill");
//					// Build a ModificationChoice for the skill type
//					List<Modification> mods = new ArrayList<Modification>();
//					for (Skill skill : SplitterMondCore.getSkills(mMod.getSkillType())) {
//						if (model.getSkillValue(skill).getValue()>0)
//							mods.add(new MastershipModification(skill, mMod.getLevel()));
//					}
//					ModificationChoice modC = new ModificationChoice(mods, 1);
//					Modification[] madeChoices = callback.letUserChoose(origin, modC);
//					if (madeChoices==null) 
//						throw new IllegalArgumentException("No choice has been made on "+mod);
//					logger.debug("made choices : "+Arrays.toString(madeChoices));
//					
//					choices.put(modC, madeChoices);
//					for (Modification choice : madeChoices) {
//						apply(choice);
//					}
//				} else {
					apply(mod);
//				}
			} catch (Exception e) {
				logger.error("Failed applying modification "+mod,e);
			}
		}
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.CharacterController#undo(org.prelle.ubiquity.modifications.ModificationImpl)
	 */
	@Override
	public void undo(Modification mod) {
		logger.debug("undo "+mod);
		if (mod instanceof AttributeModification) {
			attributes.removeModification((AttributeModification) mod);
//		} else if (mod instanceof RaceModification) {
//		} else if (mod instanceof PowerModification) {
//			powers.removeModification((PowerModification) mod);
//		} else if (mod instanceof SkillModification) {
//			skills.removeModification((SkillModification) mod);
//		} else if (mod instanceof MastershipModification) {
//			master.removeModification((MastershipModification) mod);
//		} else if (mod instanceof PointsModification) {
//			PointsModification pMod = (PointsModification)mod;
//			switch (pMod.getType()) {
//			case POWER:
//				powers.removeModification(pMod);
//				break;
//			default:
//				logger.error("Unsupported PointsModification type "+pMod.getType());
//				System.exit(0);
//			}
//		} else if (mod instanceof ResourceModification) {
//			resources.removeModification((ResourceModification) mod);
//		} else if (mod instanceof BackgroundModification) {
//			backgrounds.removeModification((BackgroundModification) mod);
//		} else if (mod instanceof NotBackgroundModification) {
//			backgrounds.removeModification((NotBackgroundModification) mod);
//		} else if (mod instanceof RequirementModification) {
//			RequirementModification req = (RequirementModification)mod;
//			logger.warn("Don't know how to undo requirement for type "+req.getType());
//		} else if (mod instanceof CultureLoreModification) {
//			model.removeCultureLore( new CultureLoreReference(((CultureLoreModification)mod).getData()) );
//		} else if (mod instanceof LanguageModification) {
//			model.removeLanguage( new LanguageReference(((LanguageModification)mod).getData()) );
		} else {
			logger.warn("Don't know how to undo modification of type "+mod.getClass()+": "+mod);
			System.exit(0);
		}
	}

	//-------------------------------------------------------------------
	public void undo(Collection<ModificationImpl> modsToUndo) {
		logger.debug("undo "+modsToUndo);
		for (ModificationImpl mod : modsToUndo) {
			// Check if modification is a choice - if so, ask user
			if (mod instanceof ModificationChoice) {
				ModificationImpl[] madeChoices = choices.get((ModificationChoice) mod);
				choices.remove((ModificationChoice) mod);
				
				if (madeChoices==null) {
					logger.warn("Trying to undo choices that are not registed: "+mod);
					continue;
				}
					
				for (ModificationImpl choice : madeChoices) {
					undo(choice);
				}
			} else {
				undo(mod);
			}
			
		}
	}

	//-------------------------------------------------------------------
	public Archetype getSelectedArchetype() {
		return model.getArchetype();
	}

	//-------------------------------------------------------------------
	public void selectArchetype(Archetype selected) {
		if (selected==model.getArchetype()) {
			// Nothing changed
			return;
		}
		
		model.setArchetype(selected);
		logger.info("Selected archetype is now "+selected);

		/*
		 * Inform listener
		 */
		GenerationEvent event = new GenerationEvent(GenerationEventType.ARCHETYPE_CHANGED, selected);
		GenerationEventDispatcher.fireEvent(event);
	}

	//-------------------------------------------------------------------
	public Motivation getSelectedMotivation() {
		return model.getMotivation();
	}

	//-------------------------------------------------------------------
	public void selectMotivation(Motivation selected) {
		if (selected==getSelectedMotivation()) {
			// Nothing changed
			return;
		}
		
		model.setMotivation(selected);
		logger.info("Selected motivation is now "+selected);

		/*
		 * Inform listener
		 */
		GenerationEvent event = new GenerationEvent(GenerationEventType.MOTIVATION_CHANGED, selected);
		GenerationEventDispatcher.fireEvent(event);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.splimo.charctrl.CharacterController#getAttributeController()
	 */
	@Override
	public AttributeController getAttributeController() {
		return attributes;
	}

	//--------------------------------------------------------------------
	public boolean canBeFinished() {
		logger.debug(String.format("archtype=%s motiv=%s attrib=%d  skills=%f  talents=%f  name=%s", 
				model.getArchetype(), model.getMotivation(), attributes.getPointsLeft(),
				skills.getPointsLeft(), talentsAndResources.getPointsLeft(), model.getName()));
		if (model.getArchetype()==null) return false;
		if (model.getMotivation()==null) return false;
		if (attributes.getPointsLeft()>0) return false;
		if (skills.getPointsLeft()>0) return false;
		if (talentsAndResources.getPointsLeft()>0) return false;
		if (model.getName()==null) return false;
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.CharacterController#getSkillController()
	 */
	@Override
	public SkillController getSkillController() {
		return skills;
	}

	@Override
	public TalentController getTalentController() {
		return talentsAndResources;
	}

	@Override
	public ResourceController getResourceController() {
		return talentsAndResources;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.CharacterController#getItemController()
	 */
	@Override
	public ItemController getItemController() {
		return items;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.CharacterController#getFlawController()
	 */
	@Override
	public FlawController getFlawController() {
		return flaws;
	}
	
	//-------------------------------------------------------------------
	/**
	 * Finalizes the generation process, grants start exp
	 * and switches to levelling mode
	 */
	public void generate() {
		logger.info("\n\n\nGenerate ...\n\n\n");
		try {
			if (model.getRewards().size()>0) 
				throw new IllegalStateException("Reward already granted");
			
		} catch (Exception e) {
			logger.error(e.toString(),e);
			return;
		}
		int startExp = template.getStartExp();
		model.setExperienceInvested(0);
//		model.setExperienceFree(startExp);
		model.addReward(new RewardImpl(startExp, UbiquityCore.getI18nResources().getString("label.startexp")+" "+template.getName()));
		logger.debug(model.dump());
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new int[]{template.getStartExp(),0}));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.CharacterController#isRequirementMet(org.prelle.ubiquity.Talent)
	 */
	@Override
	public boolean isRequirementMet(Talent item) {
		for (Requirement req : item.getRequirements()) {
			if (!model.meetsRequirement(req))
				return false;
		}
		return true;
	}

}
