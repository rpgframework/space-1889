/**
 * @author Stefan Prelle
 *
 */
module rpgframework.products.space1889 {
	exports de.rpgframework.products.space1889;
	opens de.rpgframework.products.space1889;

	provides de.rpgframework.products.ProductDataPlugin with de.rpgframework.products.space1889.ProductDataSpace1889;

	requires de.rpgframework.core;
	requires de.rpgframework.products;
	requires org.apache.logging.log4j;
	
}