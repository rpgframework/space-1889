/**
 *
 */
package org.prelle.rpgframework.space1889.data;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.space1889.Space1889Character;
import org.prelle.space1889.Space1889Core;
import org.prelle.ubiquity.BasePluginData;
import org.prelle.ubiquity.UbiquityCore;
import org.prelle.ubiquity.UbiquityRuleset;

import de.rpgframework.ConfigContainer;
import de.rpgframework.ConfigOption;
import de.rpgframework.character.RulePlugin;
import de.rpgframework.character.RulePluginFeatures;
import de.rpgframework.core.CommandResult;
import de.rpgframework.core.CommandType;
import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class Space1889DataPlugin implements RulePlugin<Space1889Character> {

	private final static Logger logger = LogManager.getLogger("space1889.data");

	private static List<RulePluginFeatures> FEATURES = new ArrayList<RulePluginFeatures>();

	private static boolean alreadyInitialized = false;

	//-------------------------------------------------------------------
	static {
		FEATURES.add(RulePluginFeatures.DATA);
	}

	//-------------------------------------------------------------------
	/**
	 */
	public Space1889DataPlugin() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getID()
	 */
	@Override
	public String getID() {
		return "DATA";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#getReadableName()
	 */
	@Override
	public String getReadableName() {
		if (this.getClass().getPackage().getImplementationTitle()!=null)
			return this.getClass().getPackage().getImplementationTitle();
		return "Space 1889 - Data";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getRules()
	 */
	@Override
	public RoleplayingSystem getRules() {
		return RoleplayingSystem.SPACE1889;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getRequiredPlugins()
	 */
	@Override
	public Collection<String> getRequiredPlugins() {
		return Arrays.asList("CORE");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getSupportedFeatures()
	 */
	@Override
	public Collection<RulePluginFeatures> getSupportedFeatures() {
		return FEATURES;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#attachConfigurationTree(de.rpgframework.ConfigContainer)
	 */
	@Override
	public void attachConfigurationTree(ConfigContainer addBelow) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getConfiguration()
	 */
	@Override
	public List<ConfigOption<?>> getConfiguration() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#willProcessCommand(java.lang.Object, de.rpgframework.core.CommandType, java.lang.Object[])
	 */
	@Override
	public boolean willProcessCommand(Object src, CommandType type, Object... values) {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#handleCommand(java.lang.Object, de.rpgframework.core.CommandType, java.lang.Object[])
	 */
	@Override
	public CommandResult handleCommand(Object src, CommandType type, Object... values) {
		return new CommandResult(type, false);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RulePlugin#init(de.rpgframework.character.RulePlugin.RulePluginProgessListener)
	 */
	@Override
	public void init(RulePluginProgessListener callback) {
		if (alreadyInitialized)
			return;
		
		UbiquityRuleset ruleset = UbiquityCore.createRuleset(RoleplayingSystem.SPACE1889);
		double totalPlugins = 5.0;
		double count = 0;

		logger.info("START -------------------------------Core-----------------------------------------------");
		PluginSkeleton CORE = new PluginSkeleton("CORE", "Space 1889 Core Rules");
		Class<Space1889DataPlugin> clazz = Space1889DataPlugin.class;
		Space1889Core.loadTemplates  (ruleset, CORE, clazz.getResourceAsStream("core/data/templates.xml"), CORE.getResources(), CORE.getHelpResources());
		Space1889Core.loadMotivations(ruleset, CORE, clazz.getResourceAsStream("core/data/motivations.xml"), CORE.getResources(), CORE.getHelpResources());
		Space1889Core.loadArchetypes (ruleset, CORE, clazz.getResourceAsStream("core/data/archetypes.xml"), CORE.getResources(), CORE.getHelpResources());
		Space1889Core.loadSkills     (ruleset, CORE, clazz.getResourceAsStream("core/data/skills.xml"), CORE.getResources(), CORE.getHelpResources());
		Space1889Core.loadTalents    (ruleset, CORE, clazz.getResourceAsStream("core/data/talents.xml"), CORE.getResources(), CORE.getHelpResources());
		Space1889Core.loadResources  (ruleset, CORE, clazz.getResourceAsStream("core/data/resources.xml"), CORE.getResources(), CORE.getHelpResources());
		Space1889Core.loadFlaws      (ruleset, CORE, clazz.getResourceAsStream("core/data/flaws.xml"), CORE.getResources(), CORE.getHelpResources());

		Space1889Core.loadItems(ruleset, CORE, clazz.getResourceAsStream("core/data/archery.xml"), CORE.getResources(), CORE.getHelpResources());
		Space1889Core.loadItems(ruleset, CORE, clazz.getResourceAsStream("core/data/armor.xml"), CORE.getResources(), CORE.getHelpResources());
		Space1889Core.loadItems(ruleset, CORE, clazz.getResourceAsStream("core/data/camera.xml"), CORE.getResources(), CORE.getHelpResources());
		Space1889Core.loadItems(ruleset, CORE, clazz.getResourceAsStream("core/data/clothing.xml"), CORE.getResources(), CORE.getHelpResources());
		Space1889Core.loadItems(ruleset, CORE, clazz.getResourceAsStream("core/data/demolitions.xml"), CORE.getResources(), CORE.getHelpResources());
		Space1889Core.loadItems(ruleset, CORE, clazz.getResourceAsStream("core/data/firearms.xml"), CORE.getResources(), CORE.getHelpResources());
		Space1889Core.loadItems(ruleset, CORE, clazz.getResourceAsStream("core/data/gunnery.xml"), CORE.getResources(), CORE.getHelpResources());
		Space1889Core.loadItems(ruleset, CORE, clazz.getResourceAsStream("core/data/melee.xml"), CORE.getResources(), CORE.getHelpResources());
		Space1889Core.loadItems(ruleset, CORE, clazz.getResourceAsStream("core/data/munition.xml"), CORE.getResources(), CORE.getHelpResources());
		Space1889Core.loadItems(ruleset, CORE, clazz.getResourceAsStream("core/data/other.xml"), CORE.getResources(), CORE.getHelpResources());
		Space1889Core.loadItems(ruleset, CORE, clazz.getResourceAsStream("core/data/profession.xml"), CORE.getResources(), CORE.getHelpResources());
		Space1889Core.loadItems(ruleset, CORE, clazz.getResourceAsStream("core/data/survival.xml"), CORE.getResources(), CORE.getHelpResources());
		Space1889Core.loadItems(ruleset, CORE, clazz.getResourceAsStream("core/data/tools.xml"), CORE.getResources(), CORE.getHelpResources());
		ruleset.getTemplate("promising").setDefaultChoice(true);
		count++; callback.progressChanged( (count/totalPlugins) );

		logger.info("START -------------------------------Venus----------------------------------------------");
		PluginSkeleton VENUS = new PluginSkeleton("VENUS", "Space 1889 - Venus");
		UbiquityCore.loadMotivations(ruleset, this, clazz.getResourceAsStream("venus/data/motivations-venus.xml"), VENUS.getResources(), VENUS.getHelpResources());
		UbiquityCore.loadArchetypes (ruleset, this, clazz.getResourceAsStream("venus/data/archetypes-venus.xml"), VENUS.getResources(), VENUS.getHelpResources());
		UbiquityCore.loadTalents    (ruleset, this, clazz.getResourceAsStream("venus/data/talents-venus.xml"), VENUS.getResources(), VENUS.getHelpResources());
		UbiquityCore.loadFlaws      (ruleset, this, clazz.getResourceAsStream("venus/data/flaws-venus.xml"), VENUS.getResources(), VENUS.getHelpResources());

		UbiquityCore.loadItems(ruleset, this, clazz.getResourceAsStream("venus/data/archery-venus.xml"), VENUS.getResources(), VENUS.getHelpResources());
		UbiquityCore.loadItems(ruleset, this, clazz.getResourceAsStream("venus/data/armor-venus.xml"), VENUS.getResources(), VENUS.getHelpResources());
		UbiquityCore.loadItems(ruleset, this, clazz.getResourceAsStream("venus/data/melee-venus.xml"), VENUS.getResources(), VENUS.getHelpResources());
		UbiquityCore.loadItems(ruleset, this, clazz.getResourceAsStream("venus/data/demolitions-venus.xml"), VENUS.getResources(), VENUS.getHelpResources());
		UbiquityCore.loadItems(ruleset, this, clazz.getResourceAsStream("venus/data/firearms-venus.xml"), VENUS.getResources(), VENUS.getHelpResources());
		UbiquityCore.loadItems(ruleset, this, clazz.getResourceAsStream("venus/data/munition-venus.xml"), VENUS.getResources(), VENUS.getHelpResources());
		count++; callback.progressChanged( (count/totalPlugins) );

		logger.info("START -------------------------------Luna----------------------------------------------");
		PluginSkeleton LUNA = new PluginSkeleton("LUNA", "Space 1889 - Luna");
		UbiquityCore.loadTalents(ruleset, this, clazz.getResourceAsStream("luna/data/talents-luna.xml"), LUNA.getResources(), LUNA.getHelpResources());
		UbiquityCore.loadItems  (ruleset, this, clazz.getResourceAsStream("luna/data/equipment-luna.xml"), LUNA.getResources(), LUNA.getHelpResources());
		count++; callback.progressChanged( (count/totalPlugins) );

		logger.info("START -------------------------------Aetherversum 2-------------------------------------");
		PluginSkeleton AETH = new PluginSkeleton("AETHER2", "Space 1889 - Das neue Ätherversum");
		UbiquityCore.loadMotivations(ruleset, this, clazz.getResourceAsStream("aether2/data/motivations-aetherversum2.xml"), AETH.getResources(), AETH.getHelpResources());
		UbiquityCore.loadTalents(ruleset, this, clazz.getResourceAsStream("aether2/data/talents-aetherversum2.xml"), AETH.getResources(), AETH.getHelpResources());
		count++; callback.progressChanged( (count/totalPlugins) );

		BasePluginData.flushMissingKeys();
		logger.debug("STOP : -----Init "+getID()+"---------------------------");
		alreadyInitialized = true;
		callback.progressChanged( 1.0f );
//		logger.fatal("Stop here");
//		System.exit(0);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getAboutHTML()
	 */
	@Override
	public InputStream getAboutHTML() {
		return Space1889DataPlugin.class.getResourceAsStream("org/prelle/space1889/data/data.html");
	}

	//-------------------------------------------------------------------
	@Override
	public List<String> getLanguages() {
		return Arrays.asList(Locale.GERMAN.getLanguage(), Locale.ENGLISH.getLanguage());
	}

}
