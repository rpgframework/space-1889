/**
 * @author Stefan Prelle
 *
 */
module space1889.data {
	exports org.prelle.rpgframework.space1889.data;
	opens org.prelle.rpgframework.space1889.data;

	provides de.rpgframework.character.RulePlugin with org.prelle.rpgframework.space1889.data.Space1889DataPlugin;

	requires org.apache.logging.log4j;
	requires de.rpgframework.core;
	requires space1889.core;
	requires ubiquity.core;
}