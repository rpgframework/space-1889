/**
 * @author Stefan Prelle
 *
 */
module ubiquity.chargen.jfx {
	exports org.prelle.ubiquity.jfx.notes;
	exports org.prelle.ubiquity.jfx.skills;
	exports org.prelle.ubiquity.jfx.wizard;
	exports org.prelle.ubiquity.jfx.attributes;
	exports org.prelle.ubiquity.jfx.pages;
	exports org.prelle.ubiquity.jfx;
	exports org.prelle.ubiquity.jfx.develop;

	requires javafx.base;
	requires javafx.controls;
	requires transitive javafx.extensions;
	requires javafx.graphics;
	requires org.apache.logging.log4j;
	requires transitive de.rpgframework.core;
	requires transitive ubiquity.chargen;
	requires transitive ubiquity.core;
	requires java.prefs;
	requires de.rpgframework.javafx;
	requires de.rpgframework.products;
}