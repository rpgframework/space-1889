package org.prelle.ubiquity.jfx.cells;

import java.util.Iterator;

import org.prelle.ubiquity.Talent;
import org.prelle.ubiquity.charctrl.CharacterController;
import org.prelle.ubiquity.requirements.AttributeRequirement;
import org.prelle.ubiquity.requirements.Requirement;
import org.prelle.ubiquity.requirements.SkillRequirement;

import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.VBox;

public class TalentListCell extends ListCell<Talent> {
	
	private CharacterController control;
	private Label lblName;
	private Label lblRequire;
	private VBox  layout;
	
	private Talent current;
	
	//--------------------------------------------------------------------
	public TalentListCell(CharacterController ctrl) {
		this.control = ctrl;
		lblName = new Label();
		lblName.getStyleClass().add("text-body");
		lblRequire = new Label();
		lblRequire.getStyleClass().add("text-tertiary-info");
		layout = new VBox();
		layout.getChildren().addAll(lblName, lblRequire);
//		layout.setStyle("-fx-padding: 2px; -fx-border-color: black; -fx-border-width: 2px;");
		
//		setOnMouseClicked(this);
	}
	
	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(Talent item, boolean empty) {
		super.updateItem(item, empty);
		current = item;
		
		if (empty) {
			setGraphic(null);
			current = null;
			return;
		}
		
		// Build requirement string
		StringBuffer buf = new StringBuffer();
		for (Iterator<Requirement> it = item.getRequirements().iterator(); it.hasNext(); ) {
			Requirement r = it.next();
			if (r instanceof AttributeRequirement) {
				AttributeRequirement req = (AttributeRequirement)r;
				buf.append( req.getAttr().getName()+" "+req.getVal());
			} else if (r instanceof SkillRequirement) {
				SkillRequirement req = (SkillRequirement)r;
				buf.append( req.getSkill().getName()+" "+req.getValue());
			} else
				buf.append(String.valueOf(r));
			// Needs comma?
			if (it.hasNext())
				buf.append(", ");
		}
		
		lblName.setText(item.getName());
		lblRequire.setText(buf.toString());
		
		if (control.isRequirementMet(item))
			lblRequire.setStyle("-fx-text-fill: green");
		else
			lblRequire.setStyle("-fx-text-fill: red");
		setGraphic(layout);
	}

}