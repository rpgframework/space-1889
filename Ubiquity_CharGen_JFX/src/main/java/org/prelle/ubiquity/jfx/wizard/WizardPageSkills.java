/**
 *
 */
package org.prelle.ubiquity.jfx.wizard;

import java.io.InputStream;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.ubiquity.Skill;
import org.prelle.ubiquity.SkillSpecialization;
import org.prelle.ubiquity.SkillSpecializationValue;
import org.prelle.ubiquity.SkillValue;
import org.prelle.ubiquity.chargen.CharacterGenerator;
import org.prelle.ubiquity.chargen.event.GenerationEvent;
import org.prelle.ubiquity.chargen.event.GenerationEventDispatcher;
import org.prelle.ubiquity.chargen.event.GenerationEventListener;
import org.prelle.ubiquity.jfx.UbiquityJFXConstants;
import org.prelle.ubiquity.jfx.skills.SkillTreePane;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;

/**
 * @author Stefan
 *
 */
public class WizardPageSkills extends WizardPage implements GenerationEventListener {
	
	protected static Logger logger = LogManager.getLogger("ubiquity.jfx");
	
	private static PropertyResourceBundle UI = UbiquityJFXConstants.UI;

	private CharacterGenerator charGen;

	// Content
	private Label    explain;
	private SkillTreePane table;

	private Label focusSkillName;
	private VBox boxSpecial;
	private Label focusSkillDescription;

	private VBox content;
	private Label pointsLeft;

	//--------------------------------------------------------------------
	public WizardPageSkills(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
		GenerationEventDispatcher.addListener(this);

		// Select standard
		refresh();
	}

	//-------------------------------------------------------------------
	private void initTable() {
		table = new SkillTreePane(charGen.getSkillController(), charGen.getTalentController(), charGen.getModel());
		table.setData(charGen.getModel());
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("wizard.selectSkill.title"));

		pointsLeft = new Label();
		/*
		 * Content
		 */
		explain = new Label(UI.getString("wizard.selectSkill.descr"));
		explain.setWrapText(true);

		initTable();

		/*
		 * Column showing per skill information
		 */
		boxSpecial = new VBox(5);
		focusSkillName = new Label();

		focusSkillDescription = new Label();
		focusSkillDescription.setWrapText(true);
		focusSkillDescription.setMaxHeight(Double.MAX_VALUE);
		focusSkillDescription.setTextAlignment(TextAlignment.JUSTIFY);
		focusSkillDescription.setAlignment(Pos.TOP_LEFT);
//		focusSkillDescription.setPrefHeight(90);

		content = new VBox();
		content.setSpacing(5);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		// Content node
		explain.setMaxWidth(720);
//		focusSkillDescription.setPrefWidth(650);

		table.setStyle("-fx-min-width: 25em");
		table.setStyle("-fx-max-width: 40em");

		/*
		 * Info column
		 */
		VBox boxInfo = new VBox(5);
		boxInfo.getChildren().addAll(focusSkillName, boxSpecial, focusSkillDescription);
		boxInfo.setStyle("-fx-min-width: 15em");
		boxInfo.setStyle("-fx-pref-width: 20em");
		boxInfo.setStyle("-fx-max-width: 25em");

		HBox sideByside = new HBox(20);
		sideByside.getChildren().addAll(table, boxInfo);
		HBox.setHgrow(table, Priority.ALWAYS);
		HBox.setHgrow(boxInfo, Priority.SOMETIMES);

		content.getChildren().addAll(explain,  sideByside);
		VBox.setVgrow(sideByside, Priority.ALWAYS);
		sideByside.setMaxHeight(Double.MAX_VALUE);
		super.setContent(content);

		// Side node
		VBox side = new VBox();
		Label lblPointsLeft = new Label(UI.getString("wizard.selectAttributes.pointsLeft"));
		lblPointsLeft.getStyleClass().add("text-body");
		side.getChildren().addAll(lblPointsLeft, pointsLeft);
		side.setAlignment(Pos.TOP_CENTER);
		setSide(side);

//		String fname = "data/space1889/archetype_soldier.png";
//		InputStream in = getClass().getClassLoader().getResourceAsStream(fname);
//		if (in!=null) {
//			setImage(new Image(in));
//		} else
//			logger.warn("Missing image at "+fname);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		explain.getStyleClass().add("text-body");
		focusSkillName.getStyleClass().add("section-head");
		focusSkillName.setWrapText(true);
		focusSkillDescription.getStyleClass().add("text-body");
		pointsLeft.getStyleClass().add("text-subheader");
		table.getStyleClass().add("tree-table-view-wizard");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		table.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n==null) {
				selectionChanged(null);
			} else
				selectionChanged(n.getValue());
		});
	}

	//-------------------------------------------------------------------
	private void selectionChanged(Skill selected) {
		logger.debug("Selected "+selected);
		// Show specializations
		boxSpecial.getChildren().clear();

		if (selected==null) {
			focusSkillName.setText(null);
			focusSkillDescription.setText(null);
		} else if (!selected.getSpecializations().isEmpty()) {
			addSpecializations(selected);
			focusSkillName.setText(selected.getName());
			focusSkillDescription.setText(selected.getHelpText());
		}
	}

	//-------------------------------------------------------------------
	private void addSpecializations(Skill selected) {
		SkillValue sVal = charGen.getModel().getSkill(selected);
		ToggleGroup radioGrp = new ToggleGroup();

		radioGrp.selectedToggleProperty().addListener( (ov,o,n) -> {
			logger.debug("Toggle changed from "+o+" to "+n);
			RadioButton oldBtn = (RadioButton)o;
			RadioButton newBtn = (RadioButton)n;
			if (oldBtn!=null && oldBtn.getUserData()!=null) {
				// Deselect
				SkillSpecialization spec = (SkillSpecialization) oldBtn.getUserData();
				SkillSpecializationValue specVal = charGen.getModel().getSkill(spec.getSkill()).getSpecialization(spec);
				charGen.getSkillController().deselectSpecialization(specVal);
			}
			if (newBtn!=null && newBtn.getUserData()!=null) {
				// Select
				charGen.getSkillController().selectSpecialization((SkillSpecialization) newBtn.getUserData());
			}
		});

		// Add a button to deselect specializations
		RadioButton btnNone = new RadioButton(UI.getString("label.noskillspec"));
		btnNone.setToggleGroup(radioGrp);
		btnNone.setSelected(true);
		boxSpecial.getChildren().add(btnNone);

		for (SkillSpecialization spec : selected.getSpecializations()) {
			RadioButton radio = new RadioButton(spec.getName());
			radio.setUserData(spec);
			radio.setToggleGroup(radioGrp);
			boolean isSelected = false;
			SkillSpecializationValue specVal = charGen.getModel().getSkill(spec.getSkill()).getSpecialization(spec);
			if (sVal!=null)
				isSelected = sVal.getSpecializations().contains(specVal);
			radio.setSelected(isSelected);
			if (isSelected) {
				// Is selected
				radio.setDisable(!charGen.getSkillController().canDeselectSpecialization(specVal));
			} else {
				// Not selected
				radio.setDisable(!charGen.getSkillController().canSelectSpecialization(spec));
			}
			boxSpecial.getChildren().add(radio);
		}

	}

	//-------------------------------------------------------------------
	private void refresh() {
		logger.debug("refresh");
//		((MyTreeTableViewSkin<SkillValue>)table.getSkin()).refresh();
//		table.requestLayout();
//		table.getItems().clear();
//		table.getItems().addAll(charGen.getSkillController().getSkills());
		pointsLeft.setText(String.valueOf(charGen.getSkillController().getPointsLeft()));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.ubiquity.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case POINTS_LEFT_SKILLS:
			logger.debug("RCV "+event);
			pointsLeft.setText(String.valueOf(charGen.getSkillController().getPointsLeft()));
			break;
		case ATTRIBUTE_CHANGED:
			logger.debug("RCV "+event);
			refresh();
			break;
		case SKILL_ADDED:
		case SKILL_REMOVED:
			logger.debug("RCV "+event);
			// If added/removed skill was selected, update selection
			TreeItem<Skill> selItem = table.getSelectionModel().getSelectedItem();
			if (selItem!=null) {
				if (selItem.getValue()==(Skill)event.getKey())
					selectionChanged(selItem.getValue());
			}
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Called from Wizard when page is left
	 */
	public void pageLeft(CloseType type) {
		logger.debug("pageLeft("+type+")");
//		charGen.selectMotivation(spinMotivation.getValue());
	}

}
