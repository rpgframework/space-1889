package org.prelle.ubiquity.jfx.cells;

import java.util.Iterator;

import org.prelle.ubiquity.Flaw;
import org.prelle.ubiquity.charctrl.CharacterController;
import org.prelle.ubiquity.requirements.AttributeRequirement;
import org.prelle.ubiquity.requirements.Requirement;
import org.prelle.ubiquity.requirements.SkillRequirement;

import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

public class FlawListCell extends ListCell<Flaw> {
	
	private CharacterController control;
	private Label lblName;
	private Label lblRequire;
	private VBox  layout;
	
	private Flaw current;
	
	//--------------------------------------------------------------------
	public FlawListCell(CharacterController ctrl) {
		this.control = ctrl;
		lblName = new Label();
		lblName.getStyleClass().add("text-body");
		lblRequire = new Label();
		lblRequire.getStyleClass().add("text-tertiary-info");
		layout = new VBox();
		layout.getChildren().addAll(lblName, lblRequire);
//		layout.setStyle("-fx-border-color: black; -fx-border-width: 1px;");
		
//		setOnMouseClicked(this);
	}
	
	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(Flaw item, boolean empty) {
		super.updateItem(item, empty);
		current = item;
		
		if (empty) {
			setGraphic(null);
			current = null;
			layout.setStyle("-fx-border-color: transparent; -fx-border-width: 1px;");
//			this.setStyle("-fx-background-color: transparent; -fx-border-width: 2px; -fx-border-color: transparent;");
			return;
		} else {
			layout.setStyle(null);
		}
		
		lblName.setText(item.getName());
//		lblRequire.setText(buf.toString());
//		
//		if (control.isRequirementMet(item))
//			lblRequire.setStyle("-fx-text-fill: green");
//		else
//			lblRequire.setStyle("-fx-text-fill: red");
		setGraphic(layout);
	}

}