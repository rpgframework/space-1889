/**
 * 
 */
package org.prelle.ubiquity.jfx.notes;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.chargen.event.GenerationEvent;
import org.prelle.ubiquity.chargen.event.GenerationEventDispatcher;
import org.prelle.ubiquity.chargen.event.GenerationEventListener;
import org.prelle.ubiquity.jfx.UbiquityJFXConstants;

import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class NotesCard extends VBox implements GenerationEventListener {
	
	private static Logger logger = LogManager.getLogger("ubiquity.jfx");
	
	private static PropertyResourceBundle UI = UbiquityJFXConstants.UI;

	private UbiquityCharacter     model;

//	private Label heading;

	//-------------------------------------------------------------------
	/**
	 */
	public NotesCard() {
		getStyleClass().addAll("table","content");
		
		initComponents();
		initLayout();
		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
//		heading   = new Label(UI.getString("label.notes"));
//		heading.getStyleClass().add("table-head");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		getStyleClass().add("text-body");
//		getChildren().add(heading);
//		heading.setMaxWidth(Double.MAX_VALUE);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case NOTES_CHANGED:
			logger.debug("NotesCard received "+event);
			updateContent();
			break;
		default:
		}		
	}

	//-------------------------------------------------------------------
	private void updateContent() {
//		getChildren().retainAll(heading);
		getChildren().clear();
		
		Label name = new Label(model.getNotes());
//		name.setWrapText(true);
		name.setMaxWidth(Double.MAX_VALUE);
		getChildren().add(name);
	}

	//-------------------------------------------------------------------
	public void setData(UbiquityCharacter	 model) {
		this.model = model;
		updateContent();
	}

}
