/**
 * 
 */
package org.prelle.ubiquity.jfx.wizard;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.ubiquity.Archetype;
import org.prelle.ubiquity.UbiquityRuleset;
import org.prelle.ubiquity.chargen.CharacterGenerator;
import org.prelle.ubiquity.jfx.UbiquityJFXConstants;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory.ListSpinnerValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author Stefan
 *
 */
public class WizardPageArchetype extends WizardPage {
	
	protected static Logger logger = LogManager.getLogger("ubiquity.jfx");
	
	private static PropertyResourceBundle UI = UbiquityJFXConstants.UI;

	private UbiquityRuleset ruleset;
	private CharacterGenerator charGen;

	private static Map<Archetype,Image> imageByArchetype;
	
	// Content
	private Label    explain;
	private Spinner<Archetype> spinArchetype;
	private Label    reference;
	private Label    description;
	
	private VBox content;

	//--------------------------------------------------------------------
	public WizardPageArchetype(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;
		this.ruleset = charGen.getRuleset();
		imageByArchetype  = new HashMap<Archetype, Image>();
		
		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
		
		// Select standard
		spinArchetype.increment();
		spinArchetype.decrement();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("wizard.selectArchetype.title"));
		
		/*
		 * Content
		 */
		explain = new Label(UI.getString("wizard.selectArchetype.descr"));
		explain.setWrapText(true);

		spinArchetype = new Spinner<>();
		spinArchetype.getStyleClass().add(Spinner.STYLE_CLASS_SPLIT_ARROWS_HORIZONTAL);
		ListSpinnerValueFactory<Archetype> factory = new ListSpinnerValueFactory<Archetype>(FXCollections.observableArrayList(charGen.getRuleset().getArchetypes()));
		factory.setWrapAround(true);
		factory.setConverter(new StringConverter<Archetype>() {
			public String toString(Archetype val) { return val!=null?val.getName():"";}
			public Archetype fromString(String string) {return null;}
		});
		spinArchetype.setValueFactory(factory);
		
		description = new Label();
		description.setWrapText(true);

		reference = new Label();
		
		content = new VBox();
		content.setSpacing(5);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		// Content node
		explain.setPrefWidth(550);
		description.setPrefWidth(550);
		
		content.getChildren().addAll(explain,  spinArchetype);
		content.getChildren().addAll(reference, description);
		super.setContent(content);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		explain.getStyleClass().add("text-body");
		description.getStyleClass().add("text-body");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		spinArchetype.valueProperty().addListener(new ChangeListener<Archetype>() {
			public void changed(ObservableValue<? extends Archetype> observable,
					Archetype oldValue, Archetype newValue) {
				logger.debug("Change to "+newValue);
				charGen.selectArchetype(newValue);
				update(newValue);
			}
		});
		
	}

	//-------------------------------------------------------------------
	protected void update(Archetype value) {
		Image img = imageByArchetype.get(value);
		if (img==null) {
			String fname = "data/space1889/archetype_"+value.getId()+".png";
			logger.trace("Load "+fname);
			InputStream in = getClass().getClassLoader().getResourceAsStream(fname);
			if (in!=null) {
				img = new Image(in);
				imageByArchetype.put(value, img);
			} else
				logger.warn("Missing image at "+fname);
		}
		setImage(img);

		// Update page reference
		int page = value.getPage();
		String product = value.getProductName();
		String format = UI.getString("format.pagereference");
		if (page>0)
			reference.setText(String.format(format, product, page));
		else
			reference.setText(String.format(format, product, "?"));

		description.setText(value.getHelpText());
	}

	//-------------------------------------------------------------------
	/**
	 * Called from Wizard when page is left
	 */
	public void pageLeft(CloseType type) {
		logger.debug("pageLeft("+type+")");
		charGen.selectArchetype(spinArchetype.getValue());
	}

}

