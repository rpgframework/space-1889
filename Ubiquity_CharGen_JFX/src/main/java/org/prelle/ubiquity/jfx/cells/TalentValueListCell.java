package org.prelle.ubiquity.jfx.cells;

import org.prelle.ubiquity.TalentValue;
import org.prelle.ubiquity.charctrl.TalentController;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.TilePane;

//--------------------------------------------------------------------
public class TalentValueListCell extends ListCell<TalentValue> implements EventHandler<MouseEvent> {
	
	private TalentController ctrl;
	private Label lblName;
	private Button btnDec;
	private Button btnInc;
	private HBox  layout;
	
	private TalentValue current;
	
	//--------------------------------------------------------------------
	public TalentValueListCell(TalentController control) {
		this.ctrl = control;
		lblName = new Label();
		lblName.getStyleClass().add("text-body");
		lblName.setAlignment(Pos.CENTER_LEFT);
		lblName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		btnDec = new Button("-");
		btnDec.getStyleClass().add("text-small-heading");
		btnDec.setMaxWidth(Double.MAX_VALUE);
		btnDec.setStyle("-fx-border-color: black; -fx-border-width: 1px; ");
		btnDec.setOnAction(event -> control.decrease(current));
		btnInc = new Button("+");
		btnInc.getStyleClass().add("text-small-heading");
		btnInc.setStyle("-fx-border-color: black; -fx-border-width: 1px;");
		btnInc.setOnAction(event -> control.increase(current));
		TilePane buttons = new TilePane(btnDec, btnInc);
		buttons.setPrefColumns(2);
		buttons.setStyle("-fx-hgap: 5px");
		layout = new HBox(5);
		layout.getChildren().addAll(lblName, buttons);
//		layout.setStyle("-fx-padding: 2px; -fx-border-color: black; -fx-border-width: 1px;");
		HBox.setHgrow(lblName, Priority.ALWAYS);

//		setOnMouseClicked(this);
	}
	
	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(TalentValue item, boolean empty) {
		super.updateItem(item, empty);
		current = item;
		
		if (empty) {
			setGraphic(null);
			current = null;
			return;
		}
		
		setGraphic(layout);
		btnInc.setVisible(item.getModifyable().getLevel()>1);
		btnDec.setVisible(item.getModifyable().getLevel()>1);
		lblName.setText(current.getName());
		btnInc.setDisable(!ctrl.canBeIncreased(current));
		btnDec.setDisable(!ctrl.canBeDecreased(current));
		btnInc.setManaged(item.getModifyable().getLevel()>1);
		btnDec.setManaged(item.getModifyable().getLevel()>1);
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.event.EventHandler#handle(javafx.event.Event)
	 */
	@Override
	public void handle(MouseEvent event) {
		if (current!=null && event.getClickCount()==2)
			ctrl.deselect(current);
	}
	
}