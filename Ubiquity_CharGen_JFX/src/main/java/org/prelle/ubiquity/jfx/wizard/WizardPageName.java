/**
 *
 */
package org.prelle.ubiquity.jfx.wizard;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.ubiquity.UbiquityCharacter.Gender;
import org.prelle.ubiquity.chargen.CharacterGenerator;
import org.prelle.ubiquity.chargen.event.GenerationEvent;
import org.prelle.ubiquity.chargen.event.GenerationEventDispatcher;
import org.prelle.ubiquity.chargen.event.GenerationEventType;
import org.prelle.ubiquity.jfx.UbiquityJFXConstants;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * @author prelle
 *
 */
public class WizardPageName extends WizardPage implements ChangeListener<String> {

	protected static Logger logger = LogManager.getLogger("ubiquity.jfx");

	private static PropertyResourceBundle UI = UbiquityJFXConstants.UI;

	private CharacterGenerator charGen;

	private Label    explain;
	private TextField tfName;
	private ChoiceBox<Gender> cbGender;
	private TextField tfHair;
	private TextField tfEye;
	private TextField tfWeight;
	private TextField tfSize;
	private TextField tfAge;
	private Slider size;
	private ImageView portrait;

	private Button btnRemoveImage;

	private Button btnAddImage;

	//-------------------------------------------------------------------
	public WizardPageName(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();

		// Select standard
		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("wizard.selectName.title"));
		/*
		 * Content
		 */
		explain = new Label(UI.getString("wizard.selectName.descr"));
		explain.setWrapText(true);

		tfName = new TextField();
		tfHair = new TextField();
		tfEye  = new TextField();
		tfWeight    = new TextField();
		tfSize   = new TextField();
		tfAge     = new TextField();
		tfWeight.setPromptText(UI.getString("prompt.weight"));
		tfSize.setPromptText(UI.getString("prompt.size"));
		tfAge.setPromptText(UI.getString("prompt.age"));

		int min = 120;
		int max = 220;
		int avg = (max-min)/2 + min;
		size      = new Slider(min,max,avg);
		size.setShowTickLabels(true);
		size.setShowTickMarks(true);
//		size.setMajorTickUnit(50);
		size.setMinorTickCount(5);
		cbGender    = new ChoiceBox<Gender>(FXCollections.observableArrayList(Gender.MALE, Gender.FEMALE));
		portrait = new ImageView();
		portrait.setFitHeight(200);
		portrait.setFitWidth(200);
		portrait.setPreserveRatio(true);
		portrait.setImage(new Image(UbiquityJFXConstants.class.getResourceAsStream("images/guest-256.png")));
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		btnAddImage = new Button(UI.getString("button.select"));
		btnRemoveImage = new Button(UI.getString("button.remove"));
		VBox portBox = new VBox(10);
		portBox.setAlignment(Pos.TOP_CENTER);
		portBox.getChildren().addAll(portrait, btnAddImage, btnRemoveImage);

		// Content node
		explain.setMaxWidth(720);

		Label lblName   = new Label(UI.getString("label.name"));
		Label lblGender = new Label(UI.getString("label.gender"));
		Label lblSize   = new Label(UI.getString("label.size"));
		Label lblWeight = new Label(UI.getString("label.weight"));
		Label lblHair   = new Label(UI.getString("label.hair"));
		Label lblEyes   = new Label(UI.getString("label.eyes"));
		Label lblAge    = new Label(UI.getString("label.age"));
		lblName.getStyleClass().add("text-small-subheader");
		lblGender.getStyleClass().add("text-small-subheader");
		lblSize.getStyleClass().add("text-small-subheader");
		lblWeight.getStyleClass().add("text-small-subheader");
		lblHair.getStyleClass().add("text-small-subheader");
		lblEyes.getStyleClass().add("text-small-subheader");
		lblAge.getStyleClass().add("text-small-subheader");

		GridPane content = new GridPane();
		content.setVgap(15);
		content.setHgap(5);
		content.add(lblName  , 0, 0);
		content.add(lblGender, 0, 1);
		content.add(lblSize  , 0, 2);
		content.add(lblWeight, 0, 3);
		content.add(lblHair  , 0, 4);
		content.add(lblEyes  , 0, 5);
		content.add(lblAge   , 0, 6);
		content.add(tfName   , 1, 0);
		content.add(cbGender   , 1, 1);
		content.add(tfSize  , 1, 2);
		content.add(tfWeight   , 1, 3);
		content.add(tfHair, 1, 4);
		content.add(tfEye , 1, 5);
		content.add(tfAge    , 1, 6);

		content.add(portBox  , 3, 0, 1,7);
		GridPane.setValignment(portBox, VPos.TOP);

		VBox realContent = new VBox();
		realContent.getStyleClass().add("text-body");
		realContent.getChildren().addAll(explain,  content);
		super.setContent(realContent);
	}



	//-------------------------------------------------------------------
	private void initStyle() {
		explain.getStyleClass().add("text-body");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		tfName.textProperty().addListener(this);
		tfHair.textProperty().addListener(this);
		tfEye.textProperty().addListener(this);
		tfAge.textProperty().addListener(this);
		tfWeight.textProperty().addListener(this);
		tfSize.textProperty().addListener(this);
		cbGender.getSelectionModel().selectedItemProperty().addListener((ov,o,n) -> {
			charGen.getModel().setGender(n);
		});

		portrait.setOnMouseClicked(event -> openFileChooser());
		btnAddImage.setOnAction(event -> openFileChooser());
		btnRemoveImage.setOnAction(event -> {
			charGen.getModel().setImage(null);
			portrait.setImage(new Image(ClassLoader.getSystemResourceAsStream(UbiquityJFXConstants.PREFIX+"/images/guest-256.png")));
		});
	}

	//-------------------------------------------------------------------
	private void refresh() {
		tfName.setText(charGen.getModel().getName());
	}

	//--------------------------------------------------------------------
	private void openFileChooser() {
		FileChooser chooser = new FileChooser();
		chooser.setInitialDirectory(new File(System.getProperty("user.home")));
		chooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
            );
		File selection = chooser.showOpenDialog(new Stage());
		if (selection!=null) {
			try {
				byte[] imgBytes = Files.readAllBytes(selection.toPath());
				portrait.setImage(new Image(new ByteArrayInputStream(imgBytes)));
				charGen.getModel().setImage(imgBytes);
			} catch (IOException e) {
				logger.warn("Failed loading image from "+selection+": "+e);
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.beans.value.ChangeListener#changed(javafx.beans.value.ObservableValue, java.lang.Object, java.lang.Object)
	 */
	@Override
	public void changed(ObservableValue<? extends String> textfield, String oldVal,	String newVal) {
		if (textfield==tfName.textProperty()) {
			charGen.getModel().setName(newVal);
			GenerationEvent event = new GenerationEvent(GenerationEventType.BASE_DATA_CHANGED, charGen.getModel());
			GenerationEventDispatcher.fireEvent(event);
		} else if (textfield==tfAge.textProperty()) {
			try {
				charGen.getModel().setAge(Integer.parseInt(newVal));
			} catch (NumberFormatException e) {
				tfAge.setText(oldVal);
			}
		} else if (textfield==tfEye.textProperty()) {
			charGen.getModel().setEyeColor(newVal);
		} else if (textfield==tfHair.textProperty()) {
			charGen.getModel().setHairColor(newVal);
		} else if (textfield==tfWeight.textProperty()) {
			try {
				charGen.getModel().setWeight(Integer.parseInt(newVal));
			} catch (NumberFormatException e) {
				tfWeight.setText(oldVal);
			}
		} else if (textfield==tfSize.textProperty()) {
			try {
				charGen.getModel().setSize(Integer.parseInt(newVal));
			} catch (NumberFormatException e) {
				tfSize.textProperty().set(oldVal);
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Called from Wizard when page is left
	 */
	public void pageLeft(CloseType type) {
		logger.debug("pageLeft("+type+")");
		if (type==CloseType.FINISH)
			charGen.generate();
	}

}
