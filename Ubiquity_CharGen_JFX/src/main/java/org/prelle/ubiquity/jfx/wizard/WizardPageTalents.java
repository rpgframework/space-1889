/**
 * 
 */
package org.prelle.ubiquity.jfx.wizard;

import java.io.InputStream;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.rpgframework.jfx.SelectionControllerNode;
import org.prelle.ubiquity.Skill;
import org.prelle.ubiquity.Talent;
import org.prelle.ubiquity.TalentValue;
import org.prelle.ubiquity.chargen.CharacterGenerator;
import org.prelle.ubiquity.chargen.event.GenerationEvent;
import org.prelle.ubiquity.chargen.event.GenerationEventDispatcher;
import org.prelle.ubiquity.chargen.event.GenerationEventListener;
import org.prelle.ubiquity.jfx.UbiquityJFXConstants;
import org.prelle.ubiquity.jfx.cells.TalentListCell;
import org.prelle.ubiquity.jfx.cells.TalentValueListCell;

import javafx.geometry.Pos;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author Stefan
 *
 */
public class WizardPageTalents extends WizardPage implements GenerationEventListener {
	
	protected static Logger logger = LogManager.getLogger("ubiquity.jfx");
	
	private static PropertyResourceBundle UI = UbiquityJFXConstants.UI;

	private CharacterGenerator charGen;
	
	// Content
	private Label    explain;
	private SelectionControllerNode<Talent, TalentValue> talents;
	
	private VBox content;
	private Label pointsLeft;

	//--------------------------------------------------------------------
	public WizardPageTalents(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;
		
		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
		GenerationEventDispatcher.addListener(this);
		
		// Select standard
		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("wizard.selectTalent.title"));

		pointsLeft = new Label();
		/*
		 * Content
		 */
		explain = new Label(UI.getString("wizard.selectTalent.descr"));
		explain.setWrapText(true);

		talents = new SelectionControllerNode<>(charGen.getTalentController());
		talents.setAvailableCellFactory(lv -> new TalentListCell(charGen));
		talents.setSelectedCellFactory(lv -> new TalentValueListCell(charGen.getTalentController()));
		
		content = new VBox();
		content.setSpacing(5);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		// Content node
		explain.setMaxWidth(720);
		
		content.getChildren().addAll(explain,  talents);
		VBox.setVgrow(talents, Priority.ALWAYS);
		talents.setMaxHeight(Double.MAX_VALUE);
		super.setContent(content);
		
		// Side node
		VBox side = new VBox();
		Label lblPointsLeft = new Label(UI.getString("wizard.selectAttributes.pointsLeft"));
		lblPointsLeft.getStyleClass().add("text-body");
		side.getChildren().addAll(lblPointsLeft, pointsLeft);
		side.setAlignment(Pos.TOP_CENTER);
		setSide(side);

//		String fname = "data/space1889/archetype_soldier.png";
//		InputStream in = getClass().getClassLoader().getResourceAsStream(fname);
//		if (in!=null) {
//			setImage(new Image(in));
//		} else
//			logger.warn("Missing image at "+fname);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		explain.getStyleClass().add("text-body");
		pointsLeft.getStyleClass().add("text-subheader");
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	private void initInteractivity() {
		talents.setOptionCallback( (t,l) -> presentChoice(t,(List<Skill>) l));
	}

	//--------------------------------------------------------------------
	private Skill presentChoice(Talent current, List<Skill> options) {
		logger.debug("presentChoice on "+current);
		if (!options.isEmpty()) {
			ChoiceBox<Skill> cbOptions = new ChoiceBox<>();
			cbOptions.setStyle("-fx-max-width: 20em");
			cbOptions.setConverter(new StringConverter<Skill>() {
				public String toString(Skill val) {return val!=null?val.getName():"";}
				public Skill fromString(String sVal) {return null;}
			});
			cbOptions.getItems().addAll(options);
			cbOptions.getSelectionModel().select(0);
			
			String mess = String.format(
					UI.getString("wizard.selectTalent.selectOption"),
					current.getName()
					);
			CloseType close = wizard.getScreenManager().showAlertAndCall(
					AlertType.QUESTION, mess, cbOptions);
			if (close==CloseType.OK) {
				logger.debug("  user selected "+cbOptions.getSelectionModel().getSelectedItem());
				return cbOptions.getSelectionModel().getSelectedItem();
			}
			
			// No option selected
			logger.debug("  No option selected by user");
		}
		return null;
	}

	//-------------------------------------------------------------------
	private void refresh() {
		pointsLeft.setText(String.valueOf(charGen.getTalentController().getPointsLeft()));
		talents.refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.ubiquity.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case POINTS_LEFT_TALENTS_RESOURCES:
			logger.debug("RCV "+event);
			pointsLeft.setText(String.valueOf(charGen.getTalentController().getPointsLeft()));
			break;
		case TALENT_ADDED:
		case TALENT_REMOVED:
		case TALENT_CHANGED:
		case TALENT_AVAILABLE_CHANGED:
			refresh();
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Called from Wizard when page is left
	 */
	public void pageLeft(CloseType type) {
		logger.debug("pageLeft("+type+")");
//		charGen.selectMotivation(spinMotivation.getValue());
	}

	//--------------------------------------------------------------------
	void onTalentDoubleClicked(Talent current) {
		// TODO Auto-generated method stub
		logger.debug("double click on "+current);
		List<Skill> options = charGen.getTalentController().getOptions(current);
		if (!options.isEmpty()) {
			ChoiceBox<Skill> cbOptions = new ChoiceBox<>();
			cbOptions.setStyle("-fx-max-width: 20em");
			cbOptions.setConverter(new StringConverter<Skill>() {
				public String toString(Skill val) {return val!=null?val.getName():"";}
				public Skill fromString(String sVal) {return null;}
			});
			cbOptions.getItems().addAll(options);
			cbOptions.getSelectionModel().select(0);
			
			String mess = String.format(
					UI.getString("wizard.selectTalent.selectOption"),
					current.getName()
					);
			CloseType close = wizard.getScreenManager().showAlertAndCall(
					AlertType.QUESTION, mess, cbOptions);
			logger.debug("CLose "+close);
			if (close==CloseType.OK) {
				Skill option = cbOptions.getSelectionModel().getSelectedItem();
				logger.debug("Option chosen = "+option);
				charGen.getTalentController().select(current, option);
				return;
			}
			
			// No option selected
			logger.debug("No option selected by user");
			return;
		}
		charGen.getTalentController().select(current);
	}

}
