package org.prelle.ubiquity.jfx.sections;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.DescriptionPane;
import org.prelle.rpgframework.jfx.OptionalDescriptionPane;
import org.prelle.ubiquity.FlawValue;
import org.prelle.ubiquity.Flaw;
import org.prelle.ubiquity.charctrl.CharacterController;
import org.prelle.ubiquity.jfx.cells.FlawListCell;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Stefan Prelle
 *
 */
public class FlawSection extends GenericListSection<FlawValue> {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(FlawSection.class.getName());

	//-------------------------------------------------------------------
	public FlawSection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(title, ctrl, provider);
//		list.setCellFactory( lv -> new FlawValueListCell(ctrl.getFlawController()));

		setData(ctrl.getModel().getFlaws());
		list.setStyle("-fx-pref-height: 50em; -fx-pref-width: 32em");
		GridPane.setVgrow(list, Priority.ALWAYS);
		list.setMaxHeight(Double.MAX_VALUE);

		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> getDeleteButton().setDisable(n==null));
		initInteractivity();
	}

	//-------------------------------------------------------------------
	protected void initInteractivity() {
		super.initInteractivity();
		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			Platform.runLater( () -> {
				getDeleteButton().setDisable( !control.getFlawController().canBeDeselected(n));
			});
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.jfx.sections.GenericListSection#onAdd()
	 */
	@Override
	protected void onAdd() {
		logger.trace("onAdd");
		Label question = new Label(RES.getString("section.resource.dialog.add.question"));
		ListView<Flaw> myList = new ListView<>();
		myList.setCellFactory(lv -> new FlawListCell(control));
		myList.getItems().addAll(control.getFlawController().getAvailable());
		myList.setPlaceholder(new Label(RES.getString("section.talent.dialog.add.placeholder")));
		VBox innerLayout = new VBox(10, question, myList);

		final DescriptionPane descr = new DescriptionPane();
		OptionalDescriptionPane layout = new OptionalDescriptionPane(innerLayout, descr);

		myList.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			descr.setText(n.getName(), n.getProductName()+" "+n.getPage(), n.getHelpText());
		});


		CloseType result = provider.getScreenManager().showAlertAndCall(AlertType.QUESTION, RES.getString("section.resource.dialog.add.title"), layout);
		if (result==CloseType.OK) {
			Flaw value = myList.getSelectionModel().getSelectedItem();
			if (value!=null) {
				logger.debug("Try add resource: "+value);
				myList.getItems().add(value);
				control.getFlawController().select(value);
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.jfx.sections.GenericListSection#onDelete()
	 */
	@Override
	protected void onDelete() {
		logger.trace("onDelete");
		FlawValue toDelete = list.getSelectionModel().getSelectedItem();
		if (toDelete!=null) {
			logger.info("Try remove talent: "+toDelete);
			if (control.getFlawController().deselect(toDelete)) {
				list.getItems().remove(toDelete);
				list.getSelectionModel().clearSelection();
			} else 
				logger.warn("GUI allowed removing a talent which cannot be deselected: "+toDelete);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.Section#refresh()
	 */
	@Override
	public void refresh() {
		setData(control.getModel().getFlaws());
	}

}
