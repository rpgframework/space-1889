/**
 *
 */
package org.prelle.ubiquity.jfx.attributes;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import org.prelle.javafx.skin.GridPaneTableViewSkin;
import org.prelle.ubiquity.Attribute;
import org.prelle.ubiquity.AttributeValue;
import org.prelle.ubiquity.charctrl.CharacterController;

/**
 * @author Stefan
 *
 */
public class AttributeTableReadOnly extends AttributeTable {

	private TableColumn<AttributeValue, Number> valCol;
	private TableColumn<AttributeValue, Number> modCol;
	private TableColumn<AttributeValue, Number> finCol;

	//--------------------------------------------------------------------
	public AttributeTableReadOnly(CharacterController ctrl, Attribute[] attributes) {
		super(ctrl, attributes);
//		this.ctrl = ctrl;
//		setSkin(new GridPaneTableViewSkin<>(this));
//		initColumns();
//		initValueFactories();
//		initCellFactories();
//		initLayout();
//
//		for (Attribute key : attributes) {
//			getItems().add(ctrl.getModel().getAttribute(key));
//		}

	}

	//--------------------------------------------------------------------
	protected void initColumns() {
		nameCol = new TableColumn<>(RES.getString("attribtable.col.name"));
		valCol  = new TableColumn<>(RES.getString("attribtable.col.value"));
		modCol  = new TableColumn<>(RES.getString("attribtable.col.modifier"));
		finCol  = new TableColumn<>(RES.getString("attribtable.col.fin"));

		modCol.setId("attribtable-mod");
		nameCol.setId("attribtable-name");
		finCol.setId("attribtable-final");
		valCol.setId("attribtable-val");

		nameCol.setMinWidth(120);
		nameCol.setPrefWidth(130);
		valCol.setPrefWidth(60);
		modCol.setPrefWidth(50);
		finCol.setPrefWidth(60);
	}

	//--------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	protected void initLayout() {
		getColumns().addAll(nameCol, valCol, modCol, finCol);
		setStyle("-fx-pref-width: 20em");
	}

	//--------------------------------------------------------------------
	protected void initValueFactories() {
		nameCol.setCellValueFactory(cdf -> new SimpleStringProperty(cdf.getValue().getModifyable().getName()));
		valCol .setCellValueFactory(cdf -> new SimpleObjectProperty<Number>(cdf.getValue().getPoints()));
		modCol.setCellValueFactory(cdf -> new SimpleObjectProperty<Number>(cdf.getValue().getModifier()));
		finCol.setCellValueFactory(cdf -> new SimpleObjectProperty<Number>(cdf.getValue().getValue()));

	}

	//--------------------------------------------------------------------
	protected void initCellFactories() {
		this.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> showHelpFor.set(n.getModifyable()));
	}
}
