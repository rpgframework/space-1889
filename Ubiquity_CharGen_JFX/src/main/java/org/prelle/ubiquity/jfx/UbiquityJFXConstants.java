/**
 * 
 */
package org.prelle.ubiquity.jfx;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

/**
 * @author prelle
 *
 */
public interface UbiquityJFXConstants {
	
	public final static String PREFIX = "org/prelle/ubiquity/jfx";

	public static final String BASE_LOGGER_NAME = "ubiquity.jfx";
	
	public static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle("org/prelle/ubiquity/jfx/i18n/chargenui");

}
