package org.prelle.ubiquity.jfx.pages;

import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.DoubleSection;
import org.prelle.rpgframework.jfx.Section;
import org.prelle.ubiquity.BasePluginData;
import org.prelle.ubiquity.ResourceValue;
import org.prelle.ubiquity.charctrl.CharacterController;
import org.prelle.ubiquity.jfx.sections.NotesSection;
import org.prelle.ubiquity.jfx.sections.ResourceSection;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;

/**
 * @author Stefan Prelle
 *
 */
public class UbiquityResourcePage extends UbiquityManagedScreenPage {

	private ScreenManagerProvider provider;

	private ResourceSection resources;
	private NotesSection notes;

	private Section secLine1;

	//-------------------------------------------------------------------
	public UbiquityResourcePage(CharacterController control, CharacterHandle handle, ScreenManagerProvider provider) {
		super(control, handle);
		this.setId("ubiquity-resource");
		this.setTitle(control.getModel().getName());
		this.provider = provider;
		this.handle   = handle;
		
		initComponents();
		initInteractivity();

		refresh();
	}

	//-------------------------------------------------------------------
	private void initBasicData() {
		resources = new ResourceSection(UI.getString("section.resources"), charGen, provider);
		notes   = new NotesSection(UI.getString("section.notes"), charGen, provider);

		secLine1 = new DoubleSection(resources, notes);
		getSectionList().add(secLine1);

		// Interactivity
		resources.showHelpForProperty().addListener( (ov,o,n) -> updateHelp( (n!=null)?((ResourceValue)n).getModifyable():null));
	}

	//-------------------------------------------------------------------
	protected void initComponents() {
		setPointsNameProperty(UI.getString("label.experience.short"));

		initBasicData();
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cmdPrint.setOnAction( ev -> {BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, charGen.getModel());});
		cmdDelete.setOnAction( ev -> BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, charGen.getModel()));
	}

	//-------------------------------------------------------------------
	private void updateHelp(BasePluginData data) {
		if (data!=null) {
			this.setDescriptionHeading(data.getName());
			this.setDescriptionPageRef(data.getProductName()+" "+data.getPage());
			this.setDescriptionText(data.getHelpText());
		} else {
			this.setDescriptionHeading(null);
			this.setDescriptionPageRef(null);
			this.setDescriptionText(null);
		}
	}

}
