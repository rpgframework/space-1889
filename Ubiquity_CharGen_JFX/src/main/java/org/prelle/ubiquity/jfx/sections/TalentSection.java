package org.prelle.ubiquity.jfx.sections;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.DescriptionPane;
import org.prelle.rpgframework.jfx.OptionalDescriptionPane;
import org.prelle.ubiquity.Talent;
import org.prelle.ubiquity.TalentValue;
import org.prelle.ubiquity.charctrl.CharacterController;
import org.prelle.ubiquity.jfx.cells.TalentListCell;
import org.prelle.ubiquity.jfx.cells.TalentValueListCell;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Stefan Prelle
 *
 */
public class TalentSection extends GenericListSection<TalentValue> {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(TalentSection.class.getName());

	//-------------------------------------------------------------------
	public TalentSection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(title, ctrl, provider);
		list.setCellFactory( lv -> new TalentValueListCell(ctrl.getTalentController()));

		setData(ctrl.getModel().getTalents());
		list.setStyle("-fx-pref-height: 50em; -fx-pref-width: 32em");
		GridPane.setVgrow(list, Priority.ALWAYS);
		list.setMaxHeight(Double.MAX_VALUE);

		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> getDeleteButton().setDisable(n==null));
		initInteractivity();
	}

	//-------------------------------------------------------------------
	protected void initInteractivity() {
		super.initInteractivity();
		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			Platform.runLater( () -> {
				getDeleteButton().setDisable( !control.getTalentController().canBeDeselected(n));
			});
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.jfx.sections.GenericListSection#onAdd()
	 */
	@Override
	protected void onAdd() {
		logger.trace("onAdd");
		Label question = new Label(RES.getString("section.talent.dialog.add.question"));
		ListView<Talent> myList = new ListView<>();
		myList.setCellFactory(lv -> new TalentListCell(control));
		myList.getItems().addAll(control.getTalentController().getAvailable());
		myList.setPlaceholder(new Label(RES.getString("section.talent.dialog.add.placeholder")));
		VBox innerLayout = new VBox(10, question, myList);

		final DescriptionPane descr = new DescriptionPane();
		OptionalDescriptionPane layout = new OptionalDescriptionPane(innerLayout, descr);

		myList.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			descr.setText(n.getName(), n.getProductName()+" "+n.getPage(), n.getHelpText());
		});


		CloseType result = provider.getScreenManager().showAlertAndCall(AlertType.QUESTION, RES.getString("section.talent.dialog.add.title"), layout);
		if (result==CloseType.OK) {
			Talent value = myList.getSelectionModel().getSelectedItem();
			if (value!=null) {
				logger.debug("Try add resource: "+value);
				myList.getItems().add(value);
				control.getTalentController().select(value);
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.jfx.sections.GenericListSection#onDelete()
	 */
	@Override
	protected void onDelete() {
		logger.trace("onDelete");
		TalentValue toDelete = list.getSelectionModel().getSelectedItem();
		if (toDelete!=null) {
			logger.info("Try remove talent: "+toDelete);
			if (control.getTalentController().deselect(toDelete)) {
				list.getItems().remove(toDelete);
				list.getSelectionModel().clearSelection();
			} else 
				logger.warn("GUI allowed removing a talent which cannot be deselected: "+toDelete);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.Section#refresh()
	 */
	@Override
	public void refresh() {
		setData(control.getModel().getTalents());
	}

}
