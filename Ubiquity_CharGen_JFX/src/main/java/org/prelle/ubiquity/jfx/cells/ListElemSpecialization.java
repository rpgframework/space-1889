package org.prelle.ubiquity.jfx.cells;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

import org.prelle.ubiquity.Skill;
import org.prelle.ubiquity.SkillSpecialization;
import org.prelle.ubiquity.SkillSpecializationValue;

public class ListElemSpecialization {
	public Skill skill;
	public SkillSpecialization data;
	public SkillSpecializationValue val;
	public BooleanProperty editable = new SimpleBooleanProperty();
}