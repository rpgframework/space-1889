/**
 *
 */
package org.prelle.ubiquity.jfx.wizard;

import java.io.InputStream;
import java.util.List;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.ubiquity.Flaw;
import org.prelle.ubiquity.FlawValue;
import org.prelle.ubiquity.UbiquityRuleset;
import org.prelle.ubiquity.charctrl.FlawController;
import org.prelle.ubiquity.chargen.CharacterGenerator;
import org.prelle.ubiquity.chargen.FlawGenerator;
import org.prelle.ubiquity.chargen.event.GenerationEvent;
import org.prelle.ubiquity.chargen.event.GenerationEventDispatcher;
import org.prelle.ubiquity.chargen.event.GenerationEventListener;
import org.prelle.ubiquity.jfx.UbiquityJFXConstants;

import de.rpgframework.RPGFrameworkLoader;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.util.Callback;

/**
 * @author Stefan
 *
 */
public class WizardPageFlaws extends WizardPage implements GenerationEventListener {

	protected static Logger logger = LogManager.getLogger("ubiquity.jfx");

	private static PropertyResourceBundle UI = UbiquityJFXConstants.UI;

	private UbiquityRuleset ruleset;
	private CharacterGenerator charGen;

	// Content
	private Label    explain;
	private ListView<Flaw> listPossible;
	private ListView<FlawValue> listSelected;

	private VBox boxSpecial;
	private Label descName;
	private Label reference;
	private Label descText;

	private VBox content;
	private CheckBox cbAdmission;

	//--------------------------------------------------------------------
	public WizardPageFlaws(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;
		this.ruleset = charGen.getRuleset();

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
		GenerationEventDispatcher.addListener(this);

		// Select standard
		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("wizard.selectFlaw.title"));

		cbAdmission = new CheckBox(UI.getString("wizard.selectFlaw.admission"));
		cbAdmission.setWrapText(true);
		/*
		 * Content
		 */
		explain = new Label(UI.getString("wizard.selectFlaw.descr"));
		explain.setWrapText(true);

		listPossible = new ListView<Flaw>();
		listPossible.setCellFactory(new Callback<ListView<Flaw>, ListCell<Flaw>>() {
			public ListCell<Flaw> call(ListView<Flaw> param) {
				FlawListCell cell = new FlawListCell(WizardPageFlaws.this);
				return cell;
			}
		});
		listPossible.setStyle("-fx-border-width: 1px; -fx-border-color: black;");
		Label placeholderPossible = new Label(UI.getString("wizard.selectFlaw.available.placeholder"));
		placeholderPossible.setWrapText(true);
		listPossible.setPlaceholder(placeholderPossible);

		listSelected = new ListView<FlawValue>();
		listSelected.setCellFactory(new Callback<ListView<FlawValue>, ListCell<FlawValue>>() {
			public ListCell<FlawValue> call(ListView<FlawValue> param) {
				FlawValueListCell lc = new FlawValueListCell(charGen.getFlawController());
				lc.prefWidthProperty().bind(listSelected.widthProperty().subtract(4));
				return lc;
			}
		});
		listSelected.setStyle("-fx-border-width: 1px; -fx-border-color: black; -fx-background-color: derive(white,-10%)");
		listSelected.getItems().addAll(charGen.getModel().getFlaws());
		Label placeholderSelected = new Label(UI.getString("wizard.selectFlaw.possible.placeholder"));
		placeholderSelected.setWrapText(true);
		listSelected.setPlaceholder(placeholderSelected);

		/*
		 * Column showing per skill information
		 */
		boxSpecial = new VBox(5);
		descName = new Label();

		reference = new Label();

		descText = new Label();
		descText.setWrapText(true);
		descText.setMaxHeight(Double.MAX_VALUE);
		descText.setTextAlignment(TextAlignment.JUSTIFY);
		descText.setAlignment(Pos.TOP_LEFT);
//		focusSkillDescription.setPrefHeight(90);

		content = new VBox();
		content.setSpacing(5);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		// Content node
		explain.setStyle("-fx-pref-width: 54em");
//		explain.setMaxWidth(720);

		/*
		 * Info column
		 */
		VBox boxInfo = new VBox(5);
		boxInfo.getChildren().addAll(descName, boxSpecial, reference, descText);

		HBox sideByside = new HBox(20);
		sideByside.getChildren().addAll(listPossible, listSelected, boxInfo);
		listPossible.setStyle("-fx-pref-width: 18em");
		listSelected.setStyle("-fx-pref-width: 18em");
		boxInfo.setStyle("-fx-pref-width: 18em");
		HBox.setHgrow(listPossible, Priority.NEVER);
		HBox.setHgrow(boxInfo, Priority.NEVER);

		content.getChildren().addAll(explain,  sideByside);
		VBox.setVgrow(sideByside, Priority.ALWAYS);
		sideByside.setMaxHeight(Double.MAX_VALUE);
		super.setContent(content);

		// Side node
		setSide(cbAdmission);

//		// Image
//		String fname = "data/space1889/archetype_soldier.png";
//		InputStream in = getClass().getClassLoader().getResourceAsStream(fname);
//		if (in!=null) {
//			setImage(new Image(in));
//		} else
//			logger.warn("Missing image at "+fname);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		explain.getStyleClass().add("text-body");
		descName.getStyleClass().add("section-head");
		descName.setWrapText(true);
		descText.getStyleClass().add("text-body");
		descText.setWrapText(true);
		cbAdmission.getStyleClass().add("text-body");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		listPossible.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			selectionChanged(n);
		});
		listSelected.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null)
				selectionChanged(n.getFlaw());
			else
				selectionChanged(null);

		});

		cbAdmission.selectedProperty().addListener((ov,o,n) -> {
			((FlawGenerator)charGen.getFlawController()).setAdmission(n);
		});

		listPossible.setOnDragDetected(event -> dragStarted(listPossible, event));
		listPossible.setOnDragOver    (event -> dragOver(event));
		listPossible.setOnDragDropped (event -> dragDropped(listPossible, event));

		listSelected.setOnDragDetected(event -> dragStarted(listSelected, event));
		listSelected.setOnDragOver    (event -> dragOver(event));
		listSelected.setOnDragDropped (event -> dragDropped(listSelected, event));
	}

	//-------------------------------------------------------------------
	private void selectionChanged(Flaw selected) {
		logger.debug("Selected "+selected);
		// Show specializations
		boxSpecial.getChildren().clear();

		if (selected==null) {
			reference.setText(null);
			descName.setText(null);
			descText.setText(null);
		} else {
			descName.setText(selected.getName());

			// Update page reference
			int page = selected.getPage();
			String product = selected.getProductName();
			String format = UI.getString("format.pagereference");
			if (page>0)
				reference.setText(String.format(format, product, page));
			else
				reference.setText(String.format(format, product, "?"));

			// Detect the required license
			String license = selected.getPlugin().getID();
			logger.debug("License to show "+selected+" is "+license);
			try {
				if (RPGFrameworkLoader.getInstance().getLicenseManager().hasLicense(ruleset.getRoleplayingSystem(), license))
					if (selected.getHelpResourceBundle()!=null)
						descText.setText( selected.getHelpResourceBundle().getString("flaw."+selected.getId()+".desc") );
				else
					descText.setText(null);
			} catch (MissingResourceException e) {
				logger.error("Missing "+e.getKey()+" in "+selected.getHelpResourceBundle().getBaseBundleName());
				descText.setText(e.getMessage());
			}
		}
	}

	//-------------------------------------------------------------------
	private void refresh() {
		listPossible.getItems().clear();
		listSelected.getItems().clear();

		listPossible.getItems().addAll(charGen.getFlawController().getAvailable());
		listSelected.getItems().addAll(charGen.getModel().getFlaws());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.ubiquity.chargen.event.GenerationEvent)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case FLAW_AVAILABLE_CHANGED:
			logger.debug("RCV "+event);
			listPossible.getItems().clear();
			listPossible.getItems().addAll((List<Flaw>) event.getKey());
			break;
		case FLAW_ADDED:
			logger.debug("RCV "+event);
			listSelected.getItems().clear();
			listSelected.getItems().addAll(charGen.getModel().getFlaws());
			break;
		case FLAW_REMOVED:
			logger.debug("RCV "+event);
			listSelected.getItems().remove( (FlawValue)event.getKey() );
			break;
		default:
		}
	}

	//--------------------------------------------------------------------
	void onFlawDoubleClicked(Flaw current) {
		logger.debug("double click on "+current);
		charGen.getFlawController().select(current);
	}

	//-------------------------------------------------------------------
	private void dragStarted(ListView<?> list, MouseEvent event) {
		logger.debug("Drag started "+event.getSource());
		String id = null;
        Label dummy = new Label();
		if (list==listPossible) {
			Flaw flaw = listPossible.getSelectionModel().getSelectedItem();
			if (flaw!=null)
				id = "flaw:"+flaw.getId();
			dummy.setText(flaw.getName());
		} else if (list==listSelected) {
			FlawValue flaw = listSelected.getSelectionModel().getSelectedItem();
			if (flaw!=null)
				id = "flaw:"+flaw.getFlaw().getId();
			dummy.setText(flaw.getFlaw().getName());
		}

		Node source = (Node) event.getSource();
		/* drag was detected, start a drag-and-drop gesture*/
        /* allow any transfer mode */
        Dragboard db = source.startDragAndDrop(TransferMode.ANY);

        /* Put a string on a dragboard */
        ClipboardContent content = new ClipboardContent();
        content.putString(id);
        db.setContent(content);

        /* Drag image */
        new Scene(dummy);
        WritableImage snapshot = dummy.snapshot(new SnapshotParameters(), null);
        db.setDragView(snapshot);

        event.consume();
    }

	//-------------------------------------------------------------------
	private void dragOver(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString() && event.getDragboard().getString().startsWith("flaw:")) {
            /* allow for both copying and moving, whatever user chooses */
            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        }
	}

	//-------------------------------------------------------------------
	private void dragDropped(ListView<?> list, DragEvent event) {
		logger.debug("Drag dropped   target="+event.getSource());
        Dragboard db = event.getDragboard();
        if (db.hasString()) {
        	String id = db.getString();
        	if (list==listSelected) {
        		logger.debug("Dropped "+id+" in selected");
        		if (!id.startsWith("flaw:"))
        			return;
        		Flaw flaw = ruleset.getFlaw(id.substring(5));
        		charGen.getFlawController().select(flaw);
                event.setDropCompleted(true);
                event.consume();
                return;
        	} else
        	if (list==listPossible) {
        		logger.debug("Dropped "+id+" in possible");
        		if (!id.startsWith("flaw:"))
        			return;
            	String flawID = id.substring(5);
        		for (FlawValue fVal : charGen.getModel().getFlaws()) {
        			if (fVal.getFlaw().getId().equals(flawID)) {
                		charGen.getFlawController().deselect(fVal);
                        event.setDropCompleted(true);
                        event.consume();
                        return;
        			}
        		}
                event.setDropCompleted(false);
                event.consume();
                return;
        	}
        }
	}

}

class FlawListCell extends ListCell<Flaw> implements EventHandler<MouseEvent> {

	private WizardPageFlaws parent;
	private Flaw current;

	//--------------------------------------------------------------------
	public FlawListCell(WizardPageFlaws parent) {
		this.parent = parent;
		setOnMouseClicked(this);
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(Flaw item, boolean empty) {
		super.updateItem(item, empty);
		current = item;

		if (empty) {
			setText(null);
			current = null;
			return;
		}

		setText(item.getName());
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.event.EventHandler#handle(javafx.event.Event)
	 */
	@Override
	public void handle(MouseEvent event) {
		if (current!=null && event.getClickCount()==2)
			parent.onFlawDoubleClicked(current);
	}
}

//--------------------------------------------------------------------
//--------------------------------------------------------------------
class FlawValueListCell extends ListCell<FlawValue> implements EventHandler<MouseEvent> {

	private FlawController ctrl;

	private FlawValue current;

	//--------------------------------------------------------------------
	public FlawValueListCell(FlawController control) {
		this.ctrl = control;
		setOnMouseClicked(this);
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(FlawValue item, boolean empty) {
		super.updateItem(item, empty);
		current = item;

		if (empty) {
			setText(null);
			setGraphic(null);
			current = null;
			return;
		}

		setText(item.getFlaw().getName());
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.event.EventHandler#handle(javafx.event.Event)
	 */
	@Override
	public void handle(MouseEvent event) {
		if (current!=null && event.getClickCount()==2)
			ctrl.deselect(current);
	}

}