package org.prelle.ubiquity.jfx.sections;

import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.ubiquity.charctrl.CharacterController;

import javafx.scene.control.TextArea;

/**
 * @author Stefan Prelle
 *
 */
public class NotesSection extends SingleSection {

	private CharacterController control;
	private TextArea taNotes;
	
	//-------------------------------------------------------------------
	public NotesSection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(provider, title, null);
		this.control = ctrl;
		taNotes = new TextArea();
		setContent(taNotes);
		
		taNotes.textProperty().addListener( (ov,o,n) -> control.getModel().setNotes(n));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.Section#refresh()
	 */
	@Override
	public void refresh() {
		taNotes.setText(control.getModel().getNotes());
	}

}
