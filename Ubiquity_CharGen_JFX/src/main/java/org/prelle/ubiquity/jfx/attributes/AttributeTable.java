/**
 *
 */
package org.prelle.ubiquity.jfx.attributes;

import java.util.PropertyResourceBundle;

import org.prelle.javafx.skin.GridPaneTableViewSkin;
import org.prelle.rpgframework.jfx.NumericalValueTableCell;
import org.prelle.ubiquity.Attribute;
import org.prelle.ubiquity.AttributeValue;
import org.prelle.ubiquity.charctrl.CharacterController;
import org.prelle.ubiquity.jfx.UbiquityJFXConstants;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 * @author Stefan
 *
 */
/**
 * @author Stefan
 *
 */
public class AttributeTable extends TableView<AttributeValue> {

	protected static PropertyResourceBundle RES = UbiquityJFXConstants.UI;

	protected CharacterController ctrl;

	protected TableColumn<AttributeValue, String> nameCol;
	private TableColumn<AttributeValue, AttributeValue> valCol;
	private TableColumn<AttributeValue, Number> modCol;
	private TableColumn<AttributeValue, Number> finCol;

	protected ObjectProperty<Attribute> showHelpFor;

	//--------------------------------------------------------------------
	public AttributeTable(CharacterController ctrl, Attribute[] attributes) {
		this.ctrl = ctrl;
		showHelpFor = new SimpleObjectProperty<Attribute>();

		setSkin(new GridPaneTableViewSkin<>(this));
		initColumns();
		initValueFactories();
		initCellFactories();
		initLayout();

		for (Attribute key : attributes) {
			getItems().add(ctrl.getModel().getAttribute(key));
		}

	}

	//--------------------------------------------------------------------
	protected void initColumns() {
		nameCol = new TableColumn<>(RES.getString("attribtable.col.name"));
		valCol  = new TableColumn<>(RES.getString("attribtable.col.value"));
		modCol  = new TableColumn<>(RES.getString("attribtable.col.modifier"));
		finCol  = new TableColumn<>(RES.getString("attribtable.col.fin"));

		modCol.setId("attribtable-mod");
		nameCol.setId("attribtable-name");
		finCol.setId("attribtable-final");
		valCol.setId("attribtable-val");

		nameCol.setPrefWidth(150);
		valCol.setPrefWidth(120);
		modCol.setPrefWidth(50);
		finCol.setPrefWidth(50);
		
		getSelectionModel().setCellSelectionEnabled(false);
	}

	//--------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	protected void initLayout() {
		getColumns().addAll(nameCol, valCol, modCol, finCol);
		setStyle("-fx-pref-width: 30em");
	}

	//--------------------------------------------------------------------
	protected void initValueFactories() {
		nameCol.setCellValueFactory(cdf -> new SimpleStringProperty(cdf.getValue().getModifyable().getName()));
		valCol .setCellValueFactory(cdf -> new SimpleObjectProperty<AttributeValue>(cdf.getValue()));
		modCol.setCellValueFactory(cdf -> new SimpleObjectProperty(cdf.getValue().getModifier()));
		finCol.setCellValueFactory(cdf -> new SimpleObjectProperty(cdf.getValue().getValue()));

	}

	//--------------------------------------------------------------------
	protected void initCellFactories() {
		valCol.setCellFactory( (col) -> new NumericalValueTableCell<Attribute,AttributeValue,AttributeValue>(ctrl.getAttributeController()));
		this.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> showHelpFor.set(n.getModifyable()));
	}

	//--------------------------------------------------------------------
	public ReadOnlyObjectProperty<Attribute> showHelpForProperty() {
		return showHelpFor;
	}

	//--------------------------------------------------------------------
	public void setControl(CharacterController control) {
		ctrl = control;
		super.refresh();
	}
}
