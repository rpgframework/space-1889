/**
 * 
 */
package org.prelle.ubiquity.jfx.pages;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.DevelopmentPage;
import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.UbiquityCore;
import org.prelle.ubiquity.charctrl.CharacterController;
import org.prelle.ubiquity.jfx.UbiquityJFXConstants;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public abstract class UbiquityDevelopmentPage extends DevelopmentPage {

	private static Logger logger = LogManager.getLogger("ubiquity.jfx");

	protected static PropertyResourceBundle UI = UbiquityJFXConstants.UI;

	protected ScreenManagerProvider provider;
	protected CharacterController control;
	protected UbiquityCharacter model;

	protected UbiquityExpLine expLine;
	
	//-------------------------------------------------------------------
	/**
	 */
	public UbiquityDevelopmentPage(CharacterController control, RoleplayingSystem rules, ScreenManagerProvider provider) {
		super(UI, rules);
		this.setId("ubiquity-development");
		this.setTitle(control.getModel().getName());
		this.provider = provider;
		this.control = control;
		model = control.getModel();
		logger.info("<init>()");
		
		expLine = new UbiquityExpLine();
		getCommandBar().setContent(expLine);

		refresh();
	}

	//-------------------------------------------------------------------
	@Override
	public void refresh() {
		logger.info("refresh");
		history.setData(UbiquityCore.getRuleset(RoleplayingSystem.SPACE1889).getUtilities().convertToHistoryElementList(model, model.getRuleset().getRoleplayingSystem()));
		expLine.setData(control.getModel());
		
		getSectionList().forEach(sect -> sect.refresh());
		setPointsFree(control.getModel().getExperienceFree());
	}

	//-------------------------------------------------------------------
	public void setData(UbiquityCharacter model) {
		this.model = model;
//		setTitle(model.getName()+" / "+UI.getString("label.development"));
		
//		history.getItems().clear();
//		history.getItems().addAll(SplitterTools.convertToHistoryElementList(model));
		refresh();
	}

}