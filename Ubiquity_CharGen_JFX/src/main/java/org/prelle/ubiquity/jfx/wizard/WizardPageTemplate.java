/**
 * 
 */
package org.prelle.ubiquity.jfx.wizard;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.ubiquity.GenerationTemplate;
import org.prelle.ubiquity.UbiquityRuleset;
import org.prelle.ubiquity.chargen.CharacterGenerator;
import org.prelle.ubiquity.jfx.UbiquityJFXConstants;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory.ListSpinnerValueFactory;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author Stefan
 *
 */
public class WizardPageTemplate extends WizardPage {
	
	protected static Logger logger = LogManager.getLogger("ubiquity.jfx");
	
	private static PropertyResourceBundle UI = UbiquityJFXConstants.UI;

	private UbiquityRuleset ruleset;
	private CharacterGenerator charGen;
	
	// Content
	private Label    explain;
	private Spinner<GenerationTemplate> spinTemplate;
	private Label    reference;
	private Label    description;
	
	private VBox content;

	//--------------------------------------------------------------------
	public WizardPageTemplate(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;
		this.ruleset = charGen.getRuleset();
		
		initComponents();
		initLayout();
		initStyle();
		initInteractivity();

		spinTemplate.getValueFactory().setValue(charGen.getTemplate());
		spinTemplate.increment();
		spinTemplate.decrement();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("wizard.selectGenerationTemplate.title"));

		
		/*
		 * Content
		 */
		explain = new Label(UI.getString("wizard.selectGenerationTemplate.descr"));
		explain.setWrapText(true);

		spinTemplate = new Spinner<>();
		spinTemplate.getStyleClass().add(Spinner.STYLE_CLASS_SPLIT_ARROWS_HORIZONTAL);
		ListSpinnerValueFactory<GenerationTemplate> factory = new ListSpinnerValueFactory<GenerationTemplate>(FXCollections.observableArrayList(charGen.getRuleset().getTemplates()));
		factory.setWrapAround(true);
		factory.setConverter(new StringConverter<GenerationTemplate>() {
			public String toString(GenerationTemplate val) { return val!=null?val.getName():"";}
			public GenerationTemplate fromString(String string) {return null;}
		});
		spinTemplate.setValueFactory(factory);
		
		description = new Label();
		description.setWrapText(true);
		reference = new Label();
		
		content = new VBox();
		content.setSpacing(5);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		// Content node
		explain.setPrefWidth(550);
		description.setPrefWidth(550);
		
		content.getChildren().addAll(explain,  spinTemplate);
		content.getChildren().addAll(reference, description);
		super.setContent(content);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		explain.getStyleClass().add("text-body");
		description.getStyleClass().add("text-body");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		spinTemplate.valueProperty().addListener(new ChangeListener<GenerationTemplate>() {
			public void changed(ObservableValue<? extends GenerationTemplate> observable,
					GenerationTemplate oldValue, GenerationTemplate newValue) {
				charGen.setTemplate(newValue);
				update(newValue);
			}
		});
		
	}

	//-------------------------------------------------------------------
	private void update(GenerationTemplate value) {
//		Image img = imageByGenerationTemplate.get(value);
//		if (img==null) {
//			String fname = "data/space1889/archetype_"+value.getId()+".png";
//			logger.trace("Load "+fname);
//			InputStream in = getClass().getClassLoader().getResourceAsStream(fname);
//			if (in!=null) {
//				img = new Image(in);
//				imageByGenerationTemplate.put(value, img);
//			} else
//				logger.warn("Missing image at "+fname);
//		}
//		setImage(img);

		int page = value.getPage();
		String product = value.getProductName();
		String format = UI.getString("format.pagereference");
		if (page>0)
			reference.setText(String.format(format, product, page));
		else
			reference.setText(String.format(format, product, "?"));

		
		description.setText(value.getHelpText());
	}

	//-------------------------------------------------------------------
	/**
	 * Called from Wizard when page is left
	 */
	public void pageLeft(CloseType type) {
		logger.error("pageLeft("+type+")");
		charGen.setTemplate(spinTemplate.getValue());
	}

}

