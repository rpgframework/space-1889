/**
 * 
 */
package org.prelle.ubiquity.jfx.cells;

import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ListCell;

import org.prelle.ubiquity.SkillValue;

/**
 * @author prelle
 *
 */
public class SkillValueListCell extends ListCell<SkillValue> {

	//-------------------------------------------------------------------
	/**
	 */
	public SkillValueListCell() {
		setContentDisplay(ContentDisplay.RIGHT);
	}

	//-------------------------------------------------------------------
	@Override
	public void updateItem(SkillValue item, boolean empty) {
		super.updateItem(item, empty);
		
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			setText(item.getModifyable().getName());
		}
		
	}

}
