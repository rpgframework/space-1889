package org.prelle.ubiquity.jfx.develop;

import java.io.ByteArrayInputStream;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.FormatStyle;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.ubiquity.Resource;
import org.prelle.ubiquity.ResourceValue;
import org.prelle.ubiquity.RewardImpl;
import org.prelle.ubiquity.UbiquityRuleset;
import org.prelle.ubiquity.charctrl.ResourceController;
import org.prelle.ubiquity.jfx.UbiquityJFXConstants;
import org.prelle.ubiquity.modifications.ResourceModification;

import de.rpgframework.genericrpg.Reward;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.products.Adventure;
import de.rpgframework.products.ProductServiceLoader;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;
import javafx.util.converter.LocalTimeStringConverter;

public class RewardBox extends HBox {
	
	private static Logger logger = LogManager.getLogger("ubiquity.jfx");
	
	private static PropertyResourceBundle res = UbiquityJFXConstants.UI;
	
	enum ViewMode {
		ADDITION,
		MODIFICATION
	}
	
	class RewardResourceController implements ResourceController {
		private UbiquityRuleset ruleset;
		public RewardResourceController(UbiquityRuleset rules) {
			this.ruleset = rules;
		}
		public double getPointsLeft() { return 0; }
		public List<Resource> getAvailableResources() { return ruleset.getResources(); }
		public boolean canBeSelected(Resource res) { return true; }
		public boolean canBeDeSelected(ResourceValue key) { return true;}
		public ResourceValue select(Resource talent) { return new ResourceValue(talent, 0); }
		public boolean deselect(ResourceValue key) { return false; }
		public boolean canBeDecreased(ResourceValue key) { return key.getPoints()>1; }
		public boolean canBeIncreased(ResourceValue key) {return key.getPoints()<5; }
		public boolean increase(ResourceValue ref) {
			if (canBeIncreased(ref)) {
				ref.setPoints(ref.getPoints()+1);
				return true;
			}
			return false;
		}
		public boolean decrease(ResourceValue ref) {
			if (canBeDecreased(ref)) {
				ref.setPoints(ref.getPoints()-1);
				return true;
			}
			return false;
		}
		@Override
		public ResourceValue findResourceReference(Resource res, String descr,
				String idref) {
			return null;
		}
		@Override
		public void trash(ResourceValue ref) {
		}
		
	}
	private UbiquityRuleset ruleset;
	private Reward model;
	private ViewMode mode;
	
	private TextField tfDescr;
	private ChoiceBox<Adventure> cbAdventure;
	private TextField tfExp;
	private ResourcePane resources;
	private ImageView cover;
	private Spinner<LocalTime> timeSpinner;
	private DatePicker datePicker;
	
	private BooleanProperty enoughData;
	
	//-------------------------------------------------------------------
	public RewardBox(UbiquityRuleset ruleset) {
		super(20);
		mode = ViewMode.ADDITION;
		this.ruleset = ruleset;
		initComponents();
		initLayout();
		initInterActivity();
	}
	
	//-------------------------------------------------------------------
	public RewardBox(UbiquityRuleset ruleset, Reward elem) {
		super(20);
		mode = ViewMode.MODIFICATION;
		this.ruleset = ruleset;
		initComponents();
		initLayout();
		initInterActivity();
		
		setData(elem);
	}
	
	//--------------------------------------------------------------------
	public BooleanProperty enoughDataProperty() { return enoughData; }
	public boolean hasEnoughData() { return enoughData.get(); }
	
	//--------------------------------------------------------------------
	private void initComponents() {
		LocalDateTime now = LocalDateTime.now();
		
		enoughData = new SimpleBooleanProperty(true);
		datePicker = new DatePicker();
		datePicker.setValue(now.toLocalDate());
		timeSpinner = new Spinner<LocalTime>(new SpinnerValueFactory<LocalTime>(){
			{
	            setConverter(new LocalTimeStringConverter(FormatStyle.MEDIUM));
	        }
			@Override
			public void decrement(int steps) {
				 if (getValue() == null)
		                setValue(LocalTime.now());
		            else {
		                LocalTime time = (LocalTime) getValue();
		                setValue(time.minusMinutes(steps));
		            }
			}

			@Override
			public void increment(int steps) {
				if (this.getValue() == null)
	                setValue(LocalTime.now());
	            else {
	                LocalTime time = (LocalTime) getValue();
	                setValue(time.plusMinutes(steps));
	            }
			}});
		timeSpinner.setEditable(true);
		timeSpinner.getEditor().textProperty().addListener( (ov,o,n) -> {
			LocalTime newVal = timeSpinner.getValueFactory().getConverter().fromString(n);
			timeSpinner.getValueFactory().setValue(newVal);
		});
		timeSpinner.getValueFactory().setValue(now.toLocalTime());
		/*
		 * Form
		 */
		tfDescr = new TextField();
		cbAdventure = new ChoiceBox<>();
		cbAdventure.getItems().addAll(ProductServiceLoader.getInstance().getAdventures(ruleset.getRoleplayingSystem()));
		cbAdventure.setConverter(new StringConverter<Adventure>() {
			public String toString(Adventure value) {return value!=null?value.getTitle():"";}
			public Adventure fromString(String string) {return null;}
		});
		Collections.sort(cbAdventure.getItems());
		tfExp = new TextField(); 
		tfExp.setMaxWidth(100);
		resources = new ResourcePane(new RewardResourceController(ruleset), true);
		resources.setPrefHeight(150);
		
		cover = new ImageView();
		cover.setFitWidth(200);
		cover.setFitHeight(300);
	}
	
	//--------------------------------------------------------------------
	private void initLayout() {
		Label title_l = new Label(res.getString("label.title"));
		Label adv_l = new Label(res.getString("label.adventure"));
		Label exp_l = new Label(res.getString("label.experience"));
		Label res_l = new Label(res.getString("label.resource"));
		Label dat_l = new Label(res.getString("label.when"));
		res_l.setMaxHeight(Double.MAX_VALUE);
		res_l.setAlignment(Pos.TOP_LEFT);

		HBox timeAndDate = new HBox(5);
		timeAndDate.getChildren().addAll(datePicker, timeSpinner);
		
		
		VBox form = new VBox(5);
		form.getChildren().addAll(title_l, tfDescr, adv_l, cbAdventure, dat_l, timeAndDate);
		if (mode!=ViewMode.MODIFICATION)
			form.getChildren().addAll(exp_l, tfExp, res_l, resources);
		VBox.setMargin(adv_l, new Insets(10,0,0,0));
		VBox.setMargin(exp_l, new Insets(10,0,0,0));
		VBox.setMargin(res_l, new Insets(10,0,0,0));
		
		getChildren().addAll(cover, form);
	}
	
	//--------------------------------------------------------------------
	private void initInterActivity() {
		cbAdventure.getSelectionModel().selectedItemProperty().addListener((ov,o,n) -> {
			logger.info("Has "+n+" cover? "+(n!=null && (n.getCover()!=null)));
			if (n!=null && (n.getCover()!=null)) {
				Image img = new Image(new ByteArrayInputStream(n.getCover()));	
				cover.setImage(img);
			} else
				cover.setImage(null);
		}); 
		
		tfDescr.textProperty().addListener((ov,o,n) -> testEnoughData());
		tfExp.textProperty().addListener((ov,o,n) -> testEnoughData());
	}
	
	//--------------------------------------------------------------------
	public void setData(Reward value) {
		this.model = value;
		tfDescr.setText(value.getTitle());
		
		Instant dateTime = Instant.ofEpochMilli(value.getDate().getTime());
		LocalDateTime timeAndDate = LocalDateTime.ofInstant(dateTime, ZoneId.systemDefault());
		datePicker.setValue(timeAndDate.toLocalDate());
		timeSpinner.getValueFactory().setValue(timeAndDate.toLocalTime());
		
		// Collect EP
		tfExp.setText(String.valueOf(model.getExperiencePoints()));

		// Find adventure
		logger.debug("Search "+value.getId());
		if (value.getId()!=null) {
			for (Adventure adv : ProductServiceLoader.getInstance().getAdventures(ruleset.getRoleplayingSystem())) 
//				logger.debug("  Compare with "+adv.getId());
				if (value.getId().equals(adv.getId())) {
					cbAdventure.getSelectionModel().select(adv);
					break;
				}
		} else
			cbAdventure.getSelectionModel().clearSelection();
		
		// Other rewards
			for (Modification mod : model.getModifications()) {
				if (mod instanceof ResourceModification) {
					logger.warn("TODO: add resources");
				}
		}
	}
	
	//--------------------------------------------------------------------
	private void testEnoughData() {
		enoughData.set(testEnoughData_impl());
	}
	
	//--------------------------------------------------------------------
	private boolean testEnoughData_impl() {
		if (tfDescr.getText()==null || tfDescr.getText().length()<1) return false;
		if (tfExp.getText()==null || tfExp.getText().length()<1) return false;
		try {
			int foo = Integer.parseInt(tfExp.getText());
			if (foo==0)
				return false;
		} catch (NumberFormatException e) {
			return false;
		}
		
		return true;
	}
	
	//--------------------------------------------------------------------
	public RewardImpl getDataAsReward() {
		LocalDateTime dateAndTime = LocalDateTime.of(datePicker.getValue(), timeSpinner.getValue());
		ZonedDateTime zonedDateTime = dateAndTime.atZone(ZoneId.systemDefault());
		Instant instant = Instant.from(zonedDateTime);
		Date date = Date.from(instant);
		
		int exp = Integer.valueOf(tfExp.getText());
		RewardImpl ret = new RewardImpl(exp, tfDescr.getText());
		ret.setDate(date);
		if (cbAdventure.getValue()!=null)
			ret.setId(cbAdventure.getValue().getId());

		logger.debug("Resources in pane = "+resources.getResources());
		for (ResourceValue val : resources.getResources()) {
			ResourceModification rMod = new ResourceModification(val.getModifyable(), val.getPoints());
			rMod.setComment(val.getComment());
			ret.addModification(rMod);
		}

		
		return ret;
	}
}