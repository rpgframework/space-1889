package org.prelle.ubiquity.jfx.pages;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.rpgframework.jfx.CharacterDocumentView;
import org.prelle.ubiquity.charctrl.CharacterController;
import org.prelle.ubiquity.jfx.UbiquityJFXConstants;

import de.rpgframework.character.CharacterHandle;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;

/**
 * @author Stefan Prelle
 *
 */
public class UbiquityManagedScreenPage extends CharacterDocumentView {

	protected static Logger logger = LogManager.getLogger(UbiquityJFXConstants.BASE_LOGGER_NAME);
	
	protected static PropertyResourceBundle UI = UbiquityJFXConstants.UI;

	protected CharacterController charGen;
	protected CharacterHandle handle;

	protected MenuItem cmdPrint;
	protected MenuItem cmdDelete;
	
	protected UbiquityExpLine expLine;

	//-------------------------------------------------------------------
	public UbiquityManagedScreenPage(CharacterController charGen, CharacterHandle handle) {
		super();
		this.charGen = charGen;
		initPrivateComponents();
	}

	//-------------------------------------------------------------------
	private void initPrivateComponents() {
		/*
		 * Exp & Co.
		 */
		setPointsNameProperty(UI.getString("label.experience.short"));
		setPointsFree(charGen.getModel().getExperienceFree());
		expLine = new UbiquityExpLine();
		expLine.setData(charGen.getModel());
		
		cmdPrint = new MenuItem(UI.getString("command.primary.print"), new Label("\uE749"));
		cmdDelete = new MenuItem(UI.getString("command.primary.delete"), new Label("\uE74D"));
		setHandle(handle);
		
		getCommandBar().setContent(expLine);
	}

	//-------------------------------------------------------------------
	protected void setHandle(CharacterHandle value) {
		if (this.handle==value)
			return;
		if (this.handle!=null) {
			getCommandBar().getPrimaryCommands().removeAll(cmdPrint, cmdDelete);			
		}
		
		this.handle = value;
		if (handle!=null) {
			getCommandBar().getPrimaryCommands().addAll(cmdPrint, cmdDelete);
		}
	}

	//-------------------------------------------------------------------
	public void refresh() {
		expLine.setData(charGen.getModel());
		
		getSectionList().forEach(sect -> sect.refresh());
		setPointsFree(charGen.getModel().getExperienceFree());
	}

}
