/**
 * @author Stefan Prelle
 *
 */
module ubiquity.chargen.jfx {
	exports org.prelle.ubiquity.jfx.notes;
	exports org.prelle.ubiquity.jfx.talents;
	exports org.prelle.ubiquity.jfx.equip;
	exports org.prelle.ubiquity.jfx.skills;
	exports org.prelle.ubiquity.jfx.wizard;
	exports org.prelle.ubiquity.jfx.attributes;
	exports org.prelle.ubiquity.jfx.resources;
	exports org.prelle.ubiquity.jfx;
	exports org.prelle.ubiquity.jfx.develop;

	requires javafx.base;
	requires javafx.controls;
	requires javafx.extensions;
	requires javafx.graphics;
	requires org.apache.logging.log4j;
	requires rpgframework.api;
	requires rpgframework.api.jfx;
	requires rpgframework.jfx;
	requires ubiquity.chargen;
	requires ubiquity.core;
}