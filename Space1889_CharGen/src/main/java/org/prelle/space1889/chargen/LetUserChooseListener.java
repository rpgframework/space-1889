/**
 * 
 */
package org.prelle.space1889.chargen;

import org.prelle.ubiquity.modifications.ModificationImpl;
import org.prelle.ubiquity.modifications.ModificationChoice;

/**
 * @author prelle
 *
 */
public interface LetUserChooseListener {

	//-------------------------------------------------------------------
	public ModificationImpl[] letUserChoose(String choiceReason, ModificationChoice choice);
	
//	//-------------------------------------------------------------------
//	/**
//	 * @param mod
//	 * @param current  Currently existing value
//	 * @return
//	 */
//	public ResourceModAddOptions letUserChooseResource(ResourceModification mod, SelectedResource current);
	
}
