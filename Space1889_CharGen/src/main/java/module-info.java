/**
 * @author Stefan Prelle
 *
 */
module space1889.chargen {
	exports org.prelle.space1889.chargen;

	requires de.rpgframework.core;
	requires space1889.core;
	requires ubiquity.chargen;
	requires ubiquity.core;
	requires simple.persist;
	requires space1889.data;
}