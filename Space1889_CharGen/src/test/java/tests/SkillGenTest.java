package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.junit.Before;
import org.junit.Test;
import org.prelle.ubiquity.GenerationTemplate;
import org.prelle.ubiquity.Skill;
import org.prelle.ubiquity.SkillSpecialization;
import org.prelle.ubiquity.Talent;
import org.prelle.ubiquity.TalentValue;
import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.UbiquityCore;
import org.prelle.ubiquity.UbiquityRuleset;
import org.prelle.ubiquity.chargen.SkillGenerator;
import org.prelle.ubiquity.chargen.event.GenerationEvent;
import org.prelle.ubiquity.chargen.event.GenerationEventDispatcher;
import org.prelle.ubiquity.chargen.event.GenerationEventListener;
import org.prelle.ubiquity.chargen.event.GenerationEventType;

import de.rpgframework.ConfigContainer;
import de.rpgframework.ConfigOption;
import de.rpgframework.character.RulePlugin;
import de.rpgframework.character.RulePluginFeatures;
import de.rpgframework.core.CommandResult;
import de.rpgframework.core.CommandType;
import de.rpgframework.core.RoleplayingSystem;

public class SkillGenTest implements GenerationEventListener {

	class TestPlugin implements RulePlugin<UbiquityCharacter> {
		public boolean willProcessCommand(Object src, CommandType type, Object... values) {
			return false;
		}
		public CommandResult handleCommand(Object src, CommandType type, Object... values) {
			return null;
		}
		public String getID() {return "CORE";}
		public String getReadableName() {return "CORE";}
		public RoleplayingSystem getRules() {return RoleplayingSystem.SPACE1889;}
		public Collection<RulePluginFeatures> getSupportedFeatures() {
			return null;
		}
		public List<ConfigOption<?>> getConfiguration() {
			return null;
		}
		public void init(RulePluginProgessListener callback) {}
		public InputStream getAboutHTML() {return null;}
		public Collection<String> getRequiredPlugins() {return new ArrayList<>();}
		public void attachConfigurationTree(ConfigContainer addBelow) { }
		public List<String> getLanguages() {
			return Arrays.asList(Locale.GERMAN.getLanguage());
		}
	}

	private static UbiquityRuleset ruleset;
	private static PropertyResourceBundle i18NResources;
	private static PropertyResourceBundle i18NHelpResources;
	private static RulePlugin<UbiquityCharacter> plugin = new SkillGenTest().new TestPlugin();
	private static Skill SCIENCE;
	private static Skill BIOLOGY;
	private static Skill PHYSICS;
	private static Skill LARCENY;
	
	//-------------------------------------------------------------------
	static {
		i18NResources = (PropertyResourceBundle) ResourceBundle.getBundle("i18n/space1889/core");
		i18NHelpResources = (PropertyResourceBundle) ResourceBundle.getBundle("i18n/space1889/core_help");

		ruleset = UbiquityCore.createRuleset(RoleplayingSystem.SPACE1889);
		
		UbiquityCore.loadTemplates(ruleset, plugin, ClassLoader.getSystemResourceAsStream("data/space1889/templates.xml"), i18NResources, i18NHelpResources);		
		UbiquityCore.loadSkills(ruleset, plugin, ClassLoader.getSystemResourceAsStream("data/space1889/skills.xml"), i18NResources, i18NHelpResources);
		UbiquityCore.loadTalents(ruleset, plugin, ClassLoader.getSystemResourceAsStream("data/space1889/talents.xml"), i18NResources, i18NHelpResources);
		
		ruleset.getTemplate("promising").setDefaultChoice(true);
		
		SCIENCE = ruleset.getSkill("science");
		BIOLOGY = ruleset.getSkill("science.biology");
		PHYSICS = ruleset.getSkill("science.physics");
		LARCENY = ruleset.getSkill("larceny");
	}
	
	private SkillGenerator skillGen;
	private GenerationTemplate template;
	private UbiquityCharacter model;
	private List<GenerationEvent> events;

	//--------------------------------------------------------------------
	public SkillGenTest() {
		events   = new ArrayList<GenerationEvent>();
		GenerationEventDispatcher.addListener(this);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.ubiquity.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		events.add(event);
	}

	//--------------------------------------------------------------------
	private boolean eventWasDelivered(GenerationEventType type) {
		for (GenerationEvent ev : events) {
			if (ev.getType()==type)
				return true;
		}
		return false;
	}
	
	//--------------------------------------------------------------------
	@Before
	public void setUp() throws Exception {
		model = new UbiquityCharacter();
		template = ruleset.getDefaultTemplates();
		skillGen = new SkillGenerator(template, ruleset, model);
		events.clear();
	}

	//--------------------------------------------------------------------
	@Test
	public void testInitial() {
		assertTrue(skillGen.getAvailableSkills().contains(BIOLOGY));
		assertTrue(skillGen.getAvailableSkills().contains(PHYSICS));
		assertFalse(skillGen.getAvailableSkills().contains(SCIENCE));		
		assertTrue(skillGen.getAvailableSkills().contains(LARCENY));
	}

	//--------------------------------------------------------------------
	@Test
	public void testIncGroupWithoutTalent() {
		System.out.println("---testIncGroupWithoutTalent---------------------");
		/*
		 * Increasing a skillgroup without having the skillmaster
		 * talent should be impossible
		 */
		assertFalse(skillGen.canBeIncreased(SCIENCE));
		assertFalse(skillGen.increase(SCIENCE));
		
	}

	//--------------------------------------------------------------------
	@Test
	public void testSelectSkillmaster() {
		System.out.println("---testSelectSkillMaster---------------------");
		Talent skillmaster = ruleset.getTalent("skillmaster");
		// At first, select a unimportant skillmaster
		TalentValue skillmasterLareny = new TalentValue(skillmaster, LARCENY, 1); 
		model.addTalent(skillmasterLareny);
		GenerationEventDispatcher.fireEvent(
				new GenerationEvent(GenerationEventType.TALENT_ADDED, skillmasterLareny));

		// There should have been no SKILL_AVAILABLE_CHANGE
		// Since mastery for non group skill
		System.out.println("events = "+events);
		assertFalse(eventWasDelivered(GenerationEventType.SKILL_AVAILABLE_CHANGED));
		assertTrue(eventWasDelivered(GenerationEventType.POINTS_LEFT_SKILLS));
		
		// Regarding science, nothing should change to initial
		assertTrue(skillGen.getAvailableSkills().contains(BIOLOGY));
		assertTrue(skillGen.getAvailableSkills().contains(PHYSICS));
		assertFalse(skillGen.getAvailableSkills().contains(SCIENCE));		
		
		// Now add science skillmaster
		events.clear();
		TalentValue skillmasterScience = new TalentValue(skillmaster, SCIENCE, 1); 
		model.addTalent(skillmasterScience);
		GenerationEventDispatcher.fireEvent(
				new GenerationEvent(GenerationEventType.TALENT_ADDED, skillmasterScience));

		// There should have been a SKILL_AVAILABLE_CHANGE
		assertTrue(eventWasDelivered(GenerationEventType.SKILL_AVAILABLE_CHANGED));
		// New expected outcome is inverse to initial
		assertFalse(skillGen.getAvailableSkills().contains(BIOLOGY));
		assertFalse(skillGen.getAvailableSkills().contains(PHYSICS));
		assertTrue (skillGen.getAvailableSkills().contains(SCIENCE));		
	}

	//--------------------------------------------------------------------
	@Test
	public void testSimple() {
		double maxGP = skillGen.getPointsLeft();
		
		assertNotNull(model.getSkill(BIOLOGY));
		assertEquals(0, model.getSkill(BIOLOGY).getModifiedValue());
		skillGen.increase(BIOLOGY);
		assertTrue(eventWasDelivered(GenerationEventType.SKILL_ADDED));
		assertTrue(eventWasDelivered(GenerationEventType.POINTS_LEFT_SKILLS));
		assertNotNull(model.getSkill(BIOLOGY));
		assertEquals(1, model.getSkill(BIOLOGY).getPoints());
		assertEquals(maxGP-1, skillGen.getPointsLeft(), 0);
		// Increase again
		events.clear();
		skillGen.increase(BIOLOGY);
		assertTrue(eventWasDelivered(GenerationEventType.SKILL_CHANGED));
		assertTrue(eventWasDelivered(GenerationEventType.POINTS_LEFT_SKILLS));
		assertNotNull(model.getSkill(BIOLOGY));
		assertEquals(2, model.getSkill(BIOLOGY).getPoints());
		assertEquals(maxGP-2, skillGen.getPointsLeft(), 0);

		events.clear();
		skillGen.decrease(BIOLOGY);
		skillGen.decrease(BIOLOGY);
		assertEquals(maxGP, skillGen.getPointsLeft(), 0);
		assertNotNull(model.getSkill(BIOLOGY));
		assertEquals(0, model.getSkill(BIOLOGY).getModifiedValue());
		assertTrue(eventWasDelivered(GenerationEventType.SKILL_REMOVED));
		assertTrue(eventWasDelivered(GenerationEventType.POINTS_LEFT_SKILLS));
	}

	//--------------------------------------------------------------------
	@Test
	public void testSpecialization() {
		System.out.println("---testSpecialization-------------------");
		double maxGP = skillGen.getPointsLeft();
		
		SkillSpecialization SPEC = LARCENY.getSpecializations().get(0);
		SkillSpecialization OTHER_SPEC = LARCENY.getSpecializations().get(1);
		
		// This should not work since skill value is 0
		assertFalse(skillGen.canSelectSpecialization(SPEC));
		assertNull(skillGen.selectSpecialization(SPEC));
		assertFalse(eventWasDelivered(GenerationEventType.SKILL_CHANGED));
		assertFalse(eventWasDelivered(GenerationEventType.POINTS_LEFT_SKILLS));
		assertNotNull(model.getSkill(LARCENY));
		assertEquals(0, model.getSkill(LARCENY).getModifiedValue());
		assertEquals(maxGP, skillGen.getPointsLeft(), 0);
		
		// Let us increase the value to 1 and try again
		skillGen.increase(LARCENY);
		events.clear();
		assertTrue(skillGen.canSelectSpecialization(SPEC));
		assertNotNull(skillGen.selectSpecialization(SPEC));
		assertTrue(eventWasDelivered(GenerationEventType.SKILL_CHANGED));
		assertTrue(eventWasDelivered(GenerationEventType.POINTS_LEFT_SKILLS));
		assertNotNull(model.getSkill(LARCENY));
		assertTrue(model.getSkill(LARCENY).getSpecializations().contains(model.getSkill(SPEC.getSkill()).getSpecialization(SPEC)));
		assertEquals(maxGP-1.5, skillGen.getPointsLeft(), 0);
		// Nothing else selected
		assertFalse(model.getSkill(LARCENY).getSpecializations().contains(model.getSkill(OTHER_SPEC.getSkill()).getSpecialization(OTHER_SPEC)));
		
		// Now try to select a second spec. - this should not work
		events.clear();
		assertFalse(skillGen.canSelectSpecialization(OTHER_SPEC));
		assertNull(skillGen.selectSpecialization(OTHER_SPEC));
		assertEquals(maxGP-1.5, skillGen.getPointsLeft(), 0);
		assertFalse(model.getSkill(LARCENY).getSpecializations().contains(model.getSkill(OTHER_SPEC.getSkill()).getSpecialization(OTHER_SPEC)));
		assertFalse(eventWasDelivered(GenerationEventType.SKILL_CHANGED));
		assertFalse(eventWasDelivered(GenerationEventType.POINTS_LEFT_SKILLS));
		
		// Now deselect again 
		events.clear();
		assertTrue(skillGen.canDeselectSpecialization(model.getSkill(SPEC.getSkill()).getSpecialization(SPEC)));
		assertTrue(skillGen.deselectSpecialization(model.getSkill(SPEC.getSkill()).getSpecialization(SPEC)));
		assertFalse(model.getSkill(LARCENY).getSpecializations().contains(model.getSkill(SPEC.getSkill()).getSpecialization(SPEC)));
		assertEquals(maxGP-1, skillGen.getPointsLeft(), 0);
		// and select other spec again
		events.clear();
		assertTrue(skillGen.canSelectSpecialization(OTHER_SPEC));
		assertNotNull(skillGen.selectSpecialization(OTHER_SPEC));
		assertEquals(maxGP-1.5, skillGen.getPointsLeft(), 0);
		assertTrue(model.getSkill(LARCENY).getSpecializations().contains(model.getSkill(OTHER_SPEC.getSkill()).getSpecialization(OTHER_SPEC)));
		assertTrue(eventWasDelivered(GenerationEventType.SKILL_CHANGED));
		assertTrue(eventWasDelivered(GenerationEventType.POINTS_LEFT_SKILLS));
		
		// Now reduce skill to 0 - this should deselect it
		events.clear();
		assertTrue(skillGen.decrease(LARCENY));
		assertEquals(maxGP, skillGen.getPointsLeft(), 0);
		assertNotNull(model.getSkill(LARCENY));
		assertEquals(0, model.getSkill(LARCENY).getModifiedValue());
	}

	//--------------------------------------------------------------------
	@Test
	public void testSwitchSkillmaster() {
		System.out.println("---testSwitchSkillmaster-------------------");
		double maxGP = skillGen.getPointsLeft();
		
		// Set Biology to 3
		skillGen.increase(BIOLOGY);
		skillGen.increase(BIOLOGY);
		skillGen.increase(BIOLOGY);
		// Set Physics to 2
		skillGen.increase(PHYSICS);
		skillGen.increase(PHYSICS);
		assertEquals(maxGP-5, skillGen.getPointsLeft(), 0);

		// Now add science skillmaster
		events.clear();
		Talent skillmaster = ruleset.getTalent("skillmaster");
		TalentValue skillmasterScience = new TalentValue(skillmaster, SCIENCE, 1); 
		model.addTalent(skillmasterScience);
		GenerationEventDispatcher.fireEvent(
				new GenerationEvent(GenerationEventType.TALENT_ADDED, skillmasterScience));

		/*
		 * Expectation is that the highest skill assigned before
		 * in this skillgroup is now the value for the group, while
		 * the points invested in the other skills are set free
		 */
		assertTrue(eventWasDelivered(GenerationEventType.SKILL_ADDED));
		assertTrue(eventWasDelivered(GenerationEventType.SKILL_REMOVED));
		assertTrue(eventWasDelivered(GenerationEventType.SKILL_CHANGED));
		assertNotNull("Skillgroup not auto-added",model.getSkill(SCIENCE));
		assertEquals(3, model.getSkill(SCIENCE).getPoints());
		assertEquals(maxGP-3, skillGen.getPointsLeft(), 0);
		
		/*
		 * Revoking the talent should free the points invested in the
		 * group without investing them new one
		 */
	}		
	
}
