/**
 * 
 */
package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Date;

import org.junit.Test;
import org.prelle.rpgframework.space1889.data.Space1889DataPlugin;
import org.prelle.simplepersist.Serializer;
import org.prelle.space1889.Space1889Character;
import org.prelle.space1889.Space1889Core;
import org.prelle.ubiquity.Attribute;
import org.prelle.ubiquity.DummyRulePlugin;
import org.prelle.ubiquity.FlawValue;
import org.prelle.ubiquity.ResourceValue;
import org.prelle.ubiquity.RewardImpl;
import org.prelle.ubiquity.SkillSpecializationValue;
import org.prelle.ubiquity.SkillValue;
import org.prelle.ubiquity.TalentValue;
import org.prelle.ubiquity.UbiquityCharacter.Gender;
import org.prelle.ubiquity.items.CarriedItem;
import org.prelle.ubiquity.modifications.AttributeModification;
import org.prelle.ubiquity.modifications.SkillModification;

import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class CharacterSerializationTest {

	final static String SEP = System.getProperty("line.separator");
	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String DATA = HEAD
			+SEP+"<space1889char atype=\"celebrity\" expfree=\"5\" expinv=\"15\" motive=\"truth\">"
			+SEP+"   <name>Unnamed Ümläut</name>"
			+SEP+"   <hairColor>blond</hairColor>"
			+SEP+"   <eyeColor>eisgrau</eyeColor>"
			+SEP+"   <skinColor>hell</skinColor>"
			+SEP+"   <size>180</size>"
			+SEP+"   <weight>80</weight>"
			+SEP+"   <gender>FEMALE</gender>"
			+SEP+"   <age>27</age>"
			+SEP+"   <image>AQIDBA==</image>"
			+SEP+"   <attributes>"
			+SEP+"      <attribute id=\"BODY\" value=\"3\"/>"
			+SEP+"      <attribute id=\"DEXTERITY\" value=\"3\"/>"
			+SEP+"      <attribute id=\"STRENGTH\" value=\"2\"/>"
			+SEP+"      <attribute id=\"CHARISMA\" value=\"3\"/>"
			+SEP+"      <attribute id=\"INTELLIGENCE\" value=\"2\"/>"
			+SEP+"      <attribute id=\"WILLPOWER\" value=\"2\"/>"
			+SEP+"   </attributes>"
			+SEP+"   <skillvals>"
			+SEP+"      <skillval skill=\"athletics\" special=\"athletics:climbing:2\" value=\"1\"/>"
			+SEP+"   </skillvals>"
			+SEP+"   <talentvals>"
			+SEP+"      <talentval idref=\"alertness\" level=\"2\"/>"
			+SEP+"      <talentval idref=\"agile\" level=\"1\"/>"
			+SEP+"   </talentvals>"
			+SEP+"   <resourcevals>"
			+SEP+"      <resourceval idref=\"status\" level=\"-2\"/>"
			+SEP+"      <resourceval comment=\"Sprechender Toaster\" idref=\"artifact\" level=\"1\"/>"
			+SEP+"   </resourcevals>"
			+SEP+"   <flawvals>"
			+SEP+"      <flawval idref=\"blind\"/>"
			+SEP+"   </flawvals>"
			+SEP+"   <carried>"
			+SEP+"      <itemref count=\"1\" idref=\"coltPeacemaker\"/>"
			+SEP+"   </carried>"
			+SEP+"   <history>"
		    +SEP+"      <attrmod attr=\"CHARISMA\" val=\"1\"/>"
		    +SEP+"      <skillmod idref=\"athletics\" val=\"1\"/>"
		    +SEP+"   </history>"
		    +SEP+"   <rewards>"
		    +SEP+"      <reward date=\"2015-10-01T10:52:24.000+02:00\" exp=\"2\">"
		    +SEP+"         <title>Belohnung1</title>"
		    +SEP+"      </reward>"
		    +SEP+"      <reward date=\"2015-10-05T10:52:24.000+02:00\" exp=\"3\">"
		    +SEP+"         <title>Belohnung2</title>"
		    +SEP+"         <modifications>"
		    +SEP+"            <attrmod attr=\"CHARISMA\" val=\"1\"/>"
		    +SEP+"         </modifications>"
		    +SEP+"      </reward>"
		    +SEP+"   </rewards>"
			+SEP+"</space1889char>"+SEP;
	
	final static String DATA2 = HEAD
			+SEP+"<space1889char>"
			+SEP+"   <name>Unnamed Ümläut</name>"
			+SEP+"   <size>0</size>"
			+SEP+"   <weight>0</weight>"
			+SEP+"   <age>0</age>"
			+SEP+"   <attributes>"
			+SEP+"      <attribute id=\"BODY\"/>"
			+SEP+"      <attribute id=\"DEXTERITY\"/>"
			+SEP+"      <attribute id=\"STRENGTH\"/>"
			+SEP+"      <attribute id=\"CHARISMA\"/>"
			+SEP+"      <attribute id=\"INTELLIGENCE\"/>"
			+SEP+"      <attribute id=\"WILLPOWER\"/>"
			+SEP+"   </attributes>"
			+SEP+"   <notes>Hallo Welt"
			+SEP+"Mät Ümlautön! ß"
			+SEP+""
			+SEP+"&#x003c; und Klammern &#x003e;&#x003e;.</notes>"
			+SEP+"</space1889char>"+SEP;
	
	final static String DATA3 = HEAD
			+SEP+"<space1889char>"
			+SEP+"   <name>Unnamed Ümläut</name>"
			+SEP+"   <size>0</size>"
			+SEP+"   <weight>0</weight>"
			+SEP+"   <age>0</age>"
			+SEP+"   <attributes>"
			+SEP+"      <attribute id=\"BODY\"/>"
			+SEP+"      <attribute id=\"DEXTERITY\"/>"
			+SEP+"      <attribute id=\"STRENGTH\"/>"
			+SEP+"      <attribute id=\"CHARISMA\"/>"
			+SEP+"      <attribute id=\"INTELLIGENCE\"/>"
			+SEP+"      <attribute id=\"WILLPOWER\"/>"
			+SEP+"   </attributes>"
			+SEP+"   <notes>! Mit äöüß ?"
			+SEP+"Einladen ja"
			+SEP
			+SEP+"Geht auch speichern?"
			+SEP+"</notes>"
			+SEP+"</space1889char>"+SEP;
	
	final static Space1889Character CHARAC = new Space1889Character();
	
	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
		Space1889DataPlugin plugin = new Space1889DataPlugin();
		plugin.init( perc -> {});
		Space1889Core.initialize(new DummyRulePlugin<>(RoleplayingSystem.SPACE1889, "CORE"));
		
		CHARAC.initPrimaryAttributes();
		CHARAC.setName("Unnamed Ümläut");
		CHARAC.setGender(Gender.FEMALE);
		CHARAC.setSize(180);
		CHARAC.setWeight(80);
		CHARAC.setHairColor("blond");
		CHARAC.setSkinColor("hell");
		CHARAC.setEyeColor("eisgrau");
		CHARAC.setAge(27);
		byte[] image = new byte[]{(byte)1,(byte)2,(byte)3,(byte)4};
		CHARAC.setImage(image);
		CHARAC.setArchetype(Space1889Core.getRuleset().getArchetype("celebrity"));
		CHARAC.setMotivation(Space1889Core.getRuleset().getMotivation("truth"));
		CHARAC.getAttribute(Attribute.BODY     ).setPoints(3);
		CHARAC.getAttribute(Attribute.DEXTERITY).setPoints(3);
		CHARAC.getAttribute(Attribute.STRENGTH ).setPoints(2);
		CHARAC.getAttribute(Attribute.CHARISMA ).setPoints(3);
		CHARAC.getAttribute(Attribute.INTELLIGENCE).setPoints(2);
		CHARAC.getAttribute(Attribute.WILLPOWER).setPoints(2);
		
		SkillValue athletics = new SkillValue(Space1889Core.getRuleset().getSkill("athletics"), 1);
		athletics.addSpecialization(new SkillSpecializationValue(athletics.getModifyable().getSpecializations().get(0),2));
		CHARAC.addSkill(athletics);
		
		CHARAC.addTalent(new TalentValue(Space1889Core.getRuleset().getTalent("alertness"), 2));
		CHARAC.addTalent(new TalentValue(Space1889Core.getRuleset().getTalent("agile"), 1));
		
		CHARAC.addResource(new ResourceValue(Space1889Core.getRuleset().getResource("status"), -2));
		ResourceValue res = new ResourceValue(Space1889Core.getRuleset().getResource("artifact"), 1);
		res.setComment("Sprechender Toaster");
		CHARAC.addResource(res);
		
		CHARAC.addFlaw(new FlawValue(Space1889Core.getRuleset().getFlaw("blind")));
		
		CHARAC.addItem(new CarriedItem(Space1889Core.getRuleset().getItemTemplate("coltPeacemaker")));
		CHARAC.setExperienceInvested(15);
		
		RewardImpl rew1 = new RewardImpl(2, "Belohnung1");
		RewardImpl rew2 = new RewardImpl(3, "Belohnung2");
		rew1.setDate(new Date(115,9,01,10,52,24));
		rew2.setDate(new Date(115,9,05,10,52,24));
		rew2.addModification(new AttributeModification(Attribute.CHARISMA, 1));
		CHARAC.addReward(rew1);
		CHARAC.addReward(rew2);
		
		CHARAC.addToHistory(new SkillModification(Space1889Core.getRuleset().getSkill("athletics"), 1));
	}

	//-------------------------------------------------------------------
	public CharacterSerializationTest() throws Exception {
//		Format format = new Format("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
//		serializer = new Persister(new AnnotationStrategy(), format);
		serializer = Space1889Core.getRuleset().getSerializer();
	}

	//-------------------------------------------------------------------
	@Test
	public void serialize() {
		try {
			StringWriter out = new StringWriter();
			serializer.write(CHARAC, out);
			System.out.println(out.toString());
			
			BufferedReader got = new BufferedReader(new StringReader(out.toString()));
			BufferedReader exp = new BufferedReader(new StringReader(DATA));
			while (true) {
				String lineGot = got.readLine();
				String lineExp = exp.readLine();
				if (lineGot==null && lineExp!=null) {
					System.err.println("Different1:\nGot "+lineGot+"\nExp "+lineExp);
					fail("Different1:\nGot "+lineGot+"\nExp "+lineExp);
				}
				if (lineGot!=null && lineExp==null) {
					System.err.println("Different2:\nGot "+lineGot+"\nExp "+lineExp);
					fail("Different2:\nGot "+lineGot+"\nExp "+lineExp);
				}
				if (lineGot==null)
					break;
				if (!lineGot.equals(lineExp)) {
					System.err.println("Different3:\nGot ["+lineGot+"]"+lineGot.length()+"\nExp ["+lineExp+"]"+lineExp.length());
					fail("Different3:\nGot "+lineGot+"\nExp "+lineExp);
				}
			}
			System.out.println("Looks identical  "+DATA.length()+" vs "+out.toString().length());
			
			assertEquals(DATA, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() {
		try {
			Space1889Character read = serializer.read(Space1889Character.class, DATA);
			System.out.println(read);
			System.out.println(CHARAC.dump());
			
			assertEquals(CHARAC, read);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void getTalentSkillOptions() {
		Space1889Core.getRuleset().getTalent("skillapt").getSkillOptions();
	}

	//-------------------------------------------------------------------
	@Test
	public void serializeComment() {
		try {
			StringWriter out = new StringWriter();
			Space1889Character MYCHARAC = new Space1889Character();
			MYCHARAC.initPrimaryAttributes();
			MYCHARAC.setNotes("Hallo Welt\nMät Ümlautön! ß\n\n< und Klammern >>.");
			MYCHARAC.setName("Unnamed Ümläut");
			serializer.write(MYCHARAC, out);
			System.out.println(out.toString());
			
			BufferedReader got = new BufferedReader(new StringReader(out.toString()));
			BufferedReader exp = new BufferedReader(new StringReader(DATA2));
			while (true) {
				String lineGot = got.readLine();
				String lineExp = exp.readLine();
				if (lineGot==null && lineExp!=null) {
					System.err.println("Different1:\nGot "+lineGot+"\nExp "+lineExp);
					System.exit(0);					
				}
				if (lineGot!=null && lineExp==null) {
					System.err.println("Different2:\nGot "+lineGot+"\nExp "+lineExp);
					System.exit(0);					
				}
				if (lineGot==null)
					break;
				if (!lineGot.equals(lineExp)) {
					System.err.println("Different3:\nGot "+lineGot+"\nExp "+lineExp);
					System.exit(0);
				}
			}
			System.out.println("Looks identical  "+DATA2.length()+" vs "+out.toString().length());
			
			assertEquals(DATA2, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void deserializeComment() {
		Space1889Character MYCHARAC = new Space1889Character();
		MYCHARAC.initPrimaryAttributes();
		MYCHARAC.setNotes("Hallo Welt\nMät Ümlautön! ß\n\n< und Klammern >>.");
		MYCHARAC.setName("Unnamed Ümläut");
		try {
			Space1889Character read = serializer.read(Space1889Character.class, DATA2);
			assertEquals("Hallo Welt\nMät Ümlautön! ß\n\n< und Klammern >>.", read.getNotes());
			assertEquals(MYCHARAC, read);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void deserializeComment2() {
		try {
			Space1889Character read = serializer.read(Space1889Character.class, DATA3);
			assertEquals("! Mit äöüß ?\nEinladen ja\n\nGeht auch speichern?\n", read.getNotes());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
