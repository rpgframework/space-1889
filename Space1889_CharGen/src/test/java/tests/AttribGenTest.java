package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.junit.Before;
import org.junit.Test;
import org.prelle.ubiquity.Attribute;
import org.prelle.ubiquity.GenerationTemplate;
import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.UbiquityCore;
import org.prelle.ubiquity.UbiquityRuleset;
import org.prelle.ubiquity.chargen.AttributeGenerator;
import org.prelle.ubiquity.chargen.event.GenerationEvent;
import org.prelle.ubiquity.chargen.event.GenerationEventDispatcher;
import org.prelle.ubiquity.chargen.event.GenerationEventListener;
import org.prelle.ubiquity.chargen.event.GenerationEventType;
import org.prelle.ubiquity.modifications.AttributeModification;

import de.rpgframework.ConfigContainer;
import de.rpgframework.ConfigOption;
import de.rpgframework.character.RulePlugin;
import de.rpgframework.character.RulePluginFeatures;
import de.rpgframework.character.RulePlugin.RulePluginProgessListener;
import de.rpgframework.core.CommandResult;
import de.rpgframework.core.CommandType;
import de.rpgframework.core.RoleplayingSystem;

public class AttribGenTest implements GenerationEventListener {

	class TestPlugin implements RulePlugin<UbiquityCharacter> {
		public boolean willProcessCommand(Object src, CommandType type, Object... values) {
			return false;
		}
		public CommandResult handleCommand(Object src, CommandType type, Object... values) {
			return null;
		}
		public String getID() {return "CORE";}
		public String getReadableName() {return "CORE";}
		public RoleplayingSystem getRules() {return RoleplayingSystem.SPACE1889;}
		public Collection<RulePluginFeatures> getSupportedFeatures() {
			return null;
		}
		public List<ConfigOption<?>> getConfiguration() {
			return null;
		}
		public void init(RulePluginProgessListener callback) {}
		public InputStream getAboutHTML() {return null;}
		public Collection<String> getRequiredPlugins() {return new ArrayList<>();}
		public void attachConfigurationTree(ConfigContainer addBelow) { }

		//-------------------------------------------------------------------
		@Override
		public List<String> getLanguages() {
			return Arrays.asList(Locale.ENGLISH.getLanguage());
		}
	}

	private static UbiquityRuleset ruleset;
	private static PropertyResourceBundle i18NResources;
	private static PropertyResourceBundle i18NHelpResources;
	private static RulePlugin<UbiquityCharacter> plugin = new AttribGenTest().new TestPlugin();
	
	//-------------------------------------------------------------------
	static {
		i18NResources = (PropertyResourceBundle) ResourceBundle.getBundle("i18n/space1889/core");
		i18NHelpResources = (PropertyResourceBundle) ResourceBundle.getBundle("i18n/space1889/core_help");

		ruleset = UbiquityCore.createRuleset(RoleplayingSystem.SPACE1889);
		
		UbiquityCore.loadTemplates(ruleset, plugin, ClassLoader.getSystemResourceAsStream("data/space1889/templates.xml"), i18NResources, i18NHelpResources);		
		UbiquityCore.loadSkills(ruleset, plugin, ClassLoader.getSystemResourceAsStream("data/space1889/skills.xml"), i18NResources, i18NHelpResources);
		UbiquityCore.loadTalents(ruleset, plugin, ClassLoader.getSystemResourceAsStream("data/space1889/talents.xml"), i18NResources, i18NHelpResources);
		
		ruleset.getTemplate("promising").setDefaultChoice(true);
	}
	
	private AttributeGenerator attrGen;
	private GenerationTemplate template;
	private UbiquityCharacter model;
	private List<GenerationEvent> events;

	//--------------------------------------------------------------------
	public AttribGenTest() {
		events   = new ArrayList<GenerationEvent>();
		GenerationEventDispatcher.addListener(this);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.ubiquity.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		events.add(event);
	}

	//--------------------------------------------------------------------
	private boolean eventWasDelivered(GenerationEventType type) {
		for (GenerationEvent ev : events) {
			if (ev.getType()==type)
				return true;
		}
		return false;
	}

	//--------------------------------------------------------------------
	private boolean wasAttributeChangeDelivered(Attribute attr) {
		for (GenerationEvent ev : events) {
			if (ev.getType()==GenerationEventType.ATTRIBUTE_CHANGED && ev.getKey()==attr)
				return true;
		}
		return false;
	}
	
	//--------------------------------------------------------------------
	@Before
	public void setUp() throws Exception {
		model = new UbiquityCharacter();
		template = ruleset.getDefaultTemplates();
		model.setRuleset(ruleset);
		attrGen = new AttributeGenerator(template, ruleset, model);
		events.clear();
	}

	//--------------------------------------------------------------------
	@Test
	public void testInitial() {
		int max = template.getAttributePoints();
		assertEquals(max-6, attrGen.getPointsLeft());
		// All attributes must exist
		for (Attribute key : Attribute.values()) {
			assertNotNull(key+" not in model",model.getAttribute(key));
		}
		assertEquals(1, model.getAttribute(Attribute.BODY).getValue());
	}

	//--------------------------------------------------------------------
	@Test
	public void testSimple() {
		int max = template.getAttributePoints();
		
		assertTrue(attrGen.canBeIncreased(Attribute.BODY));
		assertTrue(attrGen.increase(Attribute.BODY));
		assertEquals(2, model.getAttribute(Attribute.BODY).getValue());
		assertEquals(max-7, attrGen.getPointsLeft());
		assertTrue(eventWasDelivered(GenerationEventType.ATTRIBUTE_CHANGED));
		assertTrue(wasAttributeChangeDelivered(Attribute.BODY));
		
		// Now decrease again
		events.clear();
		assertTrue(attrGen.canBeDecreased(Attribute.BODY));
		assertTrue(attrGen.decrease(Attribute.BODY));
		assertEquals(1, model.getAttribute(Attribute.BODY).getValue());
		assertEquals(max-6, attrGen.getPointsLeft());
		assertTrue(eventWasDelivered(GenerationEventType.ATTRIBUTE_CHANGED));
		assertTrue(wasAttributeChangeDelivered(Attribute.BODY));
	}

	//--------------------------------------------------------------------
	@Test
	public void testSecondary() {
		int max = template.getAttributePoints();
		
		// BODY = 1
		// DEXTERITY = 2
		assertTrue(attrGen.increase(Attribute.DEXTERITY));
		// STRENGTH = 3
		assertTrue(attrGen.increase(Attribute.STRENGTH));
		assertTrue(attrGen.increase(Attribute.STRENGTH));
		// CHARISMA = 1
		// INTELLIGENCE = 2
		assertTrue(attrGen.increase(Attribute.INTELLIGENCE));
		// WILLPOWER = 3
		assertTrue(attrGen.increase(Attribute.WILLPOWER));
		assertTrue(attrGen.increase(Attribute.WILLPOWER));
		
		assertEquals(1, model.getAttribute(Attribute.BODY).getValue());
		assertEquals(2, model.getAttribute(Attribute.DEXTERITY).getValue());
		assertEquals(3, model.getAttribute(Attribute.STRENGTH).getValue());
		assertEquals(1, model.getAttribute(Attribute.CHARISMA).getValue());
		assertEquals(2, model.getAttribute(Attribute.INTELLIGENCE).getValue());
		assertEquals(3, model.getAttribute(Attribute.WILLPOWER).getValue());
		assertEquals(max-12, attrGen.getPointsLeft());
		assertTrue(eventWasDelivered(GenerationEventType.ATTRIBUTE_CHANGED));
		
		// Now check secondary attributes
		assertFalse(wasAttributeChangeDelivered(Attribute.SIZE));
		assertTrue(wasAttributeChangeDelivered(Attribute.MOVE));
		assertTrue(wasAttributeChangeDelivered(Attribute.PERCEPTION));
		assertTrue(wasAttributeChangeDelivered(Attribute.DEFENSE));
		assertFalse(wasAttributeChangeDelivered(Attribute.STUN)); // Body not raised
		assertTrue(wasAttributeChangeDelivered(Attribute.HEALTH));
		assertEquals(0, model.getAttribute(Attribute.SIZE).getValue());
		assertEquals(5, model.getAttribute(Attribute.MOVE).getValue());
		assertEquals(5, model.getAttribute(Attribute.PERCEPTION).getValue());
		assertEquals(3, model.getAttribute(Attribute.DEFENSE).getValue());
		assertEquals(1, model.getAttribute(Attribute.STUN).getValue());
		assertEquals(4, model.getAttribute(Attribute.HEALTH).getValue());
	}

	//--------------------------------------------------------------------
	@Test
	public void testModifications() {
		System.out.println("--------testModifications----------------");
		int max = template.getAttributePoints();
		
		assertEquals(2, model.getAttribute(Attribute.DEFENSE).getValue());
		assertEquals(2, model.getAttribute(Attribute.HEALTH).getValue());
		
		// Now increase size
		AttributeModification mod = new AttributeModification(Attribute.SIZE, 1);
		events.clear();
		attrGen.addModification(mod);
		assertEquals(max-6, attrGen.getPointsLeft());
		assertTrue(wasAttributeChangeDelivered(Attribute.SIZE));
		assertTrue(wasAttributeChangeDelivered(Attribute.DEFENSE));
		assertTrue(wasAttributeChangeDelivered(Attribute.HEALTH));
		assertEquals(1, model.getAttribute(Attribute.DEFENSE).getValue());
		assertEquals(3, model.getAttribute(Attribute.HEALTH).getValue());
		
		// Now decrease size again
		events.clear();
		attrGen.removeModification(mod);
		assertEquals(max-6, attrGen.getPointsLeft());
		assertTrue(wasAttributeChangeDelivered(Attribute.SIZE));
		assertTrue(wasAttributeChangeDelivered(Attribute.DEFENSE));
		assertTrue(wasAttributeChangeDelivered(Attribute.HEALTH));
		assertEquals(2, model.getAttribute(Attribute.DEFENSE).getValue());
		assertEquals(2, model.getAttribute(Attribute.HEALTH).getValue());
	}

}
