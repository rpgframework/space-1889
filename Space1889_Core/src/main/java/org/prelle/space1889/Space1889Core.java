/**
 *
 */
package org.prelle.space1889;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.simplepersist.SerializationException;
import org.prelle.ubiquity.Archetype;
import org.prelle.ubiquity.Flaw;
import org.prelle.ubiquity.Motivation;
import org.prelle.ubiquity.Resource;
import org.prelle.ubiquity.Skill;
import org.prelle.ubiquity.Talent;
import org.prelle.ubiquity.UbiquityCore;
import org.prelle.ubiquity.UbiquityRuleset;
import org.prelle.ubiquity.items.CarriedItem;

import de.rpgframework.character.RulePlugin;
import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class Space1889Core extends UbiquityCore {

	private static Logger logger = LogManager.getLogger("space1889");

	private static UbiquityRuleset ruleset;

	//-------------------------------------------------------------------
	public static void initialize(RulePlugin<Space1889Character> plugin) {
		ruleset = UbiquityCore.createRuleset(RoleplayingSystem.SPACE1889);
	}

	//-------------------------------------------------------------------
	public static UbiquityRuleset getRuleset() {
		return ruleset;
	}

	//-------------------------------------------------------------------
	public static List<Resource> getResources() {
		return ruleset.getResources();
	}

	//-------------------------------------------------------------------
	public static List<Skill> getSkills() {
		return ruleset.getSkills();
	}

	//-------------------------------------------------------------------
	public static List<Talent> getTalents() {
		return ruleset.getTalents();
	}

	//-------------------------------------------------------------------
	public static List<Archetype> getArchetypes() {
		return ruleset.getArchetypes();
	}

	//-------------------------------------------------------------------
	public static List<Motivation> getMotivations() {
		return ruleset.getMotivations();
	}

	//-------------------------------------------------------------------
	public static List<Flaw> getFlaws() {
		return ruleset.getFlaws();
	}

	//-------------------------------------------------------------------
	public static byte[] save(Space1889Character character) throws IOException {

		try {
			logger.trace("Save "+character.dump());

			// Write to System.out
			StringWriter out = new StringWriter();
			ruleset.getSerializer().write(character, out);
			return out.toString().getBytes(Charset.forName("UTF-8"));
		} catch (SerializationException e) {
			logger.error("Failed generating XML for char",e);
			throw new IOException(e);
		}
	}

	//-------------------------------------------------------------------
	public static Space1889Character load(byte[] raw) throws IOException {
		try {
			Space1889Character ret = ruleset.getSerializer().read(Space1889Character.class, new ByteArrayInputStream( raw ));
			ret.setRuleset(ruleset);
			logger.info("Character successfully loaded: "+ret);

			ruleset.getUtilities().applyAfterLoadModifications(ret);
//			loadEquipmentModifications(ret);
			return ret;
		} catch (Exception e) {
			logger.fatal("Failed loading character: "+e,e);
			throw e;
		}
	}

	//-------------------------------------------------------------------
	public static Space1889Character load(InputStream in) throws IOException {
		try {
			Space1889Character ret = ruleset.getSerializer().read(Space1889Character.class, in);
			ret.setRuleset(ruleset);
			logger.info("Character successfully loaded: "+ret);

			ruleset.getUtilities().applyAfterLoadModifications(ret);
//			loadEquipmentModifications(ret);
			return ret;
		} catch (Exception e) {
			logger.fatal("Failed loading character: "+e,e);
			throw e;
		}
	}

	//-------------------------------------------------------------------
	public static Space1889Character load(Path path) throws IOException {
		String buf = new String(Files.readAllBytes(path));
		try {
			Space1889Character ret = ruleset.getSerializer().read(Space1889Character.class, new StringReader( buf ));
			ret.setRuleset(ruleset);
			logger.info("Character successfully loaded: "+ret);

			ruleset.getUtilities().calculateSecondaryAttributes(ret);
			loadEquipmentModifications(ret);
			return ret;
		} catch (SerializationException e) {
			logger.fatal("Failed loading character: "+e,e);
			throw new IOException(e);
		}
	}


	//-------------------------------------------------------------------
	/**
	 * Prepare all character equipment for usage and eventually equip it
	 */
	private static void loadEquipmentModifications(Space1889Character model) {
		logger.debug("TODO Update modifications from "+model.getItems().size()+" equipment pieces");
		for (CarriedItem item : model.getItems()) {
			logger.debug("************"+item.getName()+"***************");

//			if (item.getLocation()!=ItemLocationType.BODY) {
//				EquipmentTools.applyEnhancements(model, item);
//			} else {
//				EquipmentTools.equip(model, item);
//			}
		}

//		EquipmentTools.updateTotalHandicap(model);
	}

}

