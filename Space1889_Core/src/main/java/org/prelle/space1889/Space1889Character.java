/**
 * 
 */
package org.prelle.space1889;

import org.prelle.simplepersist.Root;
import org.prelle.ubiquity.UbiquityCharacter;

/**
 * @author prelle
 *
 */
@Root(name="space1889char")
public class Space1889Character extends UbiquityCharacter {

	//-------------------------------------------------------------------
	/**
	 */
	public Space1889Character() {
	}

}
