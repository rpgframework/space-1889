/**
 * 
 */
package org.prelle.space1889;

import java.io.IOException;

import de.rpgframework.character.DecodeEncodeException;

/**
 * @author prelle
 *
 */
public class Space1889CharacterPlugin {

	//-------------------------------------------------------------------
	/**
	 */
	public Space1889CharacterPlugin() {
	}

	//-------------------------------------------------------------------
	public String getName(Space1889Character data) {
		return data.getName();
	}

	//-------------------------------------------------------------------
	public byte[] marshal(Space1889Character charac) throws DecodeEncodeException {
		try {
			return Space1889Core.save(charac);
		} catch (IOException e) {
			throw new DecodeEncodeException("IO-Error encoding: "+e, e.getCause());
		}
	}

	//-------------------------------------------------------------------
	public Space1889Character unmarshal(byte[] data) throws DecodeEncodeException {
		try {
			return Space1889Core.load(data);
		} catch (Exception e) {
			throw new DecodeEncodeException("IO-Error decoding: "+e, e);
		}
	}

}
