/**
 * @author Stefan Prelle
 *
 */
module space1889.core {
	exports org.prelle.space1889;

	provides de.rpgframework.character.RulePlugin with org.prelle.space1889.Space1889Rules;

//	requires java.xml;
	requires org.apache.logging.log4j;
	requires transitive de.rpgframework.core;
	requires transitive de.rpgframework.chars;
	requires simple.persist;
	requires transitive ubiquity.core;

}