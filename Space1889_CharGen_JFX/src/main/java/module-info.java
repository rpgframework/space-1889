/**
 * @author Stefan Prelle
 *
 */
module space1889.chargen.jfx {
	exports org.prelle.space1889.jfx.chargen;

	provides de.rpgframework.character.RulePlugin with org.prelle.space1889.jfx.chargen.Space1889BasePlugin;

	requires javafx.base;
	requires javafx.controls;
	requires javafx.extensions;
	requires javafx.graphics;
	requires org.apache.logging.log4j;
	requires transitive de.rpgframework.chars;
	requires transitive de.rpgframework.core;
	requires transitive de.rpgframework.javafx;
	requires transitive de.rpgframework.products;
	requires space1889.core;
	requires ubiquity.chargen;
	requires ubiquity.chargen.jfx;
	requires ubiquity.core;
	requires space1889.data;
}