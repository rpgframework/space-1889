/**
 * 
 */
package org.prelle.space1889.jfx.chargen;

import java.io.InputStream;

import org.prelle.javafx.Wizard;
import org.prelle.ubiquity.Archetype;
import org.prelle.ubiquity.chargen.CharacterGenerator;
import org.prelle.ubiquity.jfx.wizard.WizardPageArchetype;

import javafx.scene.image.Image;

/**
 * @author Stefan
 *
 */
public class Space1889PageArchetype extends WizardPageArchetype {
	
	private Image img;
	
	//-------------------------------------------------------------------
	public Space1889PageArchetype(Wizard wizard, CharacterGenerator charGen) {
		super(wizard, charGen);
		
		String fname = "data/space1889/archetype_civilservant.png";
		InputStream in = Space1889CharGenConstants.class.getResourceAsStream(fname);
		if (in!=null) {
			img = new Image(in);
			setImage(img);
		} else
			logger.warn("Missing image at "+fname);

	}

	//-------------------------------------------------------------------
	protected void update(Archetype value) {
		super.update(value);
		setImage(img);
		
	}
}

