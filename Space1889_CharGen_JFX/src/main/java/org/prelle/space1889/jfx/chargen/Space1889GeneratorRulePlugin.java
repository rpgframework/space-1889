/**
 *
 */
package org.prelle.space1889.jfx.chargen;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManager;
import org.prelle.space1889.Space1889Character;
import org.prelle.space1889.Space1889Core;
import org.prelle.ubiquity.charctrl.CharacterController;
import org.prelle.ubiquity.chargen.CharacterGenerator;
import org.prelle.ubiquity.jfx.ViewMode;
import org.prelle.ubiquity.levelling.CharacterLeveller;

import de.rpgframework.ConfigContainer;
import de.rpgframework.ConfigOption;
import de.rpgframework.character.CharacterHandle;
import de.rpgframework.character.RulePlugin;
import de.rpgframework.character.RulePluginFeatures;
import de.rpgframework.core.CommandBus;
import de.rpgframework.core.CommandBusListener;
import de.rpgframework.core.CommandResult;
import de.rpgframework.core.CommandType;
import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class Space1889GeneratorRulePlugin implements RulePlugin<Space1889Character>, CommandBusListener {

	private final static Logger logger = Space1889CharGenConstants.LOGGER;

	private static List<RulePluginFeatures> FEATURES = new ArrayList<RulePluginFeatures>();

	//-------------------------------------------------------------------
	static {
		FEATURES.add(RulePluginFeatures.CHARACTER_CREATION);
	}

	//-------------------------------------------------------------------
	/**
	 */
	public Space1889GeneratorRulePlugin() {
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getID()
	 */
	@Override
	public String getID() {
		return "CORE";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#getReadableName()
	 */
	@Override
	public String getReadableName() {
		if (this.getClass().getPackage().getImplementationTitle()!=null)
			return this.getClass().getPackage().getImplementationTitle();
		return "Space 1889 Character Generator";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getRules()
	 */
	@Override
	public RoleplayingSystem getRules() {
		return RoleplayingSystem.SPACE1889;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getRequiredPlugins()
	 */
	@Override
	public Collection<String> getRequiredPlugins() {
		return Arrays.asList("CORE");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getSupportedFeatures()
	 */
	@Override
	public Collection<RulePluginFeatures> getSupportedFeatures() {
		return FEATURES;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#attachConfigurationTree(de.rpgframework.ConfigContainer)
	 */
	@Override
	public void attachConfigurationTree(ConfigContainer addBelow) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getConfiguration()
	 */
	@Override
	public List<ConfigOption<?>> getConfiguration() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#willProcessCommand(java.lang.Object, de.rpgframework.core.CommandType, java.lang.Object[])
	 */
	@Override
	public boolean willProcessCommand(Object src, CommandType type, Object... values) {
		switch (type) {
		case SHOW_CHARACTER_MODIFICATION_GUI:
			logger.debug("Check "+Arrays.toString(values));
//			logger.debug(" 0: "+values[0].getClass());
//			logger.debug(" 1: "+values[1].getClass());
//			logger.debug(" 2: "+values[2].getClass());
//			logger.debug(" 3: "+values[3].getClass());
//			logger.debug(" 4: "+values[4].getClass());
			if (values[0]!=RoleplayingSystem.SPACE1889) return false;
			if (!(values[1] instanceof Space1889Character)) return false;
			if (!(values[2] instanceof CharacterHandle)) return false;
			if (!(values[4] instanceof ScreenManager)) return false;
			return true;
		case SHOW_CHARACTER_CREATION_GUI:
			if (values[0]!=RoleplayingSystem.SPACE1889) return false;
			if (!(values[2] instanceof ScreenManager)) return false;
			return true;
		default:
			return false;
		}

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#handleCommand(java.lang.Object, de.rpgframework.core.CommandType, java.lang.Object[])
	 */
	@Override
	public CommandResult handleCommand(Object src, CommandType type, Object... values) {
		if (!willProcessCommand(src, type, values))
			return new CommandResult(type, false, null, false);

		ScreenManager manager;
		CharacterController control;
		Space1889Character model;
		try {
			switch (type) {
			case SHOW_CHARACTER_MODIFICATION_GUI:
				logger.debug("start character modification");
				model = (Space1889Character)values[1];
				control = new CharacterLeveller(Space1889Core.getRuleset(), model);
				CharacterHandle handle = (CharacterHandle)values[2];
				manager = (ScreenManager)values[4];
				// Update image
				if (model.getImage()==null && handle.getImage()!=null)
					model.setImage(handle.getImage());

//				CharacterViewScreenSpace1889 screen = new CharacterViewScreenSpace1889(control);
//				screen.setData(model, handle);
//				manager.show(screen, "css/space1889.css");
				CharacterViewScreenSpace1889 screen = new CharacterViewScreenSpace1889(control, ViewMode.MODIFICATION, handle);
				manager.navigateTo(screen);

				return new CommandResult(type, true);
			case SHOW_CHARACTER_CREATION_GUI:
				logger.debug("start character creation");
				model = new Space1889Character();
				model.setRuleset(Space1889Core.getRuleset());
				control = new CharacterGenerator(Space1889Core.getRuleset(), model);
				manager = (ScreenManager)values[2];

//				screen = new CharacterViewScreenSpace1889(control);
//				manager.show(screen, "css/space1889.css");
				screen = new CharacterViewScreenSpace1889(control, ViewMode.GENERATION, null);
				manager.navigateTo(screen);
				screen.startGeneration(model);

				CommandResult result = new CommandResult(type, true);
				result.setReturnValue(model);
				return result;
			default:
				return new CommandResult(type, false);
			}
		} catch (Exception e) {
			logger.error("Failed processing "+type+" command",e);
			return new CommandResult(type, false, e.toString(), true);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#init()
	 */
	@Override
	public void init(RulePluginProgessListener callback) {
		CommandBus.registerBusCommandListener(this);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getAboutHTML()
	 */
	@Override
	public InputStream getAboutHTML() {
		return ClassLoader.getSystemResourceAsStream("org/prelle/space1889/jfx/chargen/i18n/space1889/chargenui.html");
	}

	//-------------------------------------------------------------------
	@Override
	public List<String> getLanguages() {
		return Arrays.asList(Locale.GERMAN.getLanguage(), Locale.ENGLISH.getLanguage());
	}

}
