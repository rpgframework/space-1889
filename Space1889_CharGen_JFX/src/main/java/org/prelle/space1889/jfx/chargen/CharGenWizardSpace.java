/**
 * 
 */
package org.prelle.space1889.jfx.chargen;

import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.Wizard;
import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.chargen.CharacterGenerator;

/**
 * @author prelle
 *
 */
public class CharGenWizardSpace extends Wizard {

	private CharacterGenerator charGen;
	private UbiquityCharacter generated;

	//-------------------------------------------------------------------
	/**
	 * @param nodes
	 */
	public CharGenWizardSpace(UbiquityCharacter model, CharacterGenerator charGen) {
		this.charGen = charGen;
		getPages().addAll(
				new Space1889PageTemplate(this, charGen),
				new Space1889PageArchetype(this, charGen),
				new Space1889PageMotivation(this, charGen),
				new Space1889PageAttributes(this, charGen),
				new Space1889PageSkills(this, charGen),
				new Space1889PageTalents(this, charGen),
				new Space1889PageResources(this, charGen),
				new Space1889PageFlaws(this, charGen),
				new Space1889PageName(this, charGen)
				);
		this.charGen = charGen;

		this.setConfirmCancelCallback( wizardParam -> {
			CloseType answer = getScreenManager().showAlertAndCall(
					AlertType.CONFIRMATION,
					Space1889CharGenConstants.UI.getString("wizard.cancelconfirm.header"),
					Space1889CharGenConstants.UI.getString("wizard.cancelconfirm.content"));
			return answer==CloseType.OK || answer==CloseType.YES;
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.Wizard#canBeFinished()
	 */
	@Override
	public boolean canBeFinished() {
		Space1889CharGenConstants.LOGGER.info("canBeFinished="+charGen.canBeFinished());
		return charGen.canBeFinished();
	}

	//-------------------------------------------------------------------
	public UbiquityCharacter getGenerated() {
		return generated;
	}

}
