package org.prelle.space1889.jfx.chargen.pages;

import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.ubiquity.charctrl.CharacterController;
import org.prelle.ubiquity.jfx.ViewMode;
import org.prelle.ubiquity.jfx.pages.UbiquityOverviewPage;

import de.rpgframework.character.CharacterHandle;

/**
 * @author Stefan Prelle
 *
 */
public class Space1889OverviewPage extends UbiquityOverviewPage {

	//-------------------------------------------------------------------
	public Space1889OverviewPage(CharacterController control, ViewMode mode, CharacterHandle handle,
			ScreenManagerProvider provider) {
		super(control, mode, handle, provider);
		// TODO Auto-generated constructor stub
	}

}
