/**
 * 
 */
package org.prelle.space1889.jfx.chargen;

import java.io.InputStream;

import org.prelle.javafx.Wizard;
import org.prelle.ubiquity.chargen.CharacterGenerator;
import org.prelle.ubiquity.jfx.wizard.WizardPageAttributes;

import javafx.scene.image.Image;

/**
 * @author Stefan
 *
 */
public class Space1889PageAttributes extends WizardPageAttributes {
	
	//-------------------------------------------------------------------
	public Space1889PageAttributes(Wizard wizard, CharacterGenerator charGen) {
		super(wizard, charGen);
		
		String fname = "data/space1889/archetype_soldier.png";
		InputStream in = Space1889CharGenConstants.class.getResourceAsStream(fname);
		if (in!=null) {
			setImage(new Image(in));
		} else
			logger.warn("Missing image at "+fname);

	}

}

