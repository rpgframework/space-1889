package org.prelle.space1889.jfx.chargen;

import org.prelle.space1889.Space1889Character;
import org.prelle.space1889.Space1889Core;
import org.prelle.ubiquity.Attribute;
import org.prelle.ubiquity.DummyRulePlugin;
import org.prelle.ubiquity.charctrl.CharacterController;
import org.prelle.ubiquity.chargen.CharacterGenerator;
import org.prelle.ubiquity.jfx.skills.SkillTreePane;

import de.rpgframework.core.RoleplayingSystem;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class MySpaceStarter extends Application {

	//-------------------------------------------------------------------
	public MySpaceStarter() {
		Space1889Core.initialize(new DummyRulePlugin<>(RoleplayingSystem.SPACE1889, "CORE"));
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unused")
	@Override
	public void start(Stage stage) throws Exception {
		Space1889Character model = new Space1889Character();
		model.setRuleset(Space1889Core.getRuleset());
		model.setName("Manfred Muster");
		model.getAttribute(Attribute.BODY).setPoints(2);
		CharacterController charGen = null;

		Parent dia = null;
		int variant = 2;
		switch (variant) {
		case 1:
			charGen = new CharacterGenerator(Space1889Core.getRuleset(), model);
			CharGenWizardSpace wizard = new CharGenWizardSpace(model,((CharacterGenerator)charGen));
			break;
		case 2:
			charGen = new CharacterGenerator(Space1889Core.getRuleset(), model);
			dia = new SkillTreePane(charGen.getSkillController(), charGen.getTalentController(), model);
			((SkillTreePane)dia).setData(model);
			break;
		default:
			System.exit(0);
		}

		Scene scene = new Scene(dia);
		scene.getStylesheets().add("css/space1889.css");
		stage.setScene(scene);
		stage.show();

	}

	public static void main(String[] args) {
		launch(args);
	}

}
