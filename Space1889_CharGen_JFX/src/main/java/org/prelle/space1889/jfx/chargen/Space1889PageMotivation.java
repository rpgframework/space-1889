/**
 * 
 */
package org.prelle.space1889.jfx.chargen;

import java.io.InputStream;

import org.prelle.javafx.Wizard;
import org.prelle.ubiquity.chargen.CharacterGenerator;
import org.prelle.ubiquity.jfx.wizard.WizardPageMotivation;

import javafx.scene.image.Image;

/**
 * @author Stefan
 *
 */
public class Space1889PageMotivation extends WizardPageMotivation {
	
	//-------------------------------------------------------------------
	public Space1889PageMotivation(Wizard wizard, CharacterGenerator charGen) {
		super(wizard, charGen);
		
		String fname = "data/space1889/archetype_criminal.png";
		InputStream in = Space1889CharGenConstants.class.getResourceAsStream(fname);
		if (in!=null) {
			setImage(new Image(in));
		} else
			logger.warn("Missing image at "+fname+" / "+Space1889CharGenConstants.class.getResource(fname));

	}

}

