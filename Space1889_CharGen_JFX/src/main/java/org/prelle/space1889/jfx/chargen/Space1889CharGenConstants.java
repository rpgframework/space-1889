/**
 *
 */
package org.prelle.space1889.jfx.chargen;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Stefan Prelle
 *
 */
public interface Space1889CharGenConstants {

	@SuppressWarnings("exports")
	public final static Logger LOGGER = LogManager.getLogger("space1889.jfx");

	public final static String PACKAGE = "org/prelle/space1889/jfx/chargen";
	public final static String CSS = "css/space1889.css";

	public static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(PACKAGE+"/i18n/chargenui");

}
