package org.prelle.space1889.jfx.chargen.pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.ubiquity.HistoryElementImpl;
import org.prelle.ubiquity.RewardImpl;
import org.prelle.ubiquity.UbiquityUtils;
import org.prelle.ubiquity.charctrl.CharacterController;
import org.prelle.ubiquity.chargen.event.GenerationEvent;
import org.prelle.ubiquity.chargen.event.GenerationEventDispatcher;
import org.prelle.ubiquity.chargen.event.GenerationEventType;
import org.prelle.ubiquity.jfx.develop.RewardBox;
import org.prelle.ubiquity.jfx.pages.UbiquityDevelopmentPage;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.HistoryElement;
import de.rpgframework.genericrpg.Reward;
import de.rpgframework.products.ProductServiceLoader;

/**
 * @author Stefan Prelle
 *
 */
public class Space1889DevelopmentPage extends UbiquityDevelopmentPage {

	protected static Logger logger = LogManager.getLogger("space1889.jfx");

	//-------------------------------------------------------------------
	public Space1889DevelopmentPage(CharacterController control, CharacterHandle handle,
			ScreenManagerProvider provider) {
		super(control, RoleplayingSystem.SPACE1889, provider);
		this.setId("space1889-development");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.DevelopmentScreen#openAdd()
	 */
	@Override
	public HistoryElement openAdd() {
		RewardBox content = new RewardBox(control.getRuleset());
		logger.warn("TODO: openAdd");
		
		ManagedDialog dialog = new ManagedDialog(UI.getString("dialog.reward.title"), content, CloseType.OK, CloseType.CANCEL);
		CloseType closed = (CloseType)provider.getScreenManager().showAndWait(dialog);
		
		if (closed==CloseType.OK) {
			RewardImpl reward = content.getDataAsReward();
			logger.debug("Add reward "+reward);
			UbiquityUtils.reward(model, reward);
			HistoryElementImpl elem = new HistoryElementImpl();
			elem.setName(reward.getTitle());
			elem.addGained(reward);
			if (reward.getId()!=null)
				elem.setAdventure(ProductServiceLoader.getInstance().getAdventure(RoleplayingSystem.SPACE1889, reward.getId()));
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, new int[]{model.getExperienceFree(), model.getExperienceInvested()}));
			return elem;
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.DevelopmentScreen#openEdit(de.rpgframework.genericrpg.HistoryElement)
	 */
	@Override
	public boolean openEdit(HistoryElement elem) {
		logger.warn("TODO: openEdit");
		/*
		 * Currently only elements
		 */
		if (elem.getGained().size()>1) {
			getScreenManager().showAlertAndCall(
					AlertType.ERROR, 
					UI.getString("error.not-possible"), 
					UI.getString("error.only-single-rewards-editable"));
			return false;
		}
		
		Reward toEdit = elem.getGained().get(0);
		RewardBox dialog = new RewardBox(control.getRuleset(), toEdit);
		
//		ManagedScreen screen = new ManagedScreen() {
//			@Override
//			public boolean close(CloseType closeType) {
//				logger.debug("close("+closeType+") overwritten  "+dialog.hasEnoughData());
//				if (closeType==CloseType.OK) {
//					return dialog.hasEnoughData();
//				}
//				return true;
//			}
//		};
//		ManagedScreenDialogSkin skin = new ManagedScreenDialogSkin(screen);
//		screen.setTitle(UI.getString("dialog.reward.title"));
//		screen.setContent(dialog);
//		screen.getNavigButtons().addAll(CloseType.OK, CloseType.CANCEL);
//		screen.setSkin(skin);
//		screen.setOnAction(CloseType.OK, event -> manager.close(screen, CloseType.OK));
//		screen.setOnAction(CloseType.CANCEL, event -> manager.close(screen, CloseType.CANCEL));
//		CloseType closed = (CloseType)manager.showAndWait(screen);
//		
//		if (closed==CloseType.OK) {
//			RewardImpl reward = dialog.getDataAsReward();
//			logger.debug("Copy edited data: ORIG = "+toEdit);
//			toEdit.setTitle(reward.getTitle());
//			toEdit.setId(reward.getId());
//			toEdit.setDate(reward.getDate());
//			// Was single reward. Changes in reward title, change element title
//			((HistoryElementImpl)elem).setName(reward.getTitle());
//			ProductService sessServ = RPGFrameworkLoader.getInstance().getProductService();
//			if (reward.getId()!=null)  {
//				Adventure adv = sessServ.getAdventure(RoleplayingSystem.SPLITTERMOND, reward.getId());
//				if (adv==null) {
//					logger.warn("Reference to an unknown adventure: "+reward.getId());
//				} else
//					((HistoryElementImpl)elem).setAdventure(adv);
//
//			}
////			((HistoryElementImpl)elem).set(reward.getTitle());
//			logger.debug("Copy edited data: NEW  = "+toEdit);
//			logger.debug("Element now   = "+elem);
//			return true;
//		}
		return false;
	}

}
