/**
 * @author Stefan Prelle
 *
 */
module ubiquity.core {
	exports org.prelle.ubiquity;
	exports org.prelle.ubiquity.items;
	exports org.prelle.ubiquity.requirements;
	exports org.prelle.ubiquity.persist;
	exports org.prelle.ubiquity.modifications;

	opens org.prelle.ubiquity;
	opens org.prelle.ubiquity.modifications to simple.persist;
	opens org.prelle.ubiquity.requirements to simple.persist;
	opens org.prelle.ubiquity.items to simple.persist;

	requires java.xml;
	requires org.apache.logging.log4j;
	requires transitive de.rpgframework.core;
	requires simple.persist;
	requires transitive de.rpgframework.chars;
	requires transitive de.rpgframework.products;
}