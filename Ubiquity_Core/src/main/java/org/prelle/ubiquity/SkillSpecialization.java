/**
 * 
 */
package org.prelle.ubiquity;

/**
 * @author prelle
 *
 */
public class SkillSpecialization {

	private transient Skill skill;
	@org.prelle.simplepersist.Attribute
	private String id;
	
	//-------------------------------------------------------------------
	public SkillSpecialization() {
	}

	//-------------------------------------------------------------------
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the name
	 */
	public String getName() {
		try {
			return skill.getResourceBundle().getString("skill."+skill.getId()+"."+id);
		} catch (Exception e) {
			System.err.println(e.toString());
			return "skill."+skill.getId()+"."+id;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @return the skill
	 */
	public Skill getSkill() {
		return skill;
	}

	//-------------------------------------------------------------------
	/**
	 * @param skill the skill to set
	 */
	public void setSkill(Skill skill) {
		this.skill = skill;
	}

}
