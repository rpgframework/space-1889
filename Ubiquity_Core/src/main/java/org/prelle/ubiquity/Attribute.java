/**
 *
 */
package org.prelle.ubiquity;

/**
 * @author prelle
 *
 */
public enum Attribute {

	BODY,
	DEXTERITY,
	STRENGTH,
	CHARISMA,
	INTELLIGENCE,
	WILLPOWER,

	SIZE,
	MOVE,
	PERCEPTION,
	INITIATIVE,
	DEFENSE,
	STUN,
	HEALTH,
	;

	//-------------------------------------------------------------------
	public String getShortName() {
		return UbiquityCore.getI18nResources().getString("attribute."+this.name().toLowerCase()+".short");
	}

    //-------------------------------------------------------------------
    public String getName() {
        return UbiquityCore.getI18nResources().getString("attribute."+this.name().toLowerCase());
    }

    //-------------------------------------------------------------------
    public String getHelpText() {
        return UbiquityCore.getI18nResources().getString("attribute."+this.name().toLowerCase()+".desc");
    }

	//-------------------------------------------------------------------
	public static Attribute[] primaryValues() {
		return new Attribute[]{BODY,DEXTERITY,STRENGTH,CHARISMA,INTELLIGENCE,WILLPOWER};
	}

	//-------------------------------------------------------------------
	public static Attribute[] secondaryValues() {
		return new Attribute[]{SIZE,MOVE,PERCEPTION,INITIATIVE,DEFENSE,STUN,HEALTH};
	}

	//-------------------------------------------------------------------
	public boolean isPrimary() {
		for (Attribute key : primaryValues())
			if (this==key) return true;
		return false;
	}

}
