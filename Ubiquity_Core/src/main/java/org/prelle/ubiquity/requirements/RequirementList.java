/**
 * 
 */
package org.prelle.ubiquity.requirements;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="requires")
@ElementListUnion({
    @ElementList(entry="attrreq", type=AttributeRequirement.class),
    @ElementList(entry="racereq", type=RaceRequirement.class),
    @ElementList(entry="skillreq", type=SkillRequirement.class),
    @ElementList(entry="talentreq", type=TalentRequirement.class),
 })
public class RequirementList extends ArrayList<Requirement> {

	private static final long serialVersionUID = 1L;

	//-------------------------------------------------------------------
	/**
	 */
	public RequirementList() {
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public RequirementList(Collection<? extends Requirement> c) {
		super(c);
	}

}
