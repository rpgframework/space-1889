/**
 * 
 */
package org.prelle.ubiquity.requirements;

import org.prelle.ubiquity.Attribute;

/**
 * @author prelle
 *
 */
public class AttributeRequirement extends Requirement {

	@org.prelle.simplepersist.Attribute
    private Attribute attr;
	@org.prelle.simplepersist.Attribute
    private int val;

    //-------------------------------------------------------------------
	public AttributeRequirement() {
	}

    //-------------------------------------------------------------------
	public AttributeRequirement(Attribute attr, int value) {
		this.attr = attr;
		this.val = value;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the attr
	 */
	public Attribute getAttr() {
		return attr;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the val
	 */
	public int getVal() {
		return val;
	}

	//-------------------------------------------------------------------
	@Override
	public String toString() {
		if (attr!=null)
			return attr.getName()+" "+val;
		return "UNKNOWN_ATTR "+val;
	}

}
