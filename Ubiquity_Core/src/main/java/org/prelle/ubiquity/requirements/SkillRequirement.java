package org.prelle.ubiquity.requirements;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.ubiquity.Skill;
import org.prelle.ubiquity.persist.SkillConverter;

/**
 * @author prelle
 *
 */
public class SkillRequirement extends Requirement {

	@Attribute(name="idref")
	@AttribConvert(SkillConverter.class)
    private Skill skill;
	@Attribute(required=false)
    private int val;
    
    //-----------------------------------------------------------------------
    public SkillRequirement() {
    	val = 1;
    }
    
    //-----------------------------------------------------------------------
    public SkillRequirement(Skill skill, int val) {
        this.skill = skill;
        this.val  = val;
    }
    
    //-----------------------------------------------------------------------
    public String toString() {
        if (skill==null)
        	return "SKILL_NOT_SET";
        return skill.getName()+" = "+val;
    }
    
    //-----------------------------------------------------------------------
    public int getValue() {
        return val;
    }
    
    //-----------------------------------------------------------------------
    public void setValue(int val) {
        this.val = val;
    }
    
    //-----------------------------------------------------------------------
    public Object clone() {
        return new SkillRequirement(skill, val);
    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof SkillRequirement) {
            SkillRequirement other = (SkillRequirement)o;
            if (other.getSkill()!=skill) return false;
            return (other.getValue()==val);
        } else
            return false;
    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean matches(Object o) {
        if (o instanceof SkillRequirement) {
            SkillRequirement other = (SkillRequirement)o;
            if (other.getSkill()!=skill) return false;
            return true;
        } else
            return false;
    }

	//-------------------------------------------------------------------
	/**
	 * @return the skill
	 */
	public Skill getSkill() {
		return skill;
	}

	//-------------------------------------------------------------------
	/**
	 * @param skill the skill to set
	 */
	public void setSkill(Skill skill) {
		this.skill = skill;
	}
    
}// AttributeModification
