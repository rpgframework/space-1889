package org.prelle.ubiquity.requirements;

import org.prelle.simplepersist.Attribute;

/**
 * @author prelle
 *
 */
public class RaceRequirement extends Requirement {

	@Attribute(name="idref")
//	@AttribConvert(TalentConverter.class)
    private String race;
    
    //-----------------------------------------------------------------------
    public RaceRequirement() {
    }
    
    //-----------------------------------------------------------------------
    public RaceRequirement(String race) {
        this.race = race;
    }
    
    //-----------------------------------------------------------------------
    public String toString() {
        if (race==null)
        	return "RACE_NOT_SET";
        return race;
    }
    
    //-----------------------------------------------------------------------
    public Object clone() {
        return new RaceRequirement(race);
    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof RaceRequirement) {
            RaceRequirement other = (RaceRequirement)o;
            return other.getRace().equals(race);
         } else
            return false;
    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean matches(Object o) {
        if (o instanceof RaceRequirement) {
            RaceRequirement other = (RaceRequirement)o;
            return other.getRace().equals(race);
        } else
            return false;
    }

	//-------------------------------------------------------------------
	public String getRace() {
		return race;
	}

    
}