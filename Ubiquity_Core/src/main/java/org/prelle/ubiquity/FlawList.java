/**
 * 
 */
package org.prelle.ubiquity;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="flaws")
@ElementList(entry="flaw",type=Flaw.class,inline=true)
public class FlawList extends ArrayList<Flaw> {

}
