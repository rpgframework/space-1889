/**
 * 
 */
package org.prelle.ubiquity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;
import org.prelle.ubiquity.Resource.Type;
import org.prelle.ubiquity.items.CarriedItem;
import org.prelle.ubiquity.modifications.AttributeModification;
import org.prelle.ubiquity.modifications.ModificationImpl;
import org.prelle.ubiquity.modifications.ModificationList;
import org.prelle.ubiquity.modifications.ResourceModification;
import org.prelle.ubiquity.persist.ArchetypeConverter;
import org.prelle.ubiquity.persist.MotivationConverter;
import org.prelle.ubiquity.requirements.AttributeRequirement;
import org.prelle.ubiquity.requirements.Requirement;
import org.prelle.ubiquity.requirements.SkillRequirement;
import org.prelle.ubiquity.requirements.TalentRequirement;

import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class UbiquityCharacter implements RuleSpecificCharacterObject {
	
	private final static Logger logger = LogManager.getLogger("ubiquity");
	private static PropertyResourceBundle res = UbiquityCore.getI18nResources();

	public enum Gender {
		MALE,
		FEMALE
		;
		public String toString() {
			return res.getString("gender."+name().toLowerCase());
		}
	}

	private UbiquityRuleset ruleset;
	
	@Element
	private String name;
	@Element
	private String hairColor, eyeColor, skinColor;
	@Element
	private int size;
	@Element
	private int weight;
	@Element
	private Gender gender;
	@Element
	private int age;
	@Element(required=false)
	private byte[] image;
	@org.prelle.simplepersist.Attribute(name="expfree")
	private int expFree;
	@org.prelle.simplepersist.Attribute(name="expinv")
	private int expInv;
	
	@org.prelle.simplepersist.Attribute(name="atype")
	@AttribConvert(ArchetypeConverter.class)
	protected Archetype archetype;

	@org.prelle.simplepersist.Attribute(name="motive")
	@AttribConvert(MotivationConverter.class)
	protected Motivation motivation;
	@ElementList(entry="attribute",type=AttributeValue.class)
	protected Attributes attributes;
	@ElementList(entry="skillval",type=SkillValue.class)
	protected List<SkillValue> skillvals;
	protected transient List<SkillValue> tmpSkillVals;
	@ElementList(entry="talentval",type=TalentValue.class)
	protected List<TalentValue> talentvals;
	@ElementList(entry="resourceval",type=ResourceValue.class)
	protected List<ResourceValue> resourcevals;
	@ElementList(entry="flawval",type=FlawValue.class)
	protected List<FlawValue> flawvals;
	@ElementList(entry="itemref",type=CarriedItem.class)
	protected List<CarriedItem> carried;
	@Element
	protected String notes;
	@Element
	private ModificationList history;
	@ElementList(entry="reward",type=RewardImpl.class)
	private List<RewardImpl> rewards;
	@Element
	private String race;


	//-------------------------------------------------------------------
	/**
	 */
	public UbiquityCharacter() {
		attributes = new Attributes();
		skillvals  = new ArrayList<SkillValue>();
		talentvals = new ArrayList<TalentValue>();
		resourcevals = new ArrayList<ResourceValue>();
		flawvals   = new ArrayList<FlawValue>();
		carried    = new ArrayList<CarriedItem>();
		history    = new ModificationList();
		rewards    = new ArrayList<RewardImpl>();
		
		tmpSkillVals = new ArrayList<SkillValue>();
	}

	//-------------------------------------------------------------------
	public boolean equals(Object o) {
		if (o instanceof UbiquityCharacter) {
			UbiquityCharacter other = (UbiquityCharacter)o;
			if (!name.equals(other.getName())) return false;
			return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	@Override
	public String toString() {
		return "UbiquityChar "+name;
	}

	//-------------------------------------------------------------------
	public String dump() {
		StringBuffer buf = new StringBuffer();
		buf.append("\nName      : "+name);
		buf.append("\nImage     : "+((image!=null)?"yes":"no"));
		buf.append("\nArchetype : "+archetype);
		buf.append("\nMotivation: "+motivation);
		buf.append("\nAttributes: "+attributes.dump());
		buf.append("\nSkills    : ");
		for (SkillValue tmp : skillvals) {
			buf.append("\n  "+tmp.getModifyable().getName());
			if (tmp.getSpecializations()!=null)
				buf.append("/"+tmp.getSpecializations());
			buf.append("\t  "+tmp.getPoints());
		}
		buf.append("\nTalents   : ");
		for (TalentValue tmp : talentvals) {
			buf.append("\n  "+tmp.getTalent().getName());
			if (tmp.getLevel()>1)
				buf.append(" "+tmp.getLevel());
		}
		buf.append("\nResources   : ");
		for (ResourceValue tmp : resourcevals) {
			buf.append("\n  "+tmp.getModifyable().getName());
			buf.append(" "+tmp.getPoints());
		}
		buf.append("\nFlaws   : ");
		for (FlawValue tmp : flawvals) {
			buf.append("\n  "+tmp.getFlaw().getName());
		}
		buf.append("\nCarries : ");
		for (CarriedItem tmp : carried) {
			if (tmp.getItem()!=null)
				buf.append("\n  "+tmp.getItem().getType()+" "+tmp.getItem().getName());
		}
		
		return buf.toString();
	}

	//-------------------------------------------------------------------
	public void initPrimaryAttributes() {
		for (Attribute attr : Attribute.primaryValues()) {
			AttributeValue toAdd = new AttributeValue(attr, 0);
			attributes.add(toAdd);
		}
		
	}

	//-------------------------------------------------------------------
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	//-------------------------------------------------------------------
	public AttributeValue getAttribute(Attribute key) {
		return attributes.get(key);
	}

	//-------------------------------------------------------------------
	public void setSkill(SkillValue skillVal) {
		if (!skillvals.contains(skillVal))
			skillvals.add(skillVal);
	}

	//-------------------------------------------------------------------
	public SkillValue getSkill(String key) {
		return getSkill(ruleset.getSkill(key));
	}

	//-------------------------------------------------------------------
	public SkillValue getSkill(Skill key) {
		for (SkillValue val : skillvals)
			if (val.getModifyable()==key)
				return val;
		
		// To prevent giving new SkillValues for unused skills everytime
		// there are accessed, cache them
		for (SkillValue val : tmpSkillVals) {
			if (val.getModifyable()==key)
				return val;
		}
		
		// Calculate dynamically
		SkillValue ret = new SkillValue(key, 0);
		tmpSkillVals.add(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public int getSkillDicePool(Skill key) {
		int attrVal = getAttribute(key.getAttribute()).getValue();
		
		for (SkillValue val : skillvals)
			if (val.getModifyable()==key)
				return val.getModifiedValue() + attrVal;
		
		// Calculate dynamically
		// Is the char Universalist?		
		TalentValue jackOfAll = getTalent("jackofall");
		if (jackOfAll!=null) {
			if (key.isPartOfGroup()) {
				return attrVal + (-3 + jackOfAll.getLevel()) ;
			} else {
				return attrVal + (-1 + jackOfAll.getLevel()) ;
			}
		} else {
//			if (key.isPartOfGroup()) {
//				return 0;
//			} else {
				return Math.max(0,attrVal-2);
//			}
		}
	}

	//-------------------------------------------------------------------
	public void addSkill(SkillValue val) {
		if (skillvals.contains(val))
			return;

		skillvals.add(val);
		tmpSkillVals.remove(val);
	}

	//-------------------------------------------------------------------
	public void removeSkill(SkillValue val) {
		if (!skillvals.contains(val))
			return;
		
		skillvals.remove(val);
		tmpSkillVals.add(val);
	}

	//-------------------------------------------------------------------
	public List<SkillValue> getSkills() {
		Collections.sort(skillvals);
		return skillvals;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the archetype
	 */
	public Archetype getArchetype() {
		return archetype;
	}

	//-------------------------------------------------------------------
	/**
	 * @param archetype the archetype to set
	 */
	public void setArchetype(Archetype archetype) {
		this.archetype = archetype;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the motivation
	 */
	public Motivation getMotivation() {
		return motivation;
	}

	//-------------------------------------------------------------------
	/**
	 * @param motivation the motivation to set
	 */
	public void setMotivation(Motivation motivation) {
		this.motivation = motivation;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the image
	 */
	public byte[] getImage() {
		return image;
	}

	//-------------------------------------------------------------------
	/**
	 * @param image the image to set
	 */
	public void setImage(byte[] image) {
		this.image = image;
	}

	//-------------------------------------------------------------------
	public TalentValue getTalent(String id) {
		for (TalentValue talent : getTalents()) {
			if (talent.getTalent().getId().equals(id))
				return talent;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public TalentValue getTalent(Talent key) {
		for (TalentValue talent : getTalents()) {
			if (talent.getTalent()==key)
				return talent;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public List<TalentValue> getTalents() {
		Collections.sort(talentvals);
		return talentvals;
	}

	//-------------------------------------------------------------------
	public void addTalent(TalentValue val) {
		if (talentvals.contains(val))
			return;

		talentvals.add(val);
	}

	//-------------------------------------------------------------------
	public void removeTalent(TalentValue val) {
		if (!talentvals.contains(val))
			return;
		
		talentvals.remove(val);
	}

	//--------------------------------------------------------------------
	public boolean meetsRequirement(Requirement req) {
		if (req instanceof AttributeRequirement) {
			AttributeRequirement aReq = (AttributeRequirement)req;
			return getAttribute(aReq.getAttr()).getValue()>=aReq.getVal();
		}
		if (req instanceof SkillRequirement) {
			SkillRequirement sReq = (SkillRequirement)req;
			if (getSkill(sReq.getSkill())==null)
				// Skill not present at all
				return false;
			return getSkill(sReq.getSkill()).getPoints()>=sReq.getValue();
		}
		if (req instanceof TalentRequirement) {
			TalentRequirement tReq = (TalentRequirement)req;
			if (getTalent(tReq.getTalent())==null)
				// Talent not present at all
				return false;
			if (tReq.getTalent().getLevel()<=1)
				return true;
			return getTalent(tReq.getTalent()).getLevel()>=tReq.getValue();
		}
		LogManager.getLogger("ubiquity").warn("TODO: meetsRequirement for " + req.getClass());
		return false;
	}

	//-------------------------------------------------------------------
	public List<ResourceValue> getResources() {
		Collections.sort(resourcevals);
		return resourcevals;
	}

	//-------------------------------------------------------------------
	public void addResource(ResourceValue val) {
		if (resourcevals.contains(val))
			return;

		resourcevals.add(val);
	}

	//-------------------------------------------------------------------
	public void removeResource(ResourceValue val) {
		if (!resourcevals.contains(val))
			return;
		
		resourcevals.remove(val);
	}

	//-------------------------------------------------------------------
	public List<FlawValue> getFlaws() {
		Collections.sort(flawvals);
		return flawvals;
	}

	//-------------------------------------------------------------------
	public void addFlaw(FlawValue val) {
		if (flawvals.contains(val))
			return;

		flawvals.add(val);
	}

	//-------------------------------------------------------------------
	public void removeFlaw(FlawValue val) {
		if (!flawvals.contains(val))
			return;
		
		flawvals.remove(val);
	}

	//-------------------------------------------------------------------
	public List<CarriedItem> getItems() {
		ArrayList<CarriedItem> ret = new ArrayList<>(carried);
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public void addItem(CarriedItem val) {
		if (carried.contains(val))
			return;

		carried.add(val);
	}

	//-------------------------------------------------------------------
	public void removeItem(CarriedItem val) {
		if (!carried.contains(val))
			return;
		
		carried.remove(val);
	}

	//--------------------------------------------------------------------
	public int getExperienceFree() {
		return expFree;
	}

	//--------------------------------------------------------------------
	public void setExperienceFree(int expFree) {
		this.expFree = expFree;
	}

	//--------------------------------------------------------------------
	public int getExperienceInvested() {
		return expInv;
	}

	//--------------------------------------------------------------------
	public void setExperienceInvested(int expInv) {
		this.expInv = expInv;
	}

	//--------------------------------------------------------------------
	public void addToHistory(Modification mod) {
		history.add(mod);
	}

	//--------------------------------------------------------------------
	public List<Modification> getHistory() {
		return new ArrayList<Modification>(history);
	}

	//--------------------------------------------------------------------
	public void addReward(RewardImpl rew) {
		rewards.add(rew);
		
		setExperienceFree(getExperienceFree() + rew.getExperiencePoints());
		for (Modification mod : rew.getModifications()) {
			if (mod instanceof ResourceModification) {
				Resource res = ((ResourceModification)mod).getResource();
				int      val = ((ResourceModification)mod).getValue();
				String comment = ((ResourceModification)mod).getComment();
				ResourceValue ref = new ResourceValue(res, val);
				// Base resources are not added but increased
				if (res.getType()==Type.BASE) {
					for (ResourceValue tmp : resourcevals) {
						if (tmp.getModifyable()==res) {
							ref = tmp;
							ref.setPoints(ref.getPoints()+val);
							ref.setComment(comment);
							break;
						}
					}
				} else {
					ref.setComment(comment);
					addResource(ref);
				}
					
				// Add to history
				addToHistory(mod);
			} else if (mod instanceof AttributeModification) {
				Attribute attr = ((AttributeModification)mod).getAttribute();
				AttributeValue aVal = getAttribute(attr);
				aVal.addModification((AttributeModification) mod);
				
			// Add to history
			addToHistory(mod);
			} else {
				logger.error("Unsupported modification: "+mod.getClass());
			}
		}
	}

	//--------------------------------------------------------------------
	public List<RewardImpl> getRewards() {
		return new ArrayList<RewardImpl>(rewards);
	}

	public String getHairColor() {
		return hairColor;
	}

	public void setHairColor(String hairColor) {
		this.hairColor = hairColor;
	}

	public String getEyeColor() {
		return eyeColor;
	}

	public void setEyeColor(String eyeColor) {
		this.eyeColor = eyeColor;
	}

	public String getSkinColor() {
		return skinColor;
	}

	public void setSkinColor(String skinColor) {
		this.skinColor = skinColor;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	//--------------------------------------------------------------------
	public void addModification(ModificationImpl mod) {
		if (mod instanceof AttributeModification)
			attributes.addModification((AttributeModification) mod);
		else
			logger.error("Don't know how to process "+mod.getClass());
	}

	//--------------------------------------------------------------------
	public void removeModification(ModificationImpl mod) {
		if (mod instanceof AttributeModification)
			attributes.removeModification((AttributeModification) mod);
		else
			logger.error("Don't know how to process "+mod.getClass());
	}

	//--------------------------------------------------------------------
	/**
	 * @return the ruleset
	 */
	public UbiquityRuleset getRuleset() {
		return ruleset;
	}

	//--------------------------------------------------------------------
	/**
	 * @param ruleset the ruleset to set
	 */
	public void setRuleset(UbiquityRuleset ruleset) {
		assert ruleset!=null;
		this.ruleset = ruleset;
	}

	//-------------------------------------------------------------------
	public String getNotes() {
		return notes;
	}

	//-------------------------------------------------------------------
	public void setNotes(String txt) {
		this.notes = txt;
	}

	//-------------------------------------------------------------------
	public void setRace(String value) {
		this.race= value;
	}

	//-------------------------------------------------------------------
	public String getRace() {
		return race;
	}

}
