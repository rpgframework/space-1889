/**
 * 
 */
package org.prelle.ubiquity;

import java.util.Date;
import java.util.List;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.ubiquity.modifications.ModificationChoice;
import org.prelle.ubiquity.modifications.ModificationList;

import de.rpgframework.genericrpg.Reward;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class RewardImpl implements Reward {

	@Element
	private String title;
	@Attribute
	private int exp;
	@Attribute(required=false)
	protected Date date;
	@Attribute(required=false)
	private String id;
	@Element
	private ModificationList modifications;
	
	//-------------------------------------------------------------------
	public RewardImpl() {
		modifications = new ModificationList();
	}
	
	//-------------------------------------------------------------------
	public RewardImpl(int exp, String name) {
		this.exp = exp;
		this.title = name;
		modifications = new ModificationList();
		date = new Date(System.currentTimeMillis());
	}
	
	//-------------------------------------------------------------------
	public String toString() {
		return "Reward '"+title+"' (EP "+exp+") at "+date+"  (id="+id+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.Reward#getTitle()
	 */
	@Override
	public String getTitle() {
		return title;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.Reward#setTitle(java.lang.String)
	 */
	@Override
	public void setTitle(String title) {
		this.title = title;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.Reward#getExperiencePoints()
	 */
	@Override
	public int getExperiencePoints() {
		return exp;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.Reward#setExperiencePoints(int)
	 */
	@Override
	public void setExperiencePoints(int exp) {
		this.exp = exp;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.Reward#getModifications()
	 */
	@Override
	public List<Modification> getModifications() {
		return modifications;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.Reward#setModifications(org.prelle.ubiquity.modifications.ModificationList)
	 */
	@Override
	public void setModifications(List<Modification> mods) {
		this.modifications = (ModificationList)mods;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.Reward#addModification(org.prelle.ubiquity.modifications.ModificationImpl)
	 */
	@Override
	public void addModification(Modification mod) {
		if (mod instanceof ModificationChoice) {
			if (((ModificationChoice)mod).getOptions().length>2 )
				throw new IllegalArgumentException("ModificationChoice without options: "+mod);
		}
		modifications.add(mod);
	}

	@Override
	public Date getDate() {
		return date;
	}

	@Override
	public void setDate(Date date) {
		this.date = date;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.Reward#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.Reward#setId(java.lang.String)
	 */
	@Override
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public void removeModification(Modification mod) {
		modifications.remove(mod);
	}

}
