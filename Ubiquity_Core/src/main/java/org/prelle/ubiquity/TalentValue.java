/**
 * 
 */
package org.prelle.ubiquity;

import java.util.ArrayList;
import java.util.List;

import org.prelle.ubiquity.modifications.ModificationImpl;
import org.prelle.ubiquity.persist.SkillConverter;
import org.prelle.ubiquity.persist.TalentConverter;

import de.rpgframework.genericrpg.ModifyableValue;
import de.rpgframework.genericrpg.SelectedValue;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author Stefan
 *
 */
public class TalentValue extends ModifyableImpl implements Comparable<TalentValue>, SelectedValue<Talent> {

	@org.prelle.simplepersist.Attribute(name="idref")
	@org.prelle.simplepersist.AttribConvert(TalentConverter.class)
	private Talent talent;
	@org.prelle.simplepersist.Attribute(name="skill",required=false)
	@org.prelle.simplepersist.AttribConvert(SkillConverter.class)
	private Skill skill;

	@org.prelle.simplepersist.Attribute(name="level", required=false)
	private int level;

	//--------------------------------------------------------------------
	public TalentValue() {
	}

	//--------------------------------------------------------------------
	public TalentValue(Talent talent, int level) {
		this.talent = talent;
		this.level  = level;
	}

	//--------------------------------------------------------------------
	public TalentValue(Talent talent, Skill option, int level) {
		this.talent = talent;
		this.level  = level;
		this.skill  = option;
	}

	//--------------------------------------------------------------------
	public String toString() {
		if (talent==null)
			return "NO_TALENT_DEFINED "+level;
		StringBuffer buf = new StringBuffer(talent.getName());
		if (skill!=null) 
			buf.append(" ("+skill.getName()+")");
		if (talent.getLevel()>1)
			buf.append(" "+level);

		return buf.toString();
	}

	//--------------------------------------------------------------------
	public String getName() {
		StringBuffer buf = new StringBuffer();
		buf.append( (talent==null)?buf.append("NO_TALENT_DEFINED"):talent.getName());
		buf.append(" ");
		if (skill!=null)
			buf.append(skill.getName()+" ");
		if (talent.getLevel()>1)
			buf.append(String.valueOf(level));
		return buf.toString();
	}

	//--------------------------------------------------------------------
	/**
	 * @return the talent
	 */
	public Talent getTalent() {
		return talent;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(TalentValue other) {
		return talent.compareTo(other.getTalent());
	}

	//--------------------------------------------------------------------
	/**
	 * @param level the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	//--------------------------------------------------------------------
	public Skill getSkillOption() {
		return skill;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValue#getModifyable()
	 */
	@Override
	public Talent getModifyable() {
		return talent;
	}

}
