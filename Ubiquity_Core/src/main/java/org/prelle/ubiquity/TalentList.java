/**
 * 
 */
package org.prelle.ubiquity;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="talents")
@ElementList(entry="talent",type=Talent.class,inline=true)
public class TalentList extends ArrayList<Talent> {

}
