/**
 * 
 */
package org.prelle.ubiquity;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="templates")
@ElementList(entry="template",type=GenerationTemplate.class,inline=true)
public class GenerationTemplateList extends ArrayList<GenerationTemplate> {

}
