/**
 * 
 */
package org.prelle.ubiquity;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.rpgframework.character.HardcopyPluginData;
import de.rpgframework.character.RulePlugin;
import de.rpgframework.character.RuleSpecificCharacterObject;


/**
 * @author Stefan
 *
 */
public abstract class BasePluginData implements HardcopyPluginData {
	
	protected static Logger logger = LogManager.getLogger("ubiquity");

	protected static PrintWriter MISSING;
	
	protected transient ResourceBundle i18n;
	protected transient ResourceBundle i18nHelp;

	protected transient RulePlugin<? extends RuleSpecificCharacterObject> plugin;

	//--------------------------------------------------------------------
	static {
		try {
			if (System.getProperty("logdir")==null)
				MISSING = new PrintWriter(Files.createTempDirectory("genesis")+System.getProperty("file.separator")+"/missing-keys-ubiquity.txt");
			else
				MISSING = new PrintWriter(System.getProperty("logdir")+System.getProperty("file.separator")+"/missing-keys-ubiquity.txt");
		} catch (IOException e) {
			logger.error("Failed setting up file for missing keys",e);
		}
	}

	//--------------------------------------------------------------------
	public BasePluginData() {
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.PluginData#setPlugin(de.rpgframework.RulePlugin)
	 */
	@Override
	public void setPlugin(RulePlugin<?> plugin) {
		this.plugin = plugin;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.PluginData#getPlugin()
	 */
	@Override
	public RulePlugin<?> getPlugin() {
		return plugin;
	}

	//--------------------------------------------------------------------
	public abstract String getName();

	//--------------------------------------------------------------------
	public abstract String getPageI18NKey();

	//--------------------------------------------------------------------
	public abstract String getHelpI18NKey();

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.HardcopyPluginData#getPage()
	 */
	@Override
	public int getPage() {
		String key = getPageI18NKey();
		try {
			return Integer.parseInt(i18n.getString(key));
		} catch (MissingResourceException mre) {
			logger.error("Missing property '"+key+"' in "+i18n.getBaseBundleName());
			if (MISSING!=null)
				MISSING.println(mre.getKey()+"   \t in "+i18n.getBaseBundleName()+".properties");
		} catch (NumberFormatException nfe) {
			logger.error("property '"+key+"' in "+i18n.getBaseBundleName()+" is not an integer");		
		}
		return 0;
	}

	//-------------------------------------------------------------------
	public void setResourceBundle(ResourceBundle i18n) {
		this.i18n = i18n;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.PluginData#getResourceBundle()
	 */
	@Override
	public ResourceBundle getResourceBundle() {
		return i18n;
	}

	//-------------------------------------------------------------------
	public void setHelpResourceBundle(ResourceBundle i18n) {
		this.i18nHelp = i18n;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.PluginData#getHelpResourceBundle()
	 */
	@Override
	public ResourceBundle getHelpResourceBundle() {
		return i18nHelp;
	}
	
	//--------------------------------------------------------------------
	/**
	 * If this data reflects a product, this method returns the 
	 * unabbreviated product name.
	 * @return full product name or NULL
	 */
	public String getProductName() {
		try {
			return i18n.getString("plugin."+getPlugin().getID()+".productname.full");
		} catch (MissingResourceException e) {
			logger.error(e.toString()+" in "+i18n.getBaseBundleName());
			if (MISSING!=null)
				MISSING.println(e.getKey()+"   \t in "+i18n.getBaseBundleName()+".properties");
		}
		return null;
	}
	
	//--------------------------------------------------------------------
	/**
	 * If this plugin reflects a product, this method returns the 
	 * abbreviated product name.
	 * @return Short product name or NULL
	 */
	public String getProductNameShort() {
		try {
			return i18n.getString("plugin."+getPlugin().getID()+".productname.short");
		} catch (MissingResourceException e) {
			logger.error(e.toString()+" in "+i18n.getBaseBundleName());
			if (MISSING!=null)
				MISSING.println(e.getKey()+"   \t in "+i18n.getBaseBundleName()+".properties");
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.HardcopyPluginData#getHelpText()
	 */
	@Override
	public String getHelpText() {
		if (i18nHelp==null)
			return null;
		String key = getHelpI18NKey();
		
		try {
			return i18nHelp.getString(key);
		} catch (MissingResourceException mre) {
			logger.error("Missing property '"+key+"' in "+i18nHelp.getBaseBundleName());
			if (MISSING!=null)
				MISSING.println(mre.getKey()+"   \t in "+i18n.getBaseBundleName()+".properties");
		}
		return null;
	}
	
	//-------------------------------------------------------------------
	public static void flushMissingKeys() {
		if (MISSING!=null)
			MISSING.flush();
	}

}
