/**
 * 
 */
package org.prelle.ubiquity;

import org.prelle.ubiquity.persist.FlawConverter;

import de.rpgframework.genericrpg.SelectedValue;

/**
 * @author Stefan
 *
 */
public class FlawValue implements Comparable<FlawValue>, SelectedValue<Flaw> {

	@org.prelle.simplepersist.Attribute(name="idref")
	@org.prelle.simplepersist.AttribConvert(FlawConverter.class)
	private Flaw flaw;

	//--------------------------------------------------------------------
	public FlawValue() {
		
	}

	//--------------------------------------------------------------------
	public FlawValue(Flaw flaw) {
		this.flaw = flaw;
	}

	//--------------------------------------------------------------------
	public String toString() {
		if (flaw==null)
			return "NO_FLAW_DEFINED";
		return flaw.getName();
	}

	//--------------------------------------------------------------------
	/**
	 * @return the flaw
	 */
	public Flaw getFlaw() {
		return flaw;
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(FlawValue other) {
		return flaw.compareTo(other.getFlaw());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectedValue#getModifyable()
	 */
	@Override
	public Flaw getModifyable() {
		return flaw;
	}

}
