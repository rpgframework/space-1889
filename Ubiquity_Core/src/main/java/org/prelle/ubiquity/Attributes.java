/**
 * 
 */
package org.prelle.ubiquity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.ubiquity.modifications.AttributeModification;

/**
 * @author prelle
 *
 */
public class Attributes extends ArrayList<AttributeValue> {

	private static final long serialVersionUID = -7540069057488032815L;

	private final static Logger logger = LogManager.getLogger("ubiquity");

	private transient Map<Attribute, AttributeValue> secondary;
	
	//-------------------------------------------------------------------
	/**
	 */
	public Attributes() {
		// Don't fill initial attributes, since this might clash when loading
		// from XML. For new characters initialize from somewhere else
		
		secondary = new HashMap<Attribute, AttributeValue>();
		for (Attribute attr : Attribute.secondaryValues()) {
			AttributeValue toAdd = new AttributeValue(attr, 0);
			secondary.put(attr, toAdd);
		}
	}

	//-------------------------------------------------------------------
	public void initBaseAttributes() {
		for (Attribute attr : Attribute.primaryValues()) {
			AttributeValue toAdd = new AttributeValue(attr, 0);
			this.add(toAdd);
		}		
	}

//	//-------------------------------------------------------------------
//	public String toString() {
//		return data.toString();
//	}

	//-------------------------------------------------------------------
	public String dump() {
		StringBuffer buf = new StringBuffer();
		for (AttributeValue tmp : this)
			buf.append("\n "+tmp.getAttribute()+" \t "+tmp);
		for (Attribute tmp : Attribute.secondaryValues())
			buf.append("\n "+tmp+" \t "+get(tmp));
		
		return buf.toString();
	}
	
//	//-------------------------------------------------------------------
//	public List<AttributeValue> getAttributes() {
//		java.util.Collections.sort(data);
//		return data;
//	}
//
//	//-------------------------------------------------------------------
//	public boolean add(AttributeValue pair) {
//		set(pair.getAttribute(), pair.getValue());
//		return true;
//	}
//
//	//-------------------------------------------------------------------
//	public void set(Attribute key, int value) {
//
//		if (key.isPrimary()) {
//			for (AttributeValue pair : this) {
//				if (pair.getAttribute()==key) {
//					pair.setPoints(value);
//					return;
//				}
//			}
//			throw new NullPointerException("No primary attribute "+key+" found");
//		}
//		AttributeValue pair = secondary.get(key);
//		if (pair==null)
//			throw new NullPointerException("No secondary attribute "+key+" found");
//		pair.setPoints(value);
//		
////		switch (key) {
////		case MOVE:
////		case PERCEPTION:
////		case INITIATIVE:
////		case DEFENSE:
////		case STUN:
////			throw new IllegalArgumentException("Cannot set secondary attributes except size");
////		
////		default: 
////		// Find matching attribute
////		for (AttributeValue pair : this) {
////			if (pair.getAttribute()==key) {
////				pair.setPoints(value);
////				return;
////			}
////		}
////
////		AttributeValue toAdd = new AttributeValue(key, value);
////		super.add(toAdd);
////		}
//	}

	//-------------------------------------------------------------------
	public int getValue(Attribute key) {
		return get(key).getValue();
//		switch (key) {
//		case MOVE:
//		case PERCEPTION:
//		case INITIATIVE:
//		case DEFENSE:
//		case STUN:
//			throw new IllegalArgumentException("Cannot get secondary attributes except size");
//		default:
//			for (AttributeValue pair : this) {
//				if (pair.getAttribute()==key) {				
//					return pair.getValue();
//				}
//			}
//		}
//		
//
//		logger.error("Something accessed an unset attribute: "+key);
//		return -1;
	}

	//-------------------------------------------------------------------
	public AttributeValue get(Attribute key) {
		if (key==null) 
			throw new NullPointerException("Attribute may not be null");
		for (AttributeValue pair : this) {
			if (pair.getAttribute()==key) {				
				return pair;
			}
		}
		AttributeValue pair = secondary.get(key);
		if (pair!=null) {
			return pair;
		}

		logger.error("Something accessed an unset attribute: "+key);
		throw new NoSuchElementException(String.valueOf(key));
	}

	//--------------------------------------------------------------------
	public void addModification(AttributeModification mod) {
		get(mod.getAttribute()).addModification(mod);
	}

	//--------------------------------------------------------------------
	public void removeModification(AttributeModification mod) {
		get(mod.getAttribute()).removeModification(mod);
	}

}
