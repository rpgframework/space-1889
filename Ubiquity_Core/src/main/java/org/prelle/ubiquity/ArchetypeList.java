/**
 * 
 */
package org.prelle.ubiquity;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="archetypes")
@ElementList(entry="archetype",type=Archetype.class,inline=true)
public class ArchetypeList extends ArrayList<Archetype> {

}
