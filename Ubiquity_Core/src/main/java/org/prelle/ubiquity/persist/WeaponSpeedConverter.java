/**
 * 
 */
package org.prelle.ubiquity.persist;

import org.prelle.simplepersist.StringValueConverter;
import org.prelle.simplepersist.SerializationException;
import org.prelle.ubiquity.items.Weapon;
import org.prelle.ubiquity.items.Weapon.Speed;

/**
 * @author prelle
 *
 */
public class WeaponSpeedConverter implements StringValueConverter<Weapon.Speed> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(Weapon.Speed value) throws Exception {
		switch (value) {
		case SLOW   : return "S";
		case AVERAGE: return "A";
		case FAST   : return "F";
		default:
			return String.valueOf(value);
		}			
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public Weapon.Speed read(String toParse) throws Exception {
		if (toParse.equals("A")) return Speed.AVERAGE;
		if (toParse.equals("S")) return Speed.SLOW;
		if (toParse.equals("F")) return Speed.FAST;

		throw new SerializationException("Don't know how to parse speed: "+toParse);
	}

}
