/**
 * 
 */
package org.prelle.ubiquity.persist;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;

import org.prelle.simplepersist.ConstructorParams;
import org.prelle.simplepersist.SerializationException;
import org.prelle.simplepersist.XMLElementConverter;
import org.prelle.simplepersist.marshaller.XmlNode;
import org.prelle.simplepersist.unmarshal.XMLTreeItem;
import org.prelle.ubiquity.Skill;
import org.prelle.ubiquity.UbiquityCore;
import org.prelle.ubiquity.UbiquityRuleset;

import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
@ConstructorParams({UbiquityCore.KEY_RULES})
public class SkillElementConverter implements XMLElementConverter<Skill> {

	private UbiquityRuleset ruleset;
	
	//-------------------------------------------------------------------
	public SkillElementConverter(RoleplayingSystem rules) {
		ruleset = UbiquityCore.getRuleset(rules);
		if (ruleset==null)
			throw new NullPointerException("Missing "+rules+" ruleset in UbiquityCore");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.marshaller.XmlNode, java.lang.Object)
	 */
	@Override
	public void write(XmlNode node, Skill value) throws Exception {
		node.setAttribute("skill", value.getId());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public Skill read(XMLTreeItem node, StartElement ev, XMLEventReader evRd) throws Exception {
		Attribute attr = ev.getAttributeByName(new QName("skill"));
		if (attr==null)
			attr = ev.getAttributeByName(new QName("idref"));
		if (attr==null)
			throw new SerializationException("Missing attribute 'idref' or 'skill' in "+ev);
		String idref = attr.getValue();
		if ("CHOSEN".equals(idref))
			return Skill.CHOSEN;
		
		Skill skill = ruleset.getSkill(idref);
		if (skill==null)
			throw new SerializationException("Unknown skill ID '"+idref+"' in "+ev);

		return skill;
	}

}
