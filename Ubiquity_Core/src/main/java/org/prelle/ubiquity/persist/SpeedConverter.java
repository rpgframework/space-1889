/**
 * 
 */
package org.prelle.ubiquity.persist;

import org.prelle.simplepersist.StringValueConverter;
import org.prelle.simplepersist.SerializationException;

/**
 * @author prelle
 *
 */
public class SpeedConverter implements StringValueConverter<Integer> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(Integer value) throws Exception {
		if (value<0)
			return (-value)+"N";
		else
			return value+"L";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public Integer read(String toParse) throws Exception {
		String num_s = toParse.substring(0, toParse.length()-1); 
		char type = toParse.toUpperCase().charAt(toParse.length()-1);
		int val = Integer.parseInt(num_s);
		if (type=='L' || type=='D')
			return val;
		if (type=='N')
			return -val;

		throw new SerializationException("Don't know how to parse damage: "+toParse);
	}

}
