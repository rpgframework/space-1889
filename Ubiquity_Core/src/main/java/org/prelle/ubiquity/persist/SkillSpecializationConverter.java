/**
 * 
 */
package org.prelle.ubiquity.persist;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import org.prelle.simplepersist.ConstructorParams;
import org.prelle.simplepersist.StringValueConverter;
import org.prelle.simplepersist.SerializationException;
import org.prelle.ubiquity.Skill;
import org.prelle.ubiquity.SkillSpecialization;
import org.prelle.ubiquity.UbiquityCore;
import org.prelle.ubiquity.UbiquityRuleset;

import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
@ConstructorParams({UbiquityCore.KEY_RULES})
public class SkillSpecializationConverter implements StringValueConverter<SkillSpecialization> {

	private UbiquityRuleset ruleset;
	
	//-------------------------------------------------------------------
	public SkillSpecializationConverter(RoleplayingSystem rules) {
		ruleset = UbiquityCore.getRuleset(rules);
		if (ruleset==null)
			throw new NullPointerException("Missing "+rules+" ruleset in UbiquityCore");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(SkillSpecialization v) throws Exception {
		if (v.getId()==null)
			throw new NullPointerException("No skill specialization set in "+v);
		if (v.getSkill()==null)
			throw new NullPointerException("No skill set in specialization "+v);
		String id = v.getSkill().getId()+"/"+v.getId();
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public SkillSpecialization read(String v) throws Exception {
		try {
			StringTokenizer tok = new StringTokenizer(v, "/- ");
			String idref   = tok.nextToken();
			String special = tok.nextToken();
			
			Skill skill = ruleset.getSkill(idref);
			if (skill==null)
				throw new SerializationException("Unknown skill ID '"+idref);

			for (SkillSpecialization spec : skill.getSpecializations())
				if (spec.getId().equals(special))
					return spec;
			throw new SerializationException("Unknown specialization '"+special+"' for skill '"+idref);
		} catch (NoSuchElementException e) {
			throw new SerializationException("Cannot parse '"+v+"' to SkillSpecialization",e);
		}
	}

}
