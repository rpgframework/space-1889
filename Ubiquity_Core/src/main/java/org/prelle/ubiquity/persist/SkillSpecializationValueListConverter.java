/**
 * 
 */
package org.prelle.ubiquity.persist;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.prelle.simplepersist.ConstructorParams;
import org.prelle.simplepersist.SerializationException;
import org.prelle.simplepersist.StringValueConverter;
import org.prelle.ubiquity.Skill;
import org.prelle.ubiquity.SkillSpecialization;
import org.prelle.ubiquity.SkillSpecializationValue;
import org.prelle.ubiquity.SkillValue;
import org.prelle.ubiquity.UbiquityCore;
import org.prelle.ubiquity.UbiquityRuleset;

import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
@ConstructorParams(value={UbiquityCore.KEY_RULES})
public class SkillSpecializationValueListConverter implements StringValueConverter<List<SkillSpecializationValue>> {

	private UbiquityRuleset ruleset;
	
	//-------------------------------------------------------------------
	public SkillSpecializationValueListConverter(RoleplayingSystem rules) {
		ruleset = UbiquityCore.getRuleset(rules);
		if (ruleset==null)
			throw new NullPointerException("Missing "+rules+" ruleset in UbiquityCore");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(List<SkillSpecializationValue> value) throws Exception {
		if (value==null || value.isEmpty())
			return null;
		
		StringBuffer buf = new StringBuffer();
		for (Iterator<SkillSpecializationValue> it=value.iterator(); it.hasNext(); ) {
			SkillSpecializationValue spec = it.next();
			buf.append(spec.getSpecialization().getSkill().getId()+":");
			buf.append(spec.getSpecialization().getId());
			buf.append(":"+spec.getValue());
			if (it.hasNext())
				buf.append(",");
		}
		return buf.toString();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public List<SkillSpecializationValue> read(String special) throws Exception {
		List<SkillSpecializationValue> ret = new ArrayList<SkillSpecializationValue>();

		StringTokenizer tok = new StringTokenizer(special,",");
		while (tok.hasMoreTokens()) {
			String single = tok.nextToken();
			if (single.equals("null"))
				continue;
			StringTokenizer subTok = new StringTokenizer(single,":");
			String skillID = subTok.nextToken();
			Skill skill = UbiquityCore.getRuleset(RoleplayingSystem.SPACE1889).getSkill(skillID);
			if (skill==null) {
				throw new SerializationException("Unknown skill '"+skillID+"' referenced in skill specialization");
			}

			String search = subTok.nextToken();
			int val = 1;
			if (subTok.hasMoreTokens()) {
				val = Integer.parseInt(subTok.nextToken());
			}
			boolean missing = true;
			for (SkillSpecialization spec : skill.getSpecializations()) {
//				System.err.println("Compare "+spec.getId()+" with "+search);
				if (spec.getId().equals(search)) {
//					if (!ret.contains(spec))
						ret.add(new SkillSpecializationValue(spec, val));
					missing = false;
					break;
				}
			}
			if (missing) 
				throw new SerializationException("Unknown specialization '"+search+"' for skill '"+skill);
		}
//		System.err.println("Result "+ret);
//		if (!special.equals("null"))
//			System.exit(10);
		return ret;
	}

}
