/**
 * 
 */
package org.prelle.ubiquity.persist;

import org.prelle.simplepersist.ConstructorParams;
import org.prelle.simplepersist.StringValueConverter;
import org.prelle.simplepersist.SerializationException;
import org.prelle.ubiquity.Resource;
import org.prelle.ubiquity.UbiquityCore;
import org.prelle.ubiquity.UbiquityRuleset;

import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
@ConstructorParams({UbiquityCore.KEY_RULES})
public class ResourceConverter implements StringValueConverter<Resource> {

	private UbiquityRuleset ruleset;
	
	//-------------------------------------------------------------------
	public ResourceConverter(RoleplayingSystem rules) {
		ruleset = UbiquityCore.getRuleset(rules);
		if (ruleset==null)
			throw new NullPointerException("Missing "+rules+" ruleset in UbiquityCore");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(Resource value) throws Exception {
		return value.getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public Resource read(String idref) throws Exception {
		Resource skill = ruleset.getResource(idref);
		if (skill==null)
			throw new SerializationException("Unknown resource ID '"+idref);

		return skill;
	}

}
