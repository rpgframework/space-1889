/**
 * 
 */
package org.prelle.ubiquity.persist;

import org.prelle.simplepersist.StringValueConverter;
import org.prelle.simplepersist.SerializationException;
import org.prelle.ubiquity.items.RangeWeapon;
import org.prelle.ubiquity.items.RangeWeapon.FireRate;

/**
 * @author prelle
 *
 */
public class FireRateConverter implements StringValueConverter<RangeWeapon.FireRate> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(RangeWeapon.FireRate value) throws Exception {
		switch (value) {
		case EVERY_FOURTH_TURN: return "1/4"; 
		case EVERY_OTHER_TURN: return "1/2"; 
		case EVERY_TURN      : return "1";
		case SEMIAUTOMATIC   : return "M";
		case AUTOMATIC       : return "A";
		default:
			return String.valueOf(value);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public RangeWeapon.FireRate read(String toParse) throws Exception {
		if (toParse.equals("¼")) return FireRate.EVERY_FOURTH_TURN;
		if (toParse.equals("1/4")) return FireRate.EVERY_FOURTH_TURN;
		if (toParse.equals("½")) return FireRate.EVERY_OTHER_TURN;
		if (toParse.equals("1/2")) return FireRate.EVERY_OTHER_TURN;
		if (toParse.equals("1")) return FireRate.EVERY_TURN;
		if (toParse.equals("H")) return FireRate.SEMIAUTOMATIC;
		if (toParse.equals("M")) return FireRate.SEMIAUTOMATIC;
		if (toParse.equals("V")) return FireRate.AUTOMATIC;
		if (toParse.equals("A")) return FireRate.AUTOMATIC;

		throw new SerializationException("Don't know how to parse rate: "+toParse);
	}

}
