/**
 *
 */
package org.prelle.ubiquity.persist;

import org.prelle.simplepersist.StringValueConverter;
import org.prelle.simplepersist.SerializationException;
import org.prelle.ubiquity.items.Capacity;
import org.prelle.ubiquity.items.Capacity.AmmunitionStorage;

/**
 * @author prelle
 *
 */
public class CapacityConverter implements StringValueConverter<Capacity> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(Capacity value) throws Exception {
		return value.getAmount()+value.getStorage().getShortName();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public Capacity read(String toParse) throws Exception {
		String num_s = null;
		String type = null;
		int pos = toParse.indexOf("(");
		if (pos>0) {
			num_s = toParse.substring(0, pos).trim();
			type = toParse.toLowerCase().substring(pos+1, pos+2);
		} else {
			num_s = toParse.substring(0, toParse.length()-1);
			type = toParse.toLowerCase().substring(toParse.length()-1);
		}
//		System.err.println(String.format("orig='%s'  pos=%d  num_s='%s'  type='%s'", toParse, pos, num_s, type));
		int val = Integer.parseInt(num_s);

		Capacity cap = new Capacity();
		cap.setAmount(val);
//		for (AmmunitionStorage store : AmmunitionStorage.values()) {
//			if (store.getShortName().equalsIgnoreCase(type)) {
//				cap.setStorage(store);
//				break;
//			}
//		}
		switch (type) {
		case "g": cap.setStorage(AmmunitionStorage.BELT); break;
		case "l": cap.setStorage(AmmunitionStorage.CLIP); break;
		case "i": cap.setStorage(AmmunitionStorage.INTERNAL); break;
		case "m": cap.setStorage(AmmunitionStorage.MAGAZINE); break;
		case "r": cap.setStorage(AmmunitionStorage.REVOLVER); break;
		}
		if (cap.getStorage()!=null)
			return cap;

		throw new SerializationException("Don't know how to parse capacity: "+toParse);
	}

}
