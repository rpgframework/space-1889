/**
 * 
 */
package org.prelle.ubiquity.modifications;


/**
 * @author prelle
 *
 */
public enum ModificationValueType {

	RELATIVE,
	ABSOLUT,
	
}
