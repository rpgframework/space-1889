/**
 * 
 */
package org.prelle.ubiquity.modifications;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
@Root(name="modifications")
@ElementListUnion({
    @ElementList(entry="selmod", type=ModificationChoice.class),
    @ElementList(entry="skillmod", type=SkillModification.class),
    @ElementList(entry="attrmod", type=AttributeModification.class),
    @ElementList(entry="resourcemod", type=ResourceModification.class),
    @ElementList(entry="talentmod", type=TalentModification.class),
    @ElementList(entry="specmod", type=SpecializationModification.class),
 })
public class ModificationList extends ArrayList<Modification> {

	private static final long serialVersionUID = 1L;

	//-------------------------------------------------------------------
	/**
	 */
	public ModificationList() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public ModificationList(Collection<? extends ModificationImpl> c) {
		super(c);
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	public List<Modification> getModificiations() {
		return this;
	}
}
