/**
 * 
 */
package org.prelle.ubiquity.modifications;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.ubiquity.SkillSpecialization;
import org.prelle.ubiquity.persist.SkillSpecializationConverter;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class SpecializationModification extends ModificationImpl {
	
	@Attribute(required=true)
	@AttribConvert(SkillSpecializationConverter.class)
	private SkillSpecialization spec;
	@Attribute(required=false)
	private int value;
	
	//--------------------------------------------------------------------
	public SpecializationModification() {
	}

	//--------------------------------------------------------------------
	public SpecializationModification(SkillSpecialization spec) {
		this.spec = spec;
	}

	//--------------------------------------------------------------------
	public SpecializationModification(SkillSpecialization spec, int val) {
		this.spec = spec;
		this.value = val;
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Modification other) {
		return 0;
	}

	//--------------------------------------------------------------------
	public SkillSpecialization getSpecialization() {
		return spec;
	}

	//--------------------------------------------------------------------
	public String toString() {
		if (spec==null)
			return "<not set>";
		if (spec.getSkill()!=null)
			return spec.getSkill().getName()+"/"+spec.getName();
		return spec.getName();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @param value the value to set
	 */
	public void setValue(int value) {
		this.value = value;
	}

}
