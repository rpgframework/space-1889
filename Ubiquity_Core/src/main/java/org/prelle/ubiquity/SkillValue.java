/**
 * 
 */
package org.prelle.ubiquity;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.Root;
import org.prelle.ubiquity.modifications.SkillModification;
import org.prelle.ubiquity.persist.SkillConverter;
import org.prelle.ubiquity.persist.SkillSpecializationValueListConverter;

import de.rpgframework.genericrpg.ModifyableValue;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
@Root(name = "skillval")
public class SkillValue extends ModifyableImpl implements ModifyableValue<Skill>, Comparable<SkillValue> {

	@org.prelle.simplepersist.Attribute(name="skill")
	@org.prelle.simplepersist.AttribConvert(SkillConverter.class)
	private Skill skill;

	@org.prelle.simplepersist.Attribute(name="special",required=false)
	@org.prelle.simplepersist.AttribConvert(SkillSpecializationValueListConverter.class)
	private List<SkillSpecializationValue> specializations;

	@org.prelle.simplepersist.Attribute(name="value")
	private int points;

	//-------------------------------------------------------------------
	public SkillValue() {
		this.points = -1;
		specializations = new ArrayList<SkillSpecializationValue>();
	}

	//-------------------------------------------------------------------
	public SkillValue(Skill skill, int val) {
		this.skill = skill;
		this.points = val;
		specializations = new ArrayList<SkillSpecializationValue>();
	}

	//-------------------------------------------------------------------
	public SkillValue(SkillValue copy) {
		this.skill = copy.getModifyable();
		this.points = copy.getPoints();
		specializations = new ArrayList<SkillSpecializationValue>(copy.getSpecializations());
	}

	//-------------------------------------------------------------------
	public String toString() {
		if (specializations==null)
			return String.format("%s  VAL=%d  MOD=%d", skill, points, getModifiedValue());
		if (skill!=null)
			return String.format("%s  VAL=%d  MOD=%d", skill.getId()+"/"+specializations, points, getModifiedValue());

		return String.format("%s  VAL=%d  MOD=%d", skill+"/"+specializations, points, getModifiedValue());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the value
	 */
	public int getPoints() {
		return points;
	}

	//-------------------------------------------------------------------
	/**
	 * @param value the value to set
	 */
	public void setPoints(int value) {
		this.points = value;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifyable()
	 */
	@Override
	public Skill getModifyable() {
		return skill;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SkillValue other) {
		return skill.compareTo(other.getModifyable());
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifiedValue()
	 */
	@Override
	public int getModifiedValue() {
		return points + getModifier();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the specialization
	 */
	public List<SkillSpecializationValue> getSpecializations() {
		return specializations;
	}

	//-------------------------------------------------------------------
	public SkillSpecializationValue getSpecialization(SkillSpecialization spec) {
		for (SkillSpecializationValue tmp : specializations) {
			if (tmp.getSpecialization()==spec)
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @param specialization the specialization to set
	 */
	public void addSpecialization(SkillSpecializationValue specialization) {
		if (!this.specializations.contains(specialization))
			this.specializations.add(specialization);
	}

	//-------------------------------------------------------------------
	public void removeSpecialization(SkillSpecializationValue specialization) {
		this.specializations.remove(specialization);
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifier()
	 */
	@Override
	public int getModifier() {
		int ret = 0;
		for (Modification mod : getModifications()) {
			if (mod instanceof SkillModification) {
				ret += ((SkillModification)mod).getValue();
			}
		}
		return ret;
	}

}
