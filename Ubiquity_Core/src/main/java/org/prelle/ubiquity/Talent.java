/**
 * 
 */
package org.prelle.ubiquity;

import java.text.Collator;
import java.util.ArrayList;
import java.util.List;
import java.util.MissingResourceException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;
import org.prelle.ubiquity.modifications.ModificationList;
import org.prelle.ubiquity.persist.SkillElementConverter;
import org.prelle.ubiquity.requirements.RequirementList;

import de.rpgframework.character.PluginData;
import de.rpgframework.genericrpg.SelectableItem;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.Modifyable;

/**
 * @author prelle
 *
 */
public class Talent extends BasePluginData implements PluginData, Comparable<Talent>, Modifyable, SelectableItem {
	
	private static Logger logger = LogManager.getLogger("ubiquity");
	
	public enum Type {
		NORMAL,
		PER_SKILL,
		PER_ATTRIBUTE
	}

	public enum Special {
		/**
		 * Replace (when called) the skill options with all known skills
		 * from Core. For groups add group as skill as well as all 
		 * specializations.
		 */
		ALL_SKILLS,
	}
	
	@org.prelle.simplepersist.Attribute
	private String    id;
	@org.prelle.simplepersist.Attribute(required=false)
	private int    level;
	@org.prelle.simplepersist.Attribute(name="gen")
	private boolean  generationOnly;
	@Element
	private ModificationList modifications;
	@Element(name="requires")
	private RequirementList requirements;
	
	@ElementList(type=Skill.class,entry="skillref",convert=SkillElementConverter.class)
	private List<Skill> skillOptions;
	@org.prelle.simplepersist.Attribute(required=false)
	private Special special;

	//-------------------------------------------------------------------
	/**
	 */
	public Talent() {
		level = 1;
		modifications = new ModificationList();
		requirements  = new RequirementList();
		skillOptions  = new ArrayList<Skill>();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "talent."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "talent."+id+".desc";
	}

	//-------------------------------------------------------------------
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer(getName());
		if (!requirements.isEmpty())
			buf.append(" (REQ: "+requirements+")");
		if (modifications.size()>0)
			buf.append(" (MOD: "+modifications+")");
		return buf.toString();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the name
	 */
	public String getName() {
		if (i18n==null)
			return "talent."+id;
		try {
			return i18n.getString("talent."+id);
		} catch (Exception e) {
			logger.error(e.toString());
		}
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Talent o) {
		return Collator.getInstance().compare(getName(), o.getName());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the requirement
	 */
	public RequirementList getRequirements() {
		return requirements;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the skillOptions
	 */
	public List<Skill> getSkillOptions() {
		if (special==Special.ALL_SKILLS) {
			logger.debug("getSkillOptions() dynamically accesses rules");
			return new ArrayList<Skill>(UbiquityCore.getRuleset(super.getPlugin().getRules()).getSkills());
		}
		return skillOptions;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.PluginData#getId()
	 * @see de.rpgframework.genericrpg.SelectableItem#getId()
	 */
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectableItem#getTypeId()
	 */
	@Override
	public String getTypeId() {
		return "talent";
	}

	//--------------------------------------------------------------------
	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the modifications
	 */
	public List<Modification> getModifications() {
		return modifications;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the generationOnly
	 */
	public boolean isGenerationOnly() {
		return generationOnly;
	}

	//-------------------------------------------------------------------
	public String getShortDescription() {
		if (i18n==null)
			return null;
		String key = "talent."+id+".shortdesc";
		try {
			return i18n.getString(key);
		} catch (MissingResourceException mre) {
			logger.error("Missing property '"+key+"' in "+i18n.getBaseBundleName());
			if (MISSING!=null)
				MISSING.println(mre.getKey()+"   \t in "+i18n.getBaseBundleName()+".properties");
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modifyable#setModifications(java.util.List)
	 */
	@Override
	public void setModifications(List<Modification> mods) {
		modifications.clear();
		modifications.addAll(mods);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modifyable#addModification(de.rpgframework.genericrpg.modification.Modification)
	 */
	@Override
	public void addModification(Modification mod) {
		modifications.add(mod);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modifyable#removeModification(de.rpgframework.genericrpg.modification.Modification)
	 */
	@Override
	public void removeModification(Modification mod) {
		modifications.remove(mod);
	}

}
