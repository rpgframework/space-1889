/**
 * 
 */
package org.prelle.ubiquity;

import java.text.Collator;
import java.util.ResourceBundle;

/**
 * @author prelle
 *
 */
public class Motivation extends BasePluginData implements Comparable<Motivation> {
	
	@org.prelle.simplepersist.Attribute
	private String    id;

	//-------------------------------------------------------------------
	/**
	 */
	public Motivation() {
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "motivation."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "motivation."+id+".desc";
	}

	//-------------------------------------------------------------------
	public String toString() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Motivation o) {
		return Collator.getInstance().compare(getName(), o.getName());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the name
	 */
	public String getName() {
		return i18n.getString("motivation."+id);
	}

	//-------------------------------------------------------------------
	public void setResourceBundle(ResourceBundle i18n) {
		this.i18n = i18n;
	}

}
