/**
 * 
 */
package org.prelle.ubiquity;

import java.text.Collator;
import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Replacer;
import org.prelle.ubiquity.persist.SkillConverter;

/**
 * @author prelle
 *
 */
public class Skill extends BasePluginData implements Comparable<Skill> {
	
	public final static Skill CHOSEN = new Skill("CHOSEN");

	@org.prelle.simplepersist.Attribute
	private String    id;
	@org.prelle.simplepersist.Attribute
	private Attribute attr;
	@org.prelle.simplepersist.Attribute(required=false)
	private boolean group;
	@ElementList(entry="specialization",type=SkillSpecialization.class)
	private List<SkillSpecialization> specializations;
	/**
	 * In case of Sub-Skills of a skillgroup, this references
	 * the parent skill (the skillgroup itself)
	 */
	private transient Skill parentSkill;
	private transient List<Skill> childSkills;

	//-------------------------------------------------------------------
	public Skill() {
		specializations = new ArrayList<SkillSpecialization>();
		childSkills     = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	Skill(String id) {
		this();
		this.id = id;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "skill."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "skill."+id+".desc";
	}

	//-------------------------------------------------------------------
	public String toString() {
		return id+" ("+attr+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the attr1
	 */
	public Attribute getAttribute() {
		return attr;
	}

	//-------------------------------------------------------------------
	/**
	 * @param attr1 the attr1 to set
	 */
	public void setAttribute(Attribute attr1) {
		this.attr = attr1;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Skill o) {
		return Collator.getInstance().compare(getName(), o.getName());
	}

	//-------------------------------------------------------------------
	public boolean isPartOfGroup() {
		if (id==null) return false;
		return id.indexOf(".")>0;
	}

	//-------------------------------------------------------------------
	public String getGroupName() {
		int pos = id.indexOf(".");
		if (pos>0)
			return i18n.getString("skill."+id.substring(0, pos));
		return null;
	}

	//-------------------------------------------------------------------
	public String getGroupNameShort() {
		int pos = id.indexOf(".");
		if (pos>0)
			return i18n.getString("skill."+id.substring(0, pos) + ".short");
		return null;
	}

	//-------------------------------------------------------------------
	public String getNameWithoutGroup() {
		return i18n.getString("skill."+id);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the name
	 */
	public String getName() {
		if (isPartOfGroup())
			return getGroupName()+"/"+getNameWithoutGroup();
		if (i18n==null)
			return id;
		return i18n.getString("skill."+id);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the specializations
	 */
	public List<SkillSpecialization> getSpecializations() {
		return specializations;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the group
	 */
	public boolean isGroup() {
		return group;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the parentSkill
	 */
	public Skill getParentSkill() {
		return parentSkill;
	}

	//--------------------------------------------------------------------
	/**
	 * @param parentSkill the parentSkill to set
	 */
	public void setParentSkill(Skill parentSkill) {
		this.parentSkill = parentSkill;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the childSkills
	 */
	public List<Skill> getChildSkills() {
		return childSkills;
	}

	//--------------------------------------------------------------------
	public void addChildSkill(Skill childSkill) {
		childSkills.add(childSkill);
	}

}

class SkillReference {
	@org.prelle.simplepersist.Attribute
	@AttribConvert(SkillConverter.class)
	Skill idref;
	
	public SkillReference() {
	}
	
	public SkillReference(Skill motiv) {
		this.idref = motiv;
	}
	
	public Skill get() {
		return idref;
	}
	public void set(Skill mot) {
		this.idref = mot;
	}
}

class SkillReplacer implements Replacer<Skill, SkillReference> {
	
	public SkillReplacer() {
	}

	@Override
	public SkillReference write(Skill value) throws Exception {
		return new SkillReference(value);
	}

	@Override
	public Skill read(SkillReference from) throws Exception {
		return from.get();
	}
	
}

