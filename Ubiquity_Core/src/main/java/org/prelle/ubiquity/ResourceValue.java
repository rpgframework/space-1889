/**
 * 
 */
package org.prelle.ubiquity;

import java.util.ArrayList;
import java.util.List;

import org.prelle.ubiquity.persist.ResourceConverter;

import de.rpgframework.genericrpg.ModifyableValue;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class ResourceValue implements ModifyableValue<Resource>, Comparable<ResourceValue> {

	@org.prelle.simplepersist.Attribute(name="idref")
	@org.prelle.simplepersist.AttribConvert(ResourceConverter.class)
	private Resource resource;

	@org.prelle.simplepersist.Attribute(name="level")
	private int level;

	@org.prelle.simplepersist.Attribute(name="comment",required=false)
	private String comment;

	//--------------------------------------------------------------------
	public ResourceValue() {
	}

	//--------------------------------------------------------------------
	public ResourceValue(Resource resrc, int level) {
		this.resource = resrc;
		this.level  = level;
	}

	//--------------------------------------------------------------------
	public String toString() {
		StringBuffer buf = new StringBuffer();
		if (resource==null)
			buf.append("UNSET_RESOURCE");
		else
			buf.append(resource.getName());
		buf.append(" "+level);
		if (comment!=null)
			buf.append(" '"+comment+"'");
		return buf.toString();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifyable()
	 */
	@Override
	public Resource getModifyable() {
		return resource;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getPoints()
	 */
	@Override
	public int getPoints() {
		return level;
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(ResourceValue other) {
		return resource.compareTo(other.getModifyable());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#setPoints(int)
	 */
	@Override
	public void setPoints(int level) {
		this.level = level;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	//--------------------------------------------------------------------
	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	//--------------------------------------------------------------------
	public String getIdReference() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addModification(Modification mod) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeModification(Modification mod) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Modification> getModifications() {
		// TODO Auto-generated method stub
		return new ArrayList<>();
	}

	@Override
	public int getModifier() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getModifiedValue() {
		// TODO Auto-generated method stub
		return getPoints();
	}

	@Override
	public void setModifications(List<Modification> mods) {
		// TODO Auto-generated method stub
		
	}

}
