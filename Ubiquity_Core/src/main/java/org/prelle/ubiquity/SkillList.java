/**
 * 
 */
package org.prelle.ubiquity;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="skills")
@ElementList(entry="skill",type=Skill.class,inline=true)
public class SkillList extends ArrayList<Skill> {

}
