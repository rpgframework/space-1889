/**
 * 
 */
package org.prelle.ubiquity.items;

/**
 * @author prelle
 *
 */
public class Damage {
	
	public enum Type {
		LETHAL,
		NON_LETHAL,
	}

	private int value;
	private Type type;
	
	//-------------------------------------------------------------------
	public Damage(int val, Type type) {
		this.value = val;
		this.type  = type;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

}
