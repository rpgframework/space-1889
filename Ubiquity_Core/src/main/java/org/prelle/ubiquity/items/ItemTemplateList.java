/**
 * 
 */
package org.prelle.ubiquity.items;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name = "items")
@ElementList(entry="item",type=ItemTemplate.class,inline=true)
public class ItemTemplateList extends ArrayList<ItemTemplate> {

	private static final long serialVersionUID = 1L;

	//-------------------------------------------------------------------
	public ItemTemplateList() {
	}

	//-------------------------------------------------------------------
	public ItemTemplateList(Collection<? extends ItemTemplate> c) {
		super(c);
	}

}
