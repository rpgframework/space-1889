/**
 * 
 */
package org.prelle.ubiquity.items;

import org.prelle.ubiquity.UbiquityCore;

/**
 * @author prelle
 *
 */
public enum ItemType {
	
	OTHER,
	WEAPON_CLOSE,
	WEAPON_RANGE,
	DEMOLITIONS,
	ARMOR,
	AMMUNITION,
	CLOTHING,
	SURVIVAL,
	PROFESSIONAL,
	CAMERA,
	TOOLS,
	VEHICLE_LAND,
	VEHICLE_AIR,
	VEHICLE_WATER,
    ;

    public String getName() {
        return UbiquityCore.getI18nResources().getString("itemtype."+name().toLowerCase());
    }

}
