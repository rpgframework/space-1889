/**
 * 
 */
package org.prelle.ubiquity.items;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.simplepersist.Element;
import org.prelle.ubiquity.requirements.Requirement;
import org.prelle.ubiquity.requirements.RequirementList;

/**
 * @author ogerlippe
 *
 */
public class Armor extends ItemTypeData {

	protected final static Logger logger = LogManager.getLogger("ubiquity.items");

	@org.prelle.simplepersist.Attribute
	private int defense;
	@org.prelle.simplepersist.Attribute
	private int handicap;

	@Element(name="requires")
	private RequirementList requires;

	//--------------------------------------------------------------------
	public Armor() {
		super(ItemType.ARMOR);
		requires = new RequirementList();
	}

	//--------------------------------------------------------------------
	/**
	 * @see Object#equals(Object)
	 */
	public boolean equals(Object o) {
		if (o instanceof Armor) {
            Armor other = (Armor) o;
            try {
                if (defense!=other.getDefense()) return false;
                if (handicap!=other.getHandicap()) return false;
                if (!requires.equals(other.getRequirements())) return false;
                return true;
            } catch (Exception e) {
                logger.error("Error comparing items",e);
            }
        }
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the defence
	 */
	public int getDefense() {
		return defense;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the dexterity penalty
	 */
	public int getHandicap() {
		return handicap;
	}

	//--------------------------------------------------------------------
	public void addRequirement(Requirement req) {
		requires.add(req);
	}

	//--------------------------------------------------------------------
	/**
	 * @return the requires
	 */
	public RequirementList getRequirements() {
		return requires;
	}

	//--------------------------------------------------------------------
	public String toString() {
		return String.format("def=%s req=%s", defense, requires);
	}

	//-------------------------------------------------------------------
	/**
	 * @param defense the defense to set
	 */
	public void setDefense(int defense) {
		this.defense = defense;
	}

	//-------------------------------------------------------------------
	/**
	 * @param dexterityPenalty the dexterityPenalty to set
	 */
	public void setHandicap(int value) {
		this.handicap = value;
	}

}
