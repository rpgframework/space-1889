/**
 * 
 */
package org.prelle.ubiquity.items;

import org.prelle.ubiquity.UbiquityCore;

/**
 * @author prelle
 *
 */
public enum ItemAttribute {
	
	WEIGHT,
	MIN_ATTRIBUTES,
	SPEED,
	RATE,
	CAPACITY,
	REACH,
	DAMAGE,
	HANDICAP,
	;

	//-------------------------------------------------------------------
	public String getShortName() {
		return UbiquityCore.getI18nResources().getString("itemattribute."+this.name().toLowerCase()+".short");
	}

    //-------------------------------------------------------------------
    public String getName() {
        return UbiquityCore.getI18nResources().getString("itemattribute."+this.name().toLowerCase());
    }

}
