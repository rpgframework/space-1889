package org.prelle.ubiquity.items;

import org.prelle.ubiquity.UbiquityCore;

/**
 * Created by rupp on 28.09.2014.
 */
public enum ItemLocationType {
    BODY,
    SOMEWHEREELSE;

    public String getName() {
        return UbiquityCore.getI18nResources().getString("itemlocationtype."+name().toLowerCase());
    }
}
