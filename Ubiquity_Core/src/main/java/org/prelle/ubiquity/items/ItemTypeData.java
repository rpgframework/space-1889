/**
 * 
 */
package org.prelle.ubiquity.items;

/**
 * @author prelle
 *
 */
public abstract class ItemTypeData {

	protected ItemType type;
	
	//-------------------------------------------------------------------
	protected ItemTypeData(ItemType type) {
		this.type = type;
	}
	
	//-------------------------------------------------------------------
	/**
	 * Return the kind of item
	 */
	public ItemType getType() {
		return type;
	}
	
}
